﻿import React from "react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import blank from "./components/Blank/Blank"
import Footer from "./components/Footer";
import Header from "./components/Header";
import Katalog from "./components/Katalog";
import Login from "./components/Login";
import Register from "./components/Register";
import Cart from "./components/Cart";
import Home from "./components/Home";
import DetailBarang from "./components/DetailBarang";
import Wishlist from "./components/Wishlist"
import MyAccount from "./components/MyAccount";
import Admin from "./components/admin";
import Address from "./components/Address";
import Review from "./components/Review";
import PagePath from "./components/PagePath";
import Invoice from "./components/Invoice";

//TODO Web Template Studio: Add routes for your new pages here.
const App = () => {
  function header(){
    if (window.location.pathname.indexOf("admin") != 1 && window.location.pathname.indexOf("invoice") != 1){
      return(<React.Fragment><Header/><br/><br/></React.Fragment>);
    }
  }
  function footer(){
    if (window.location.pathname.indexOf("invoice") != 1){
      return(<Footer/>);
    }
  }
    return (
      <React.Fragment>
          {header()}
            <PagePath/>
            <Route exact path = "/" component = { Home } />
            <Route exact path = "/login" component = { Login } />
            <Route exact path = "/register" component = { Register } />
            <Route exact path = "/cart" component = { Cart } />
            <Route exact path = "/katalog/:kategori/:jenis" component = { Katalog } />
            <Route exact path = "/detailbarang/:id_sneaker" component = { DetailBarang } />
            <Route exact path = "/myaccount/:index" component = { MyAccount } />
            <Route exact path = "/wishlist" component = { Wishlist } />
            <Route exact path = "/address" component = { Address } />
            <Route exact path = "/review" component = { Review } />
            <Route path = "/admin" component = { Admin } />
            <Route path = "/invoice" component = { Invoice } />
          {footer()}
      </React.Fragment>
    );
}

export default App;
