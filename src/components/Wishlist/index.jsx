import React, { useState, useEffect, Fragment } from 'react';
import axios from "axios";
import ModalDetail from '../Katalog/ModalDetail'
import './index.css'

const Wishlist = () => {
    const [wish, setWish] = useState([]);
    const [qty, setQty] = useState([]);
    let token=window.sessionStorage.getItem("user");

    //DETAIL SNEAKER
    const [detailalaskaki, setDetailalaskaki] = useState([]);
    const [ show, setShow ] = useState(false);

    async function getAllWishlist(){
        try{
            const res = await fetch("/apiUser/getAllWishlist", { 
                method : "GET",
                headers : { "Content-Type" : "application/json",'x-access-token': token }
            });
            const result = await res.json();
            result.alaskaki= result.alaskaki.map(item => ({
                id_wishlist: item.id_wishlist,
                id_sneaker: item.id_sneaker,
                id_kategori: item.id_kategori,
                nama_sneaker: item.nama_sneaker,
                jenis_sneaker: item.jenis_sneaker,
                brand_sneaker: item.brand_sneaker,
                harga_sneaker: item.harga_sneaker,
                status_sneaker: item.status_sneaker,
                gambar: item.gambar,
                ukuran:[],
                rating:[]
            }));
            for (let i = 0; i < result.alaskaki.length; i++) {
                result.alaskaki[i].ukuran=result.ukuran[i];
            }
            for (let i = 0; i < result.alaskaki.length; i++) {
                result.alaskaki[i].rating=result.rating[i];
            }
            setWish(result.alaskaki);
        }catch(error){
            console.error(error);
        }
    }
    useEffect(() => {
        getAllWishlist();
    }, []);

    function handleEmpty(e) {
        if(window.confirm('Are You Sure Want to Clear All Wishlist?')){
            e.preventDefault();
            axios
                .delete("http://127.0.0.1:3001/apiUser/emptyWishlist",{
                    headers: {
                        'x-access-token': token
                    } 
                })
                .then((res) => {
                })
                .catch((err) => {
                    console.log(err);
                });
            getAllWishlist();
        }
    }

    function handleDetailSneaker(item){
        setDetailalaskaki(item);
        setShow(true);
    }
    async function handleRemoveItem(id_wishlist){
        let obj = {
            id_wishlist : id_wishlist
        }
        try{
            const res = await fetch("/apiUser/deletePerItemWishlist", { 
                method : "POST",
                headers : { "Content-Type" : "application/json"},
                body: JSON.stringify(obj)
            });
            getAllWishlist();
        }catch(error){
            console.error(error);
        }
    }


    if(wish.length<=0){
        return(
            <div style={{margin:"5% 0 2% 4%", width: "50%"}}>
                <p>Your Wishlist Is Empty</p>
            </div>
        );
    }else{
        return (
            <>
            <div style={{margin: "auto",width: "50%",marginTop:"5%",marginBottom:'2%'}} className="container_wishlist_PC">
                <table className="table-responsive">
                    <thead className="text-center"
                        style={{ 
                                backgroundColor: 'white', 
                                color: 'black'
                        }}> 
                        <tr>
                            <th></th>
                            <th>ITEM NAME</th>
                            <th>PRICE</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {
                            wish.map((t, index) => <tr key={t.id_wishlist}>
                                <td className="p-3"> 
                                    <img className="img-fluid img-thumbnail"
                                            width="180vw"
                                            style={{ 
                                            border: '0.15vw solid rgb(1, 1, 50)', 
                                            borderRadius: '3%', 
                                            boxShadow: 'inset 0 1px 1px rgba(1, 1, 50, 0.075), 0 0 8px rgba(1, 1, 50, 0.5)' 
                                            }}
                                            src={ process.env.PUBLIC_URL+'/images/sneakers/'+t.gambar} 
                                            onClick={() => handleDetailSneaker(t)}/>
                                </td>
                                <td className="p-6">
                                    {t.nama_sneaker}
                                </td>
                                <td className="p-5">
                                    {"Rp." + t.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                                </td>
                                <td className="p-3">
                                    <button className="btn btn-danger" onClick={() => handleRemoveItem(t.id_wishlist)}>
                                        REMOVE
                                    </button>
                                </td>
                            </tr>)
                        }
                    </tbody>
                </table>
                <div className="d-flex mt-4">
                    <button className="btn btn-secondary mr-2" onClick={handleEmpty}>
                        EMPTY WISHLIST
                    </button>
                </div>
            </div>


            <div style={{margin: "auto",margin: "3%", fontSize:"10px"}} className="container_wishlist_HP">
                <br/><br/>
                <div style={{width:"100%"}}>
                        <center><div style={{float:"left", width:"40%"}}>ITEM PICTURE</div></center>
                        <div style={{float:"left", width:"30%"}}>ITEM NAME</div>
                        <div style={{float:"left", width:"30%"}}>PRICE</div>
                </div>
                <br style={{clear:"both"}}/><br/>
                <div style={{width:"100%"}}>
                    {
                        wish.map((t, index) => {
                            return(
                                <>
                                    <div style={{float:"left", width:"40%"}}>
                                        <center>
                                            <img 
                                                width="60%"
                                                style={{ 
                                                border: '0.15vw solid rgb(1, 1, 50)', 
                                                borderRadius: '3%', 
                                                boxShadow: 'inset 0 1px 1px rgba(1, 1, 50, 0.075), 0 0 8px rgba(1, 1, 50, 0.5)' 
                                                }}
                                                src={ process.env.PUBLIC_URL+'/images/sneakers/'+t.gambar} 
                                                onClick={() => handleDetailSneaker(t)}/>
                                        </center>
                                    </div>
                                    <div style={{float:"left", width:"30%"}}>{t.nama_sneaker}</div>
                                    <div style={{float:"left", width:"30%"}}>{"Rp." + t.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</div>
                                    <button className="btn btn-danger" onClick={() => handleRemoveItem(t.id_wishlist)} style={{marginLeft:"-1%", marginTop:"5%", width:"20%", fontSize:"7px"}}>
                                        REMOVE
                                    </button>
                                    <div style={{clear:"both"}}></div><br/><br/>
                                </>
                            )
                        })
                    }
                </div>
            </div>
            <br/><br/><br/><br/><br/><br/>
            <ModalDetail show={show} setShow={setShow} detailalaskaki={detailalaskaki} />
            </>
        );
    }
    }


 
export default Wishlist;