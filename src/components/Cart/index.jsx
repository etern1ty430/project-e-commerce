import React, { useState, useEffect, Fragment } from 'react';
import { useHistory } from "react-router-dom";
import { faArrowAltCircleLeft, faArrowAltCircleRight, faTimesCircle } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios from "axios";
import './index.css';
import DetailPromo from "./DetailPromo";

import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'

const Cart = () => {
	const properties = { //PROPERTIES SLIDE PROMO
		transitionDuration: 500,
		infinite: true,
		prevArrow:  <form onSubmit={prevPromo}>
						<input type="submit" style={{width: "30px", backgroundColor:"transparent",float:"right", position:"absolute", textDecoration:"none", zIndex:"2", opacity:"0"}} value=""/>
						<FontAwesomeIcon icon={ faArrowAltCircleLeft } style={{float:"right", marginTop:"70%", cursor:"pointer"}}/>
					</form>,
		nextArrow:  <form onSubmit={nextPromo}>
						<input type="submit" style={{width: "30px", backgroundColor:"transparent",float:"left", position:"absolute", textDecoration:"none", zIndex:"2", opacity:"0", marginLeft:"-9%"}} value=""/>
						<FontAwesomeIcon icon={ faArrowAltCircleRight } style={{float:"left", position:"absolute", marginLeft:"-9%", cursor:"pointer"}}/>
					</form>
	};
	const propertiesFalse = { //PROPERTIES SLIDE PROMO
		transitionDuration: 500,
		infinite: false,
		prevArrow: <div style={{opacity:"0"}}/>,
		nextArrow: <div style={{opacity:"0"}}/>
	};

	const [cart, setCart] = useState([]);
	const [promo, setPromo] = useState([]);
	const [qty, setQty] = useState([]);

	//CART TOTAL (LABEL ONLY)
	const [total, setTotal] = useState(0);
	const [totalPromo, setTotalPromo] = useState(0); const [idPromoSelect, setIdPromoSelect] = useState(0);
	const [grandTotal, setGrandTotal] = useState(0);

	//MODAL PROMO DETAIL
	const [showpromo, setShowpromo] = useState(false);
	const [detailpromo, setDetailpromo] = useState(null);

	const history = useHistory();
	const routeChange = () =>{ 
		let path = `/address`;
		window.sessionStorage.setItem("promotemp",totalPromo); 
		if(promo.length > 0) window.sessionStorage.setItem("idpromotemp",promo[idPromoSelect].id_promo); 
		else window.sessionStorage.setItem("idpromotemp",null);
		window.sessionStorage.setItem("carttemp",cart);
		history.push(path);
		return window.location.reload();
	}

    let token=window.sessionStorage.getItem("user");


	function prevPromo(e){
		e.preventDefault();
		if(idPromoSelect == 0) setIdPromoSelect(promo.length-1);
		else setIdPromoSelect(idPromoSelect-1);
		loadCart();
	}
	function nextPromo(e){
		e.preventDefault();
		if(idPromoSelect == promo.length-1) setIdPromoSelect(0);
		else setIdPromoSelect(idPromoSelect+1);
		loadCart();
	}

	function loadCart(){
		if(window.sessionStorage.getItem("user")!=null){
			axios
			.get("http://127.0.0.1:3001/apiUser/getCart", {
				headers: {
					'x-access-token': token
				}
			})
			.then((res) => {
				setCart(res.data[0].listCart);
				setPromo(res.data[0].promo);
				
				let tempQty = [];
				for(let i=0; i<res.data[0].listCart.length; i++){
					tempQty[i] = res.data[0].listCart[i].jumlah_sneaker;
				}
				setQty(tempQty);

				//HITUNG TOTAL
				var temp = 0;
				for (var i = 0; i < res.data[0].listCart.length; i++) { 
					temp += res.data[0].listCart[i].harga_sneaker*res.data[0].listCart[i].jumlah_sneaker;  
				}
				setTotal(temp);
				if(idPromoSelect == -1 && res.data[0].promo.length > 0) setIdPromoSelect(0);
				//calculatePromo(temp, res.data[0].promo);
			})
			.catch((err) => {
				console.log(err);
			});
		}
		else{
			if(window.sessionStorage.getItem("cart") != null){
				let sessionCart = JSON.parse(window.sessionStorage.getItem("cart"));
				let tempQty = [];
				for(let i=0; i<sessionCart.length; i++){
					sessionCart[i].counter = i;
					tempQty[i] = sessionCart[i].jumlah_sneaker;
				}
				setQty(tempQty);
				setCart(sessionCart);

				//HITUNG TOTAL
				var temp = 0;
				for (var i = 0; i < sessionCart.length; i++) { 
					temp += sessionCart[i].harga_sneaker*sessionCart[i].jumlah_sneaker;  
				}
				setTotal(temp);
				setGrandTotal(temp);
			}
		}
	}
    useEffect(() => {
		loadCart();
	}, []);

	function handleEmpty() {
		if(window.confirm('Are You Sure Want to Clear All Cart?')){
			if(window.sessionStorage.getItem("user")!=null){
				axios
					.delete("http://127.0.0.1:3001/apiUser/emptyCart",{
						headers: {
							'x-access-token': token
						} 
					})
					.then((res) => {
						loadCart();
					})
					.catch((err) => {
						console.log(err);
					});
			}
			else {
				window.sessionStorage.setItem('cart', JSON.stringify([]));
				loadCart();
			}
		}
	}
	
	async function handleChangeQty(qty,id_cart, id_sneaker, ukuran_sneaker){
		if(window.sessionStorage.getItem("user")!=null){
			if(qty == 0){
				deleteItem(id_cart);
			}
			else {
				let obj = {
					id_cart: id_cart,
					qty: qty
				}
				try{
					const res = await fetch("/apiUser/updateQtyCart", { 
						method : "POST",
						headers : { "Content-Type" : "application/json" },
						body: JSON.stringify(obj)
					});
					loadCart();
				}catch(error){
					console.error(error);
				}
			}
		}
		else{
			let sessionCart = JSON.parse(window.sessionStorage.getItem("cart"));
			if(qty == 0){
				let temp = -1;
				for(let i=0; i<sessionCart.length; i++){
					if(sessionCart[i].id_sneaker == id_sneaker && sessionCart[i].ukuran_sneaker == ukuran_sneaker) temp = i;
				}
				sessionCart.splice(temp, 1);
				window.sessionStorage.setItem('cart', JSON.stringify(sessionCart));
				loadCart();
			}
			else {
				for(let i=0; i<sessionCart.length; i++){
					if(sessionCart[i].id_sneaker == id_sneaker && sessionCart[i].ukuran_sneaker == ukuran_sneaker) {
						sessionCart[i].jumlah_sneaker = qty;
					}
				}
				window.sessionStorage.setItem('cart', JSON.stringify(sessionCart));
				loadCart();
			}
		}
	}

	async function deleteItem(id_cart, id_sneaker, ukuran_sneaker){ // untuk delete per item pada cart
		if(window.sessionStorage.getItem("user")!=null){
			let obj = {
				id_cart: id_cart
			}
			try{
				const res = await fetch("/apiUser/deleteCart", { 
					method : "DELETE",
					headers : { "Content-Type" : "application/json" },
					body: JSON.stringify(obj)
				});
				loadCart();
			}catch(error){
				console.error(error);
			}
		}
		else {
			let sessionCart = JSON.parse(window.sessionStorage.getItem("cart"));
			let tempIndex = -1;
			for(let i=0; i<sessionCart.length; i++){
				if(sessionCart[i].id_sneaker == id_sneaker && sessionCart[i].ukuran_sneaker == ukuran_sneaker) tempIndex = i;
			}
			sessionCart.splice(tempIndex, 1);
			window.sessionStorage.setItem('cart', JSON.stringify(sessionCart));
			loadCart();
		}
	}

	function handleOpenPromo(item){
		setDetailpromo(item);
		setShowpromo(true);
	}

	function showPromo(){
		if(window.sessionStorage.getItem("user")!=null && promo.length > 1){
			return(
				<Slide autoplay={false} {...properties}>
				{
					promo.map((item, index) => {
						return(
							<div className="each-slide" id="gambar_promo">
								<div className="image-container" >
								<img className="card-img-top" onClick={() => handleOpenPromo(item)} src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar } style={{width: "90%",borderRadius:"20px"}}/>
								</div>
							</div>
						)
					})
				}
				</Slide>
			)
		}
		else if(window.sessionStorage.getItem("user")!=null && promo.length == 1){
			return(
				<Slide autoplay={false} {...propertiesFalse}>
				{
					promo.map((item, index) => {
						return(
							<div className="each-slide" id="gambar_promo">
								<div className="image-container" >
								<img className="card-img-top" onClick={() => handleOpenPromo(item)} src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar } style={{borderRadius:"20px"}}/>
								</div>
							</div>
						)
					})
				}
				</Slide>
			)
		}
		else {
			return("Tidak Ada Promo");
		}
	}

	useEffect(() => {
		if(window.sessionStorage.getItem("user")!=null) calculatePromo(total, promo);
	}, [idPromoSelect, total, promo, cart]);
	function calculatePromo(totalTemp, promoParameter){
		if(promo.length > 0){
			if(totalTemp && promoParameter){
				let promoTemp = promoParameter[idPromoSelect];
				//alert(idPromoSelect)
				if(promoTemp.min_belanja > totalTemp) {
					setTotalPromo(0);
					setGrandTotal(totalTemp);
				}
				else {
					let tempDiskon = promoTemp.diskon * total / 100;
					if(tempDiskon > promoTemp.max_diskon) {
						setTotalPromo(promoTemp.max_diskon);
						setGrandTotal(totalTemp-promoTemp.max_diskon);
					}
					else {
						setTotalPromo(tempDiskon);
						setGrandTotal(totalTemp-tempDiskon);
					}
				}
			}
		}
		else {
			setGrandTotal(totalTemp);
		}
	}
	
	return (
		<div class="row" style={{fontFamily:"calibri"}}>
			<DetailPromo detailpromo={detailpromo} showpromo={showpromo} setShowpromo={setShowpromo}/>
			<div class="col-sm-1"></div>
			<div class="col-sm-7">
			<table class="styleTable">
				<br/><br/>
					<tr>
						<th>&emsp;PRODUCT</th>
						<th>&nbsp;&nbsp;</th>
						<th>SIZE</th>
						<th>QUANTITY</th>
						<th>SUB TOTAL</th>
					</tr>
					<tr>
						<td><hr style={{width:"100%"}}></hr></td>
						<td><hr style={{width:"100%"}}></hr></td>
						<td><hr style={{width:"100%"}}></hr></td>
						<td><hr style={{width:"100%"}}></hr></td>
						<td><hr style={{width:"100%"}}></hr></td>
					</tr>
					{cart ? 
						cart.map((c, index) => {
							return (
								<>
									<tr>
									<td>
										<center>
											<img className="cartproduct" src={ process.env.PUBLIC_URL+'/images/sneakers/'+c.gambar_sneaker } />
											<p class="nama_sneaker">{c.nama_sneaker}</p>
											<p class="harga_sneaker">{"Rp." + c.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</p>
										</center>
									</td>
									<td> &nbsp;&nbsp; </td>
									<td>
										&nbsp;&nbsp;&nbsp;{c.ukuran_sneaker}
									</td>
									<td>
										<input className="qty" type="number" value={qty[c.counter]} onChange={(e) => handleChangeQty(e.target.value,c.id_cart, c.id_sneaker, c.ukuran_sneaker)} max={c.stok_sneaker}></input>
										({c.stok_sneaker} stock)
									</td>
									<td style={{fontWeight:"bold"}}>{"Rp." + (c.jumlah_sneaker*c.harga_sneaker).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</td>
									<td><FontAwesomeIcon icon={ faTimesCircle } onClick={() => deleteItem(c.id_cart, c.id_sneaker, c.ukuran_sneaker)}/></td>
									</tr>
									<hr style={{width:"360%"}}/>
								</>
							)
						})
					: ""}
					<br/><br/>
				</table>
				<button onClick={() => handleEmpty()} class="empty_cart">EMPTY CART</button>
			</div>
			<div class="col-sm-3">
				<table>
					<br/><br/>
					<tr>
						<th class="promo" style={{fontWeight:"bold"}}>PROMO</th>
					</tr>
					<hr/>
				</table>
				<br/><br/>
				<div className="slide-container" style={{marginTop:"-15%"}}>
					{showPromo()}
				</div><br/>

				<div class="cart_total">
					<div style={{width:"100%"}}>
						<div style={{width:"40%", float:"left"}}>
							<b>CART TOTAL 	</b><br/><br/>
							Total 		<br/>
							Promo 			<br/>
											<br/>
							Grand Total			<br/>
						</div>
						<div style={{width:"60%", textAlign:"right", color:"red", fontWeight:"bold", float:"left"}}>
																		<br/><br/>
														{"Rp." + total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}	<br/>
							<p style={{color:"green"}}>	{"Rp." + totalPromo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}	</p>
																		<hr style={{backgroundColor:"black"}}/>
														{"Rp." + grandTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} <br/>
						</div>
					</div>
					<center style={{clear:"both"}}>
						<br/><br/>
						<button onClick={routeChange} style={{backgroundColor:"black", color:"white", letterSpacing:"2px", padding:"4%"}}>PROCEED TO CHECKOUT</button>
					</center>
				</div><br/><br/>
			</div>
			<div class="col-sm-1"></div>
		</div>
	); 
}

export default Cart;