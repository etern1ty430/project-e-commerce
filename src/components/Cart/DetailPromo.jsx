import React, { useState, useEffect } from "react";
import { Modal } from 'react-bootstrap';

const DetailPromo = ({detailpromo, showpromo, setShowpromo}) => {
    function handleClosePromo() { 
        setShowpromo(false); 
    }

    return (
        <>
          <Modal size="md" style={{ fontSize: '1.25vw', opacity:1 }}
            show={ showpromo } 
            onHide={ handleClosePromo }>
              { detailpromo ? 
                  <> 
                      <Modal.Header closeButton>
                          <Modal.Title>
                              Detail Promo  
                          </Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="mt-4">
                            <div className="card mb-4">
                                <div className="card-title"
                                    style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                    <h5 className="text-center text-white p-3">
                                        {detailpromo.judul.toUpperCase()}
                                    </h5>
                                </div>
                                <div className="card-body"
                                    style={{ padding: '0% !important' }}>
                                    <div className="row">
                                        <img className="col-12" src={ process.env.PUBLIC_URL+'/images/promo/'+detailpromo.gambar} />
                                    </div>
                                </div>
                                <div>
                                <hr/>
                                <div style={{marginLeft:"3%"}}>
                                    <Modal.Title>
                                    Description :  
                                    </Modal.Title>
                                    <p style={{fontSize:"15px"}}>{detailpromo.deskripsi}</p>
                                </div>
                                <hr/>
                                <div style={{marginLeft:"3%"}}>
                                    <Modal.Title>
                                    Terms And Conditions :  
                                    </Modal.Title>
                                    <div style={{fontSize:"15px", width:"100%"}}>
                                        <div style={{width:"45%", float:"left"}}>
                                            Discount            <br/>
                                            Max Discount        <br/>
                                            Minimum Shipping    <br/><br/>
                                            Validity            <br/>
                                        </div>
                                        <div style={{width:"55%", float:"left"}}>
                                            : {detailpromo.diskon}%         <br/>
                                            : Rp.{detailpromo.max_diskon.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}      <br/>
                                            : Rp.{detailpromo.min_belanja.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}     <br/><br/>
                                            : {detailpromo.tanggal_mulai.substr(0,10)} To {detailpromo.tanggal_berakhir.substr(0,10)} <br/>
                                        </div>
                                    </div>
                                    <br style={{clear:"both"}}/>
                                    <hr/>
                                    <p style={{textAlign:"right",marginRight:"2%",fontSize:"15px",color:"red"}}>{detailpromo.jumlah_kupon}x Promo Remaining</p>
                                </div>
                                </div>
                            </div>
                          </div>
                      </Modal.Body>
                  </> 
              : '' } 
          </Modal>
        </>
    )
};

export default DetailPromo