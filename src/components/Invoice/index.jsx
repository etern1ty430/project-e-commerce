import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebook, faTwitter, faLine, faSnapchat, faYahoo } from "@fortawesome/free-brands-svg-icons"
import { useHistory } from 'react-router-dom';
import axios from "axios";
import './index.css'


const Invoice = () => {
    const [dataLaporanHeader, setDataLaporanHeader] = useState(null);
    const [dataLaporanDetail, setDataLaporanDetail] = useState(null);
    const history= useHistory();
    let token=window.sessionStorage.getItem("user");

    async function loadInvoice(){
        try{
            const res = await fetch("/apiUser/getInvoice", { 
                method : "GET",
                headers : { "Content-Type" : "application/json"}
            });
            let item = await res.json();
            setDataLaporanHeader(item.header);
            setDataLaporanDetail(item.detail);
        }catch(error){
            console.error(error);
        }
    }

    function emptyCart() {
        if(window.sessionStorage.getItem("user")!=null){
            axios
                .delete("http://127.0.0.1:3001/apiUser/emptyCart",{
                    headers: {
                        'x-access-token': token
                    } 
                })
                .then((res) => {
                })
                .catch((err) => {
                    console.log(err);
                });
        }
        else {
            window.sessionStorage.setItem('cart', JSON.stringify([]));
        }
	}

    async function goToHome(){
        emptyCart();
        history.push('/');
        return window.location.reload();
    }

    useEffect(() => {
		loadInvoice();
	}, []);

    if(dataLaporanHeader && dataLaporanDetail){
        return (
            <>
                <div class="containerPC">
                    <div style={{marginLeft:'5%', marginRight:'5%'}} onClick={goToHome}>
                        <div class="row">
                            {/* -----INVOICE AND DATE----- */}
                            <div class="col-sm-6" style={{backgroundColor:"none"}}>
                                <br/><br/><br/>
                                <p style={{fontSize:"48px"}}>
                                {/* <label style={{letterSpacing:"0px"}}>____</label> &nbsp; */}
                                <label style={{letterSpacing:"5px"}}> I N V O I C E</label>
                                </p>
                                {
                                dataLaporanHeader.map((item) => {
                                    let date = new Date(item.tanggal_beli);
                                    date.setDate(date.getDate()-1);
                                    return (
                                        <div style={{letterSpacing:'2px', fontSize:'20px'}}>
                                        No. #{item.id_trans} // {date.toDateString()}
                                        </div>
                                    )
                                })
                                }
                            </div>
                            {/* -----LOGO----- */}
                            <div class="col-sm-6" style={{textAlign:'right', backgroundColor:'none'}}>
                                <div style={{backgroundColor:'rgb(65,65,65)', width:"35%", height:"100%", float:'right', color:'white', letterSpacing:'1px'}}>
                                <center>
                                    <img src={ process.env.PUBLIC_URL+'/icons/icon1transparent.png'} style={{width:"70%"}}></img>
                                    <p style={{fontSize:"30px", marginBottom:'-10px'}}>AlasKakiQu</p>
                                    <p>Good design is a language, <br/>not a style</p>
                                </center>
                                </div>
                            </div>
                            </div>
                
                            <br/><br/><br/><br/>
                            <div class="row">
                            {/* -----HEADER INVOICE----- */}
                            <div class="col-sm-6" style={{backgroundColor:"none", letterSpacing:'2px'}}>
                                <p style={{marginBottom:'0', fontSize:'19px'}}><b>Billing To :</b></p>
                                {
                                dataLaporanHeader.map((item) => {
                                    return(
                                        <div>
                                        {item.nama_pembeli}<br/>
                                        {item.alamat_pengiriman}, {item.kode_pos}<br/>
                                        </div>
                                    )
                                })
                                }
                            </div>
                            {/* -----TOTAL INVOICE----- */}
                            <div class="col-sm-6" style={{backgroundColor:"none", textAlign:'right', letterSpacing:'2px'}}>
                                <p style={{marginBottom:'0', fontSize:'20px'}}>Total Due</p>
                                {
                                dataLaporanHeader.map((item) => {
                                    return(
                                        <div style={{fontSize:"32px"}}>
                                        <b>Rp.{parseInt(item.total_harga).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b>
                                        </div>
                                    )
                                })
                                }
                            </div>
                        </div>
                
                
                            
                        {/* -----ISI INVOICE----- */}
                        <br/><br/>
                        {
                        dataLaporanHeader.map((item) => {
                            return (
                                <div key={item.id_trans}>
                                {/* HEADER TABEL */}
                                <hr style={{backgroundColor:'black'}}/><b>
                                <div style={{float:"left",width:"3%"}}> NO. </div>
                                <div style={{float:"left",width:"24%"}}> SNEAKERS </div>
                                <div style={{float:"left",width:"13%"}}> BRAND </div>
                                <div style={{float:"left",width:"10%", textAlign:"right"}}> SIZE </div>
                                <div style={{float:"left",width:"10%", textAlign:"right"}}> QUANTITY </div>
                                <div style={{float:"left",width:"20%", textAlign:"right"}}> PRICE </div>
                                <div style={{float:"left",width:"20%", textAlign:"right"}}> SUBTOTAL </div>
                                </b><p style={{clear:"both"}}/>
                                <hr style={{backgroundColor:'black'}}/>
            
            
                                {/* DETAIL PEMBELIAN */}
                                <div style={{float:"left",width:"3%"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.counter}.</p><hr/></div>
                                        );
                                    })
                                    }
                                </div>
                                <div style={{float:"left",width:"24%"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.nama_item}</p><hr/></div>
                                        );
                                    })
                                    }
                                </div>
                                <div style={{float:"left",width:"13%"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.brand_item}</p><hr/></div>
                                        );
                                    })
                                    }
                                </div>
                                <div style={{float:"left",width:"10%", textAlign:"right"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.ukuran_item}</p><hr/></div>
                                        );
                                    })
                                    }
                                </div>
                                <div style={{float:"left",width:"10%", textAlign:"right"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.jumlah_item}</p><hr/></div>
                                        );
                                    })
                                    }
                                </div>
                                <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p><hr/></div>
                                        );
                                    })
                                    }
                                    <div style={{minHeight:"50px"}}><b> SUB-TOTAL </b></div>
                                    <div style={{minHeight:"50px"}}> SHIPPING COST (RAJAONGKIR) </div>
                                    <div style={{color:'red'}}> DISCOUNT ({item.diskon_promo}%) </div>
                                    <div style={{color:'red'}}> {item.deskripsi_promo} </div>
                                </div>
                                <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                                    {
                                    dataLaporanDetail.map(itemDetail => {
                                        return (
                                            <div><p style={{padding:'10px 0 10px 0'}}>Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p><hr/></div>
                                        );
                                    })
                                    }
                                    <div style={{minHeight:"50px"}}><b>Rp.{(parseInt(item.total_harga) - parseInt(item.harga_shipping) + parseInt(item.total_promo)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                                    <div style={{minHeight:"50px"}}>Rp.{item.harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                                    <div style={{color:'red'}}> Rp.{item.total_promo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </div><br/>
                                </div>
                                <p style={{clear:"both"}}/>
            
            
                                {/* GRAND TOTAL */}
                                <hr style={{backgroundColor:'black'}}/>
                                <div style={{float:"left",width:"60%", textAlign:"right", minHeight:"30px"}}/>
                                <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px", fontSize:'20px'}}><b>GRAND TOTAL</b></div>
                                <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px", fontSize:'20px'}}><b>Rp.{parseInt(item.total_harga).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                                <p style={{clear:"both"}}/>
                                <hr style={{backgroundColor:'black'}}/><br/><br/><br/>
                                </div>
                            )
                        })
                        }
            
                        {/* FOOTER INVOICE */}
                        <div class="row" style={{letterSpacing:'0px'}}>
                        <div class="col-sm-6" style={{textAlign:'right'}}>
                            <p>73-77 Ngagel Jaya Utara, 60123</p>
                            <p style={{marginTop:'-1%'}}>Surabaya, Jawa Timur</p>
                            <p style={{marginTop:'-1%'}}>contact@alaskakiqu.com</p>
                            <p style={{marginTop:'-1%'}}>+6281 2345 6789</p>
                        </div>
                        <div class="col-sm-6" style={{fontSize:'33px'}}>
                            <b>
                            <p style={{marginTop:'0.5%'}}>THANKS FOR</p>
                            <p style={{marginTop:'-0.5%'}}>YOUR BUSINESS</p>
                            </b>
                        </div>
                        </div>
            
                        <center style={{marginBottom:'5%'}}>
                        <br/>
                        <b>www.alaskakiqu.com</b>
                        <br/>
                        <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block', marginTop:'1%'}}>
                            <i class="circleIcon"><FontAwesomeIcon icon={ faFacebook } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                        </span>&nbsp;&nbsp;
                        <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                            <i class="circleIcon"><FontAwesomeIcon icon={ faTwitter } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                        </span>&nbsp;&nbsp;
                        <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                            <i class="circleIcon"><FontAwesomeIcon icon={ faYahoo } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                        </span>&nbsp;&nbsp;
                        <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                            <i class="circleIcon"><FontAwesomeIcon icon={ faSnapchat } style={{height:"70%", width:"70%", marginTop:'20%'}}/></i>
                        </span>&nbsp;&nbsp;
                        </center>
                    </div>
                </div>





                <div class="containerHP">
                    <div style={{marginLeft:'5%', marginRight:'5%'}} onClick={goToHome}>
                        <div style={{width:"100%"}}>
                            {/* -----INVOICE AND DATE----- */}
                            <div style={{backgroundColor:"none", width:"60%", float:"left"}}>
                                <br/><br/><br/>
                                <p style={{fontSize:"18px"}}>
                                    {/* <label style={{letterSpacing:"0px"}}>____</label> &nbsp; */}
                                    <label style={{letterSpacing:"5px"}}> I N V O I C E</label>
                                </p>
                                {
                                dataLaporanHeader.map((item) => {
                                    let date = new Date(item.tanggal_beli);
                                    date.setDate(date.getDate()-1);
                                    return (
                                        <div style={{letterSpacing:'2px', fontSize:'8px', marginTop:"-5%"}}>
                                            No. #{item.id_trans} // {date.toDateString()}
                                        </div>
                                    )
                                })
                                }
                            </div>
                            {/* -----LOGO----- */}
                            <div style={{textAlign:'right', backgroundColor:'none', width:"40%", float:"left"}}>
                                <div style={{backgroundColor:'rgb(65,65,65)', width:"70%", height:"100%", float:'right', color:'white', letterSpacing:'1px'}}>
                                    <center>
                                        <img src={ process.env.PUBLIC_URL+'/icons/icon1transparent.png'} style={{width:"70%"}}></img>
                                        <p style={{fontSize:"12px", marginBottom:'-10px'}}>AlasKakiQu</p>
                                        <p style={{fontSize:"6px"}}>Good design is a language, <br/>not a style</p>
                                    </center>
                                </div>
                            </div>
                        </div>
                
                        <div style={{width:"100%", clear:"both"}}>
                            <br/><br/>
                            {/* -----HEADER INVOICE----- */}
                            <div style={{backgroundColor:"none", letterSpacing:'1px', width:"50%", float:"left"}}>
                                <p style={{marginBottom:'0', fontSize:'10px'}}><b>Billing To :</b></p>
                                {
                                dataLaporanHeader.map((item) => {
                                    return(
                                        <div style={{fontSize:"8px"}}>
                                            {item.nama_pembeli}<br/>
                                            {item.alamat_pengiriman}, {item.kode_pos}<br/>
                                        </div>
                                    )
                                })
                                }
                            </div>
                            {/* -----TOTAL INVOICE----- */}
                            <div style={{backgroundColor:"none", textAlign:'right', letterSpacing:'2px', width:"50%", float:"left"}}>
                                <p style={{marginBottom:'0', fontSize:'10px'}}>Total Due</p>
                                {
                                dataLaporanHeader.map((item) => {
                                    return(
                                        <div style={{fontSize:"16px"}}>
                                        <b>Rp.{parseInt(item.total_harga).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b>
                                        </div>
                                    )
                                })
                                }
                            </div>
                        </div>
                
                
                            
                        {/* -----ISI INVOICE----- */}
                        <br/><br/>
                        {
                        dataLaporanHeader.map((item) => {
                            return (
                                <div key={item.id_trans} style={{fontSize:"10px"}}>
                                    {/* HEADER TABEL */}
                                    <hr style={{backgroundColor:'black'}}/><b>
                                    <div style={{float:"left",width:"10%"}}> NO. </div>
                                    <div style={{float:"left",width:"30%"}}> SNEAKERS </div>
                                    <div style={{float:"left",width:"10%"}}> SIZE </div>
                                    <div style={{float:"left",width:"20%", textAlign:"right"}}> QUANTITY </div>
                                    <div style={{float:"left",width:"30%", textAlign:"right"}}> SUBTOTAL </div>
                                    </b><p style={{clear:"both"}}/>
                                    <hr style={{backgroundColor:'black'}}/>
                
                
                                    {/* DETAIL PEMBELIAN */}
                                    <div style={{float:"left",width:"10%"}}> 
                                        {
                                        dataLaporanDetail.map(itemDetail => {
                                            return (
                                                <div><p style={{padding:'0 0 10px 0'}}>{itemDetail.counter}.</p><hr/></div>
                                            );
                                        })
                                        }
                                    </div>
                                    <div style={{float:"left",width:"30%"}}> 
                                        {
                                        dataLaporanDetail.map(itemDetail => {
                                            return (
                                                <div><p style={{padding:'0 0 10px 0'}}>{itemDetail.nama_item}</p><hr/></div>
                                            );
                                        })
                                        }
                                    </div>
                                    <div style={{float:"left",width:"10%"}}> 
                                        {
                                        dataLaporanDetail.map(itemDetail => {
                                            return (
                                                <div><p style={{padding:'0 0 10px 0'}}>{itemDetail.ukuran_item}</p><hr/></div>
                                            );
                                        })
                                        }
                                    </div>
                                    <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                                        {
                                        dataLaporanDetail.map(itemDetail => {
                                            return (
                                                <div><p style={{padding:'0 0 10px 0'}}>{itemDetail.jumlah_item}</p><hr/></div>
                                            );
                                        })
                                        }
                                    </div>
                                    {/* <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                                        {
                                        dataLaporanDetail.map(itemDetail => {
                                            return (
                                                <div><p style={{padding:'10px 0 10px 0'}}>Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p><hr/></div>
                                            );
                                        })
                                        }
                                        <div style={{minHeight:"50px"}}><b> SUB-TOTAL </b></div>
                                        <div style={{minHeight:"50px"}}> SHIPPING COST (RAJAONGKIR) </div>
                                        <div style={{color:'red'}}> DISCOUNT ({item.diskon_promo}%) </div>
                                        <div style={{color:'red'}}> {item.deskripsi_promo} </div>
                                    </div> */}
                                    <div style={{float:"left",width:"30%", textAlign:"right"}}> 
                                        {
                                        dataLaporanDetail.map(itemDetail => {
                                            return (
                                                <div><p style={{padding:'0 0 10px 0'}}>Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p><hr/></div>
                                            );
                                        })
                                        }
                                        {/* <div style={{minHeight:"50px"}}><b>Rp.{(parseInt(item.total_harga) - parseInt(item.harga_shipping) + parseInt(item.total_promo)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                                        <div style={{minHeight:"50px"}}>Rp.{item.harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                                        <div style={{color:'red'}}> Rp.{item.total_promo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </div><br/> */}
                                    </div>

                                    <div style={{clear:"both", width:"100%", textAlign:"right"}}>
                                        <div style={{width:"70%", float:"left"}}>
                                            <div style={{minHeight:"20px"}}><b> SUB-TOTAL </b></div>
                                            <div style={{minHeight:"20px"}}> SHIPPING COST (RAJAONGKIR) </div>
                                            <div style={{color:'red'}}> DISCOUNT ({item.diskon_promo}%) </div>
                                            <div style={{color:'red'}}> {item.deskripsi_promo} </div>
                                        </div>
                                        <div style={{width:"30%", float:"left"}}>
                                            <div style={{minHeight:"20px"}}><b>Rp.{(parseInt(item.total_harga) - parseInt(item.harga_shipping) + parseInt(item.total_promo)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                                            <div style={{minHeight:"20px"}}>Rp.{item.harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                                            <div style={{color:'red'}}> Rp.{item.total_promo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </div><br/>
                                        </div>
                                    </div>
                
                                    {/* GRAND TOTAL */}
                                    <br style={{clear:"both"}}/>
                                    <hr style={{backgroundColor:'black'}}/>
                                    <div style={{clear:"both", width:"100%", textAlign:"right"}}>
                                        <div style={{width:"70%", float:"left"}}>
                                            <div style={{fontSize:'10px'}}><b>GRAND TOTAL</b></div>
                                        </div>
                                        <div style={{width:"30%", float:"left"}}>
                                            <div style={{fontSize:'10px'}}><b>Rp.{parseInt(item.total_harga).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                                        </div>
                                    </div>
                                    <br style={{clear:"both"}}/>
                                    <hr style={{backgroundColor:'black'}}/><br/><br/><br/>
                                </div>
                            )
                        })
                        }
            
                        {/* FOOTER INVOICE */}
                        <div style={{letterSpacing:'0px', width:"100%", fontSize:"10px"}}>
                            <div style={{textAlign:'right', width:"47%", float:"left"}}>
                                <p>73-77 Ngagel Jaya Utara, 60123</p>
                                <p style={{marginTop:'-5%'}}>Surabaya, Jawa Timur</p>
                                <p style={{marginTop:'-5%'}}>contact@alaskakiqu.com</p>
                                <p style={{marginTop:'-5%'}}>+6281 2345 6789</p>
                            </div>
                            <div style={{ width:"6%", float:"left"}}>&nbsp;</div>
                            <div style={{fontSize:'17px', width:"47%", float:"left"}}>
                                <b>
                                <p style={{marginTop:'8%'}}>THANKS FOR</p>
                                <p style={{marginTop:'-5%'}}>YOUR BUSINESS</p>
                                </b>
                            </div>
                        </div>
            
                        <center style={{marginBottom:'5%', clear:"both", fontSize:"10px"}}>
                            <br/>
                            <b>www.alaskakiqu.com</b>
                            <br/>
                            <span style={{height:'25px', width:'25px', backgroundColor:'black', borderRadius:'50%', display:'inline-block', marginTop:'1%'}}>
                                <i class="circleIcon"><FontAwesomeIcon icon={ faFacebook } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                            </span>&nbsp;&nbsp;
                            <span style={{height:'25px', width:'25px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                                <i class="circleIcon"><FontAwesomeIcon icon={ faTwitter } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                            </span>&nbsp;&nbsp;
                            <span style={{height:'25px', width:'25px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                                <i class="circleIcon"><FontAwesomeIcon icon={ faYahoo } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                            </span>&nbsp;&nbsp;
                            <span style={{height:'25px', width:'25px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                                <i class="circleIcon"><FontAwesomeIcon icon={ faSnapchat } style={{height:"70%", width:"70%", marginTop:'20%'}}/></i>
                            </span>&nbsp;&nbsp;
                        </center>
                    </div>
                </div>
            </>
        ); 
    }
	else return "";
}

export default Invoice;