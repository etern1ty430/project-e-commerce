import React, { useState, useEffect } from "react";
import { MDBDataTableV5 } from 'mdbreact';
import Modal from 'react-modal';
import { faFacebook, faTwitter, faLine, faSnapchat, faYahoo } from "@fortawesome/free-brands-svg-icons"
import { faTimes } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const Posts = ({dataSetTable, dataLaporanHeader, dataLaporanDetail, dataTable, idTransPopup, isModalInvoice, isModalDetail, onCloseDetail, onCloseInvoice }) => {
    function handleClosePopupDetail() {
      onCloseDetail();
    }
    function handleClosePopupInvoice() {
      onCloseInvoice();
    }

    useEffect(() => {
        //getTransactions();
    });

    return (
        <>
          { dataTable && dataTable.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTable } />
          : 'No orders history.' }
            {/* MENU POPUP DETAIL*/}
            <Modal isOpen={isModalDetail} onRequestClose={() => handleClosePopupDetail() } style={{overlay:{backgroundColor:'gray'},content:{color:'black', width:'50%', marginLeft:'auto', marginRight:'auto'}}}>
              
              <h1 style={{textAlign:"center",fontFamily: "'Comfortaa', cursive"}}>
                Laporan Detail
                <FontAwesomeIcon icon={ faTimes } style={{position:'fixed', right:'26%', height:'3%', width:'3%'}} onClick={() => handleClosePopupDetail() }/>
              </h1><hr style={{backgroundColor:'black'}}/>
              {
                dataLaporanHeader.map(item => {
                  if(item.id_trans == idTransPopup)
                  {
                    let date = new Date(item.tanggal_beli);
                    // date.setDate(date.getDate()-1);
                    console.log(date);
                    return (
                      <div> 
                        ID Transaksi        <label style={{textIndent:"41.5px"}}>: &nbsp; {item.id_trans}</label> <br/>
                        Tanggal Transaksi   <label style={{textIndent:"0px"}}>: &nbsp; {date.toDateString()}</label> <br/>
                        Jumlah Transaksi    <label style={{textIndent:"4.5px"}}>: &nbsp; {item.jumlah_beli}</label> <br/>
                        Total Transaksi     <label style={{textIndent:"22px"}}>: &nbsp; Rp.{item.total_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</label> <br/><br/>
    
                        Nama Pembeli        <label style={{textIndent:"27.5px"}}>: &nbsp; {item.nama_pembeli}</label> <br/>
                        Nama Penerima       <label style={{textIndent:"17px"}}>: &nbsp; {item.nama_penerima}</label> <br/> 
                        Alamat              <label style={{textIndent:"77.5px"}}>: &nbsp; {item.alamat_pengiriman}</label> <br/>
                        Kode Pos            <label style={{textIndent:"63px"}}>: &nbsp; {item.kode_pos}</label> <br/> 
                        No Telp             <label style={{textIndent:"73.5px"}}>: &nbsp; {item.no_telp}</label> <br/> 
                        Kota                <label style={{textIndent:"95px"}}>: &nbsp; {item.nama_kota}</label> <br/>
                        {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == idTransPopup)
                                return (
                                  <div> <hr/>
                                    Nama Item     <label style={{textIndent:"50px"}}>: {itemDetail.nama_item} ( {itemDetail.brand_item} - {itemDetail.kategori_item} )</label><br/> 
                                    Ukuran Item   <label style={{textIndent:"40.5px"}}>: {itemDetail.ukuran_item}</label> <br/> 
                                    Jumlah Item   <label style={{textIndent:"40px"}}>: {itemDetail.jumlah_item}</label> <br/> 
                                    Subtotal      <label style={{textIndent:"66px"}}>: Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- (Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-/pcs)</label><br/>
                                  </div>
                                );
                            })
                        }
                      </div>
                  )};
                })
              }
              
            </Modal>


            {/* MENU POPUP INVOICE*/}
            <Modal isOpen={isModalInvoice} onRequestClose={() => handleClosePopupInvoice() }  style={{border:"2px solid black", overlay:{backgroundColor:'gray'},content:{color:'black', backgroundColor:'rgb(240,240,240)', padding:'0 5% 0 10%', boxShadow:'inset 50px 0 0 0 rgb(220,220,220)', letterSpacing:'1px', fontFamily:'arial'}}}>
              <FontAwesomeIcon icon={ faTimes } style={{position:'fixed', right:'50', height:'3%', width:'3%'}} onClick={() => handleClosePopupInvoice() }/>
              <div class="row">
                {/* -----INVOICE AND DATE----- */}
                <div class="col-sm-6" style={{backgroundColor:"none"}}>
                  <br/><br/><br/>
                  <p style={{fontSize:"48px"}}>
                    {/* <label style={{letterSpacing:"0px"}}>____</label> &nbsp; */}
                    <label style={{letterSpacing:"5px"}}> I N V O I C E</label>
                  </p>
                  {
                    dataLaporanHeader.map((item) => {
                      if(item.id_trans == idTransPopup)
                      {
                        let date = new Date(item.tanggal_beli);
                        // date.setDate(date.getDate()-1);
                        return (
                          <div style={{letterSpacing:'2px', fontSize:'20px'}}>
                            No. #{item.id_trans} // {date.toDateString()}
                          </div>
                        )
                      }
                    })
                  }
                </div>
                {/* -----LOGO----- */}
                <div class="col-sm-6" style={{textAlign:'right', backgroundColor:'none'}}>
                  <div style={{backgroundColor:'rgb(65,65,65)', width:"35%", height:"100%", float:'right', color:'white', letterSpacing:'1px'}}>
                    <center>
                      <img src={ process.env.PUBLIC_URL+'/icons/icon1transparent.png'} style={{width:"70%"}}></img>
                      <p style={{fontSize:"30px", marginBottom:'-10px'}}>AlasKakiQu</p>
                      <p>Good design is a language, <br/>not a style</p>
                    </center>
                  </div>
                </div>
              </div>

              <br/><br/><br/><br/>
              <div class="row">
                {/* -----HEADER INVOICE----- */}
                <div class="col-sm-6" style={{backgroundColor:"none", letterSpacing:'2px'}}>
                  <p style={{marginBottom:'0', fontSize:'19px'}}><b>Billing To :</b></p>
                  {
                    dataLaporanHeader.map((item) => {
                      if(item.id_trans == idTransPopup)
                      {
                        return(
                          <div>
                            {item.nama_pembeli}<br/>
                            {item.alamat_pengiriman}, {item.kode_pos}<br/>
                          </div>
                        )
                      }
                    })
                  }
                </div>
                {/* -----TOTAL INVOICE----- */}
                <div class="col-sm-6" style={{backgroundColor:"none", textAlign:'right', letterSpacing:'2px'}}>
                  <p style={{marginBottom:'0', fontSize:'20px'}}>Total Due</p>
                  {
                    dataLaporanHeader.map((item) => {
                      if(item.id_trans == idTransPopup)
                      {
                        return(
                          <div style={{fontSize:"32px"}}>
                            <b>Rp.{parseInt(item.total_harga).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b>
                          </div>
                        )
                      }
                    })
                  }
                </div>
              </div>


              
              {/* -----ISI INVOICE----- */}
              <br/><br/>
              {
                dataLaporanHeader.map((item) => {
                  if(item.id_trans == idTransPopup)
                  {
                    return (
                      <div key={item.id_trans}>
                        {/* HEADER TABEL */}
                        <hr style={{backgroundColor:'black'}}/><b>
                        <div style={{float:"left",width:"27%"}}> SNEAKERS </div>
                        <div style={{float:"left",width:"13%"}}> BRAND </div>
                        <div style={{float:"left",width:"10%", textAlign:"right"}}> SIZE </div>
                        <div style={{float:"left",width:"10%", textAlign:"right"}}> QUANTITY </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> PRICE </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> SUBTOTAL </div>
                        </b><p style={{clear:"both"}}/>
                        <hr style={{backgroundColor:'black'}}/>


                        {/* DETAIL PEMBELIAN */}
                        <div style={{float:"left",width:"27%"}}> 
                          {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == item.id_trans)
                                return (
                                    <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.nama_item}</p><hr/></div>
                                );
                            })
                          }
                        </div>
                        <div style={{float:"left",width:"13%"}}> 
                          {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == item.id_trans)
                                return (
                                  <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.brand_item}</p><hr/></div>
                                );
                            })
                          }
                        </div>
                        <div style={{float:"left",width:"10%", textAlign:"right"}}> 
                          {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == item.id_trans)
                                return (
                                  <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.ukuran_item}</p><hr/></div>
                                );
                            })
                          }
                        </div>
                        <div style={{float:"left",width:"10%", textAlign:"right"}}> 
                          {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == item.id_trans)
                                return (
                                  <div><p style={{padding:'10px 0 10px 0'}}>{itemDetail.jumlah_item}</p><hr/></div>
                                );
                            })
                          }
                        </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                          {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == item.id_trans)
                                return (
                                  <div><p style={{padding:'10px 0 10px 0'}}>Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p><hr/></div>
                                );
                            })
                          }
                          <div style={{minHeight:"50px"}}><b> SUB-TOTAL </b></div>
                          <div style={{minHeight:"50px"}}> SHIPPING COST (RAJAONGKIR) </div>
                          <div style={{color:'red'}}> DISCOUNT ({item.diskon_promo}%) </div>
                          <div style={{color:'red'}}> {item.deskripsi_promo} </div>
                        </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                          {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == item.id_trans)
                                return (
                                  <div><p style={{padding:'10px 0 10px 0'}}>Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p><hr/></div>
                                );
                            })
                          }
                          <div style={{minHeight:"50px"}}><b>Rp.{(parseInt(item.total_harga) - parseInt(item.harga_shipping) + parseInt(item.total_promo)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                          <div style={{minHeight:"50px"}}>Rp.{item.harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                          <div style={{color:'red'}}> Rp.{item.total_promo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </div><br/>
                        </div>
                        <p style={{clear:"both"}}/>


                        {/* GRAND TOTAL */}
                        <hr style={{backgroundColor:'black'}}/>
                        <div style={{float:"left",width:"60%", textAlign:"right", minHeight:"30px"}}/>
                        <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px", fontSize:'20px'}}><b>GRAND TOTAL</b></div>
                        <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px", fontSize:'20px'}}><b>Rp.{item.total_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                        <p style={{clear:"both"}}/>
                        <hr style={{backgroundColor:'black'}}/><br/><br/><br/>
                      </div>
                    )}
                })
              }

              {/* FOOTER INVOICE */}
              <div class="row" style={{letterSpacing:'0px'}}>
                <div class="col-sm-6" style={{textAlign:'right'}}>
                  <p>73-77 Ngagel Jaya Utara, 60123</p>
                  <p style={{marginTop:'-1%'}}>Surabaya, Jawa Timur</p>
                  <p style={{marginTop:'-1%'}}>contact@alaskakiqu.com</p>
                  <p style={{marginTop:'-1%'}}>+6281 2345 6789</p>
                </div>
                <div class="col-sm-6" style={{fontSize:'33px'}}>
                  <b>
                    <p style={{marginTop:'0.5%'}}>THANKS FOR</p>
                    <p style={{marginTop:'-0.5%'}}>YOUR BUSINESS</p>
                  </b>
                </div>
              </div>

              <center style={{marginBottom:'5%'}}>
                <br/>
                <b>www.alaskakiqu.com</b>
                <br/>
                <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block', marginTop:'1%'}}>
                  <i class="circleIcon"><FontAwesomeIcon icon={ faFacebook } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                </span>&nbsp;&nbsp;
                <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                  <i class="circleIcon"><FontAwesomeIcon icon={ faTwitter } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                </span>&nbsp;&nbsp;
                <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                  <i class="circleIcon"><FontAwesomeIcon icon={ faYahoo } style={{height:"70%", width:"70%", marginTop:'25%'}}/></i>
                </span>&nbsp;&nbsp;
                <span style={{height:'35px', width:'35px', backgroundColor:'black', borderRadius:'50%', display:'inline-block'}}>
                  <i class="circleIcon"><FontAwesomeIcon icon={ faSnapchat } style={{height:"70%", width:"70%", marginTop:'20%'}}/></i>
                </span>&nbsp;&nbsp;
              </center>
              
            </Modal>
                
        </>
    )
};

export default Posts