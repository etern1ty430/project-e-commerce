import React, { useState, useEffect } from 'react';
import {Doughnut} from 'react-chartjs-2';
// import { ChartModule } from 'react-chartjs-2';
import 'chartjs-plugin-labels';

const chartPie = ({ chartData, location, legendPosition }) => {
  const pageNumbers = [];

  return (
    <div style={{position: "relative", width: "50%", height: 400,float:"left"}}>
        <Doughnut
          data={chartData}

          options={{
            title:{
              display:true,
              text:'Pie Diagram',
              fontSize:25,
              fontFamily: "'Comfortaa', cursive"
            },
            legend:{
              display:true,
              position:legendPosition
            },
            animation:{
              animateScale:true
            },
            plugins: {
              labels: [
                {
                  render: 'percentage',
                  position: 'outside'
                }
              ]
            }
            // plugins: {
            //   labels: {
            //     // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
            //     render: 'value',
         
            //     // precision for percentage, default is 0
            //     precision: 0,
         
            //     // identifies whether or not labels of value 0 are displayed, default is false
            //     showZero: true
            //   }
            // }
          }}
        />
      </div>
  );
};

export default chartPie;