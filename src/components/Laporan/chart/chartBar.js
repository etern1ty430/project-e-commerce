import React, { useState, useEffect } from 'react';
import {Bar} from 'react-chartjs-2';

const chartBar = ({ chartData, location, legendPosition }) => {
  const pageNumbers = [];

  return (
    <div style={{position: "relative", width: "50%", height: 400,float:"left"}}>
        <Bar
          data={chartData}
          options={{
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Quantity Sold Product',
                        fontSize: "20",
                        fontFamily: "'Comfortaa', cursive"
                    }
                }],
                xAxes: [{
                    fontSize: 10,
                    scaleLabel: {
                        display: true,
                        labelString: 'Product Name',
                        fontSize: "20",
                        fontFamily: "'Comfortaa', cursive"
                    }
                }]
            },
            title:{
              display:true,
              text:'Bar Chart',
              fontSize:25,
              fontFamily: "'Comfortaa', cursive"
            },
            legend:{
              display:false,
              position:legendPosition
            },
            plugins: {
              labels: {
                // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                render: 'value',
         
                // precision for percentage, default is 0
                precision: 0,
         
                // identifies whether or not labels of value 0 are displayed, default is false
                showZero: true
              }
            }
          }}
        />
      </div>
  );
};

export default chartBar;