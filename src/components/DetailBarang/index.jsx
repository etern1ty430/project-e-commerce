﻿import React, { useState, useEffect } from 'react';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch,faCartPlus,faStar } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ReactTooltip from 'react-tooltip';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const DetailBarang = ({ match }) => {
  let token=window.sessionStorage.getItem("user");
  const [detailalaskaki, setDetailalaskaki] = useState([]);
  const [detailukuran, setDetailukuran] = useState([]);
  const [selectukuran, setSelectukuran] = useState(-1);
  const [maxstock, setMaxstock] = useState(0);
  const [defaultstock, setDefaultstock] = useState(0);
  const [selectbutton, setSelectbutton] = useState("");
	const history= useHistory();

  async function getDetailAlasKaki(){
    try {
      const fetchAPI= await fetch(`/apiAlasKaki/getdetailalaskaki/${match.params.id_sneaker}`, { 
          method: 'GET'
      });
      
      const result= await fetchAPI.json();
      setDetailalaskaki(result.detail);
      setDetailukuran(result.ukuran);
    } catch (error) {
        console.error(error);
    }
  }
  function cekstatus(){
    if(detailalaskaki.status_sneaker=="1"){return(<h3>Status : Avalaible</h3>);}
    else{return(<h3>Status : Non-Avalaible</h3>);}
  }
  function handlePilihUkuran(ukuran,max){
    setSelectukuran(ukuran);
    setMaxstock(max);
    setDefaultstock(0);
  }
  async function handleSubmit(e){
    e.preventDefault();
    let id_sneaker = detailalaskaki.id_sneaker;
    let jumlah_sneaker = e.target.amount.value;
    let ukuran_sneaker = selectukuran;
    let harga_satuan = detailalaskaki.harga_sneaker;
    let subtotal = parseInt(jumlah_sneaker)*parseInt(harga_satuan);
    
    if(selectbutton=="cart"){
      if (selectukuran != -1 && jumlah_sneaker != 0) {
        try {
          const fetchAPI= await fetch('/apiUser/addtocart', {
              method: 'POST',
              headers: { 
                'Content-Type' : 'application/json',
                'x-access-token': token  
              },
              body: JSON.stringify({
                id_sneaker: id_sneaker,
                jumlah_sneaker: jumlah_sneaker,
                ukuran_sneaker: ukuran_sneaker,
                harga_satuan: harga_satuan,
                subtotal: subtotal,
                max: maxstock
              })
          });
          const res= await fetchAPI.json();
  
          if (res.status !== 200) {
              return alert(`Failed to Add Cart.\n${res.message}`);
          }
          alert(res.message);
        } catch (error) {
            return console.error(error);
        }
      }
      else{
        return alert('Size Not Selected Or Amount Is 0');
      }
    }
    else if(selectbutton=="wishlist"){
      if(window.sessionStorage.getItem("user")!=null){
        try {
          const fetchAPI= await fetch('/apiUser/addtowishlist', {
              method: 'POST',
              headers: { 
                'Content-Type' : 'application/json',
                'x-access-token': token  
              },
              body: JSON.stringify({
                id_sneaker: id_sneaker
              })
          });
          const res= await fetchAPI.json();
  
          if (res.status !== 200) {
              return alert(`Failed to Add Wishlist.\n${res.message}`);
          }
          alert(res.message);
        } catch (error) {
            return console.error(error);
        }
      }
      else{
        if (window.confirm("You Not Login Yet, Login?")) {
          history.push(`/login`);
          return window.location.reload();
        } 
      }
    }
  }
  useEffect(() => {
    getDetailAlasKaki();
  }, []);
	return (
    <>
    <br/><br/>
      <form onSubmit={handleSubmit}>
        <div class="row">
          <div class="col-sm-2"></div>
          <div class="col-sm-8">
            <div class="row">
              <div class="col-sm-6">
                <h1>{detailalaskaki.nama_sneaker} </h1>
                <br/>
                <img className="card-img-top" style={{width:"70%",border:"3px solid black",borderRadius:"20px"}} src={ process.env.PUBLIC_URL+'/images/sneakers/'+"AirForce1Peach_detail.jpg" } />
              </div>
              <div class="col-sm-6">
                <br/><br/><br/>
                <div class="row">
                  <div class="col-sm-6">
                    <h3>Harga : {"Rp."+detailalaskaki.harga_sneaker}</h3><br/>
                    <h3>Brand : {detailalaskaki.brand_sneaker}</h3><br/>
                    <h3>Size & Fit : </h3><br/>
                  </div>
                  <div class="col-sm-6">
                    {cekstatus()}
                  </div>
                </div>
                {
                  detailukuran.length?
                  detailukuran.map((item, index) => {
                    return(
                        <button type="button" onClick={() => handlePilihUkuran(item.ukuran_sneaker,item.stok_sneaker)} className="btn btn-light" style={{width:"50px",height:"50px",display:"inline-block",marginRight:"2%",cursor:"pointer",borderRadius:0}}>
                            {detailukuran[index].ukuran_sneaker}
                        </button>
                    );
                  })
                  : <strong style={{color:"black"}}>Size Empty </strong>
                }
                <br/>
                <br/>
                <h3>Amount : </h3><br/>
                <div class="form-group">
                  <input type="number" id="amount" class="form-control" onChange={(e) => setDefaultstock(e.target.value)} value={defaultstock} min={0} max={maxstock} placeholder="Enter Quantity"/>
                </div>
                <br/>
                <button type="submit" onClick={() => setSelectbutton("cart")} id="cart" class="btn btn-dark btn-block"><FontAwesomeIcon icon={ faCartPlus } style={{width:"18px",height:"20px"}}/>&nbsp; ADD TO CART</button>
                <button type="submit" onClick={() => setSelectbutton("wishlist")} id="wishlist" class="btn btn-dark btn-block"><FontAwesomeIcon icon={ faStar } style={{width:"18px",height:"20px"}}/>&nbsp; ADD TO WISHLIST</button>
              </div>
            </div>
          </div>
          <div class="col-sm-2"></div>
        </div>
      </form>
    <br/><br/>
    </>
	);
}

export default DetailBarang;