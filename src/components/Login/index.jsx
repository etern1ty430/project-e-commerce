﻿import React from "react";
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import { useHistory } from 'react-router-dom';
import './index.css';

const Login = () => {
  const history= useHistory();

  async function processLogin(e){
    e.preventDefault();
    let username = e.target.username.value;
    let password = e.target.password.value;

    let fields= {
      username: username,
      password: password
    }

    if (!Object.keys(fields).every(key => fields[key] !== '')) {
        return alert('All field required.');
    }

    if(username=="admin" && password=="admin"){
      history.push(`/admin`);
      return window.location.reload();
    }
    else{
      try {
        const fetchAPI= await fetch('/apiUser/login', {
            method: 'POST',
            headers: { 'Content-Type' : 'application/json' },
            body: JSON.stringify({
                username: username,
                password: password
            })
        });
        const res= await fetchAPI.json();

        if (res.status !== 200) {
            return alert(`Failed to login.\n${res.message}`);
        }
        window.sessionStorage.setItem('user',res.userLogged.token);
        alert(res.message);
        history.push(`/`);
        return window.location.reload();
    } catch (error) {
        return console.error(error);
    }
    }
  }
	return (
    <div class="container-login">
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <br/><br/><br/>
          <center>
            <div style={{display:"inline-block"}}>
              <a href="/login" style={{color:"black",textDecoration:"underline",font:"normal 20px 'Cookie', cursive",marginLeft:"-10%",marginRight:"20%"}}>Login</a>
              <a href="/register" style={{color:"black",textDecoration:"none",font:"normal 20px 'Cookie', cursive"}}>Register</a>
            </div><br/><br/><br/>
          </center>
            
            <form onSubmit={processLogin}>
              <div class="form-group">
                <label>Username</label>
                <input class="form-control" id="username" placeholder="Enter Username"/>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter Password"/>
              </div>
              <br/>
              <button id="btn-submit-login" type="submit" style={{borderRadius:"0px"}} class="btn btn-dark">Submit</button> 
            </form>
        </div>
        <div class="col-sm-2"></div>
      </div>
    </div>
	);
}

export default Login;