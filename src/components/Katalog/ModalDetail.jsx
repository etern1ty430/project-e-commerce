import React, { useState, useEffect } from 'react';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch,faAngleDown,faStar,faCartPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ReactTooltip from 'react-tooltip';
import './index.css';
import Collapse from 'react-bootstrap/Collapse'
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { SideBySideMagnifier } from "react-image-magnifiers";


const ModalDetail = ({show,setShow,detailalaskaki}) => {
  const history= useHistory();
  let token=window.sessionStorage.getItem("user");
  //const [detailalaskaki, setDetailalaskaki] = useState([]);

  const [selectukuran, setSelectukuran] = useState(-1);
  const [maxstock, setMaxstock] = useState(0);
  const [defaultstock, setDefaultstock] = useState(0);
  const [selectbutton, setSelectbutton] = useState("");
  const [styleCart, setStyleCart] = useState("btn btn-dark btn-block");

  async function handleSubmit(e){
    e.preventDefault();
    let id_sneaker = detailalaskaki.id_sneaker;
    let jumlah_sneaker = e.target.amount.value;
    let ukuran_sneaker = selectukuran;
    let harga_satuan = detailalaskaki.harga_sneaker;
    let subtotal = parseInt(jumlah_sneaker)*parseInt(harga_satuan);
    if(selectbutton=="cart"){
        //cart dengan database jika ada user
            if (selectukuran != -1 && jumlah_sneaker != 0) {
                if(window.sessionStorage.getItem("user")!=null){
                    try {
                        const fetchAPI= await fetch('/apiUser/addtocart', {
                            method: 'POST',
                            headers: { 
                            'Content-Type' : 'application/json',
                            'x-access-token': token  
                            },
                                body: JSON.stringify({
                                id_sneaker: id_sneaker,
                                jumlah_sneaker: jumlah_sneaker,
                                ukuran_sneaker: ukuran_sneaker,
                                harga_sneaker: harga_satuan,
                                subtotal: subtotal,
                                max: maxstock
                            })
                        });
                        const res= await fetchAPI.json();
                
                        if (res.status !== 200) {
                            return alert(`Failed to Add Cart.\n${res.message}`);
                        }
                        alert(res.message);
                    } catch (error) {
                        return console.error(error);
                    }
                }
                //cart dengan session jika belum login
                else{
                    if (!window.sessionStorage.getItem("cart")) {
                        let sessionCart=[];
                        sessionCart.push({
                            id_sneaker: id_sneaker,
                            nama_sneaker: detailalaskaki.nama_sneaker,
                            gambar_sneaker: detailalaskaki.gambar,
                            jumlah_sneaker: jumlah_sneaker,
                            ukuran_sneaker: ukuran_sneaker,
                            harga_sneaker: harga_satuan,
                            stok_sneaker: maxstock,
                            subtotal: subtotal
                        });
                        window.sessionStorage.setItem('cart', JSON.stringify(sessionCart));
                        alert("Add To Cart Success.");
                    }
                    else{
                        let sessionCart = JSON.parse(window.sessionStorage.getItem("cart"));
                        let cek = -1;
                        for (let i = 0; i < sessionCart.length; i++) {
                            if(sessionCart[i].id_sneaker == id_sneaker && sessionCart[i].ukuran_sneaker == ukuran_sneaker){
                                cek=i;
                            }
                        }
                        //kembar
                        if(cek!=-1){
                            let combine=parseInt(sessionCart[cek].jumlah_sneaker)+parseInt(jumlah_sneaker);
                            if(combine>maxstock){
                                alert("Exceed Stock.");
                            }
                            else{
                                sessionCart[cek].jumlah_sneaker=combine;
                                sessionCart[cek].subtotal=sessionCart[cek].jumlah_sneaker*sessionCart[cek].harga_satuan;
                                window.sessionStorage.setItem('cart', JSON.stringify(sessionCart));
                                alert("Update Cart Success.");
                            }
                        }
                        //tidak kembar
                        else{
                            sessionCart.push({
                                id_sneaker: id_sneaker,
                                nama_sneaker: detailalaskaki.nama_sneaker,
                                gambar_sneaker: detailalaskaki.gambar,
                                jumlah_sneaker: jumlah_sneaker,
                                ukuran_sneaker: ukuran_sneaker,
                                harga_sneaker: harga_satuan,
                                stok_sneaker: maxstock,
                                subtotal: subtotal
                            });
                            window.sessionStorage.setItem('cart', JSON.stringify(sessionCart));
                            alert("Add To Cart Success.");
                        }
                    }
                    console.log(window.sessionStorage.getItem("cart"));
                }
            }
            else{
              return alert('Size Not Selected Or Amount Is 0');
            }
        
    }
    else if(selectbutton=="wishlist"){
      if(window.sessionStorage.getItem("user")!=null){
        try {
          const fetchAPI= await fetch('/apiUser/addtowishlist', {
              method: 'POST',
              headers: { 
                'Content-Type' : 'application/json',
                'x-access-token': token  
              },
              body: JSON.stringify({
                id_sneaker: id_sneaker
              })
          });
          const res= await fetchAPI.json();
  
          if (res.status !== 200) {
              return alert(`Failed to Add Wishlist.\n${res.message}`);
          }
          alert(res.message);
        } catch (error) {
            return console.error(error);
        }
      }
      else{
        if (window.confirm("You Not Login Yet, Login?")) {
          history.push(`/login`);
          return window.location.reload();
        } 
      }
    }
  }
  function cekCart(){
    if(detailalaskaki.ukuran && detailalaskaki.status_sneaker=="1"){setStyleCart("btn btn-dark btn-block");}
    else{setStyleCart("btn btn-danger btn-block");}
  }
  function cekstatus(){
    if(detailalaskaki.status_sneaker=="1"){return(<p style={{marginLeft:"5%"}}>Status : Avalaible</p>);}
    else{return(<p style={{marginLeft:"5%"}}>Status : Non-Avalaible</p>);}
  }
  function handlePilihUkuran(ukuran,max){
    setSelectukuran(ukuran);
    setMaxstock(max);
    setDefaultstock(0);
    if (max>0) {setStyleCart("btn btn-dark btn-block");}
    else{setStyleCart("btn btn-danger btn-block");}
  }
  function handleClose() { 
    setShow(false); 
    setDefaultstock(0);
    setMaxstock(0);
  }
  useEffect(() => {
    cekCart();
  }, [detailalaskaki.ukuran]);
    return (
    <Modal class="container-modaldetail" size="lg" style={{ fontSize: '15pt', opacity:1 }}
        show={ show } 
        onHide={ handleClose }>
            { detailalaskaki ? 
                <> 
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Detail Alas Kaki  
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <div class="modaldetail-dekstop">
                        <div className="mt-4">
                            <form onSubmit={handleSubmit}>
                            <div className="card-title"
                                style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                <h3 className="text-center text-white p-3">
                                    {detailalaskaki.nama_sneaker}
                                </h3>
                            </div>
                            <center>
                            <SideBySideMagnifier imageSrc={process.env.PUBLIC_URL+'/images/sneakers/' + detailalaskaki.gambar} className="w-70 mb-3" alwaysInPlace="true" style={{ width:"70%", marginLeft:"auto", marginRight:"auto",border:"3px solid black"}}/>
                            {/* <img className="card-img-top" style={{width:"70%",border:"3px solid black",borderRadius:"20px"}} src={ process.env.PUBLIC_URL+'/images/sneakers/'+detailalaskaki.gambar } /> */}
                            </center>
                            <br/>
                            <div class="row" style={{marginLeft:"13%"}}>
                                <div class="col-sm-6">
                                <p>{detailalaskaki.harga_sneaker ? "Rp."+detailalaskaki.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : ''},-</p>
                                <p>{detailalaskaki.brand_sneaker}</p>
                                <p>Size & Fit : </p>
                                </div>
                                <div class="col-sm-6">
                                {cekstatus()}
                                </div>
                            </div>

                            <div style={{paddingLeft:"15%",paddingRight:"15%",paddingBottom:"5%"}}>
                            {
                                detailalaskaki.ukuran?
                                detailalaskaki.ukuran.map((item, index) => {
                                    if(item.ukuran_sneaker==selectukuran){
                                        return(
                                            <div style={{width:"50px",height:"50px",display:"inline-block",marginRight:"2%",cursor:"pointer",borderRadius:0}}>
                                                <button type="button" data-tip={item.stok_sneaker+" Available"} onClick={() => handlePilihUkuran(item.ukuran_sneaker,item.stok_sneaker)} className="btn btn-dark" >
                                                {item.ukuran_sneaker}
                                                </button>
                                                <ReactTooltip />
                                            </div>
                                        );
                                    }
                                    else{
                                        return(
                                            <div style={{width:"50px",height:"50px",display:"inline-block",marginRight:"2%",cursor:"pointer",borderRadius:0}}>
                                                <button type="button" data-tip={item.stok_sneaker+" Available"} onClick={() => handlePilihUkuran(item.ukuran_sneaker,item.stok_sneaker)} className="btn btn-light" >
                                                {item.ukuran_sneaker}
                                                </button>
                                                <ReactTooltip />
                                            </div>
                                        );
                                    }
                                })
                                : <h6 style={{color:"red",fontWeight:"bold"}}>Size Empty </h6>
                            }
                            <p>Amount : </p>
                            <div class="form-group">
                                <input type="number" id="amount" class="form-control" onChange={(e) => setDefaultstock(e.target.value)} value={defaultstock} min={0} max={maxstock} placeholder="Enter Quantity"/>
                            </div>
                            <br/>
                            <button type="submit" onClick={() => setSelectbutton("cart")} id="cart" class={styleCart}><FontAwesomeIcon icon={ faCartPlus } style={{width:"18px",height:"20px"}}/>&nbsp; ADD TO CART</button>
                            <button type="submit" onClick={() => setSelectbutton("wishlist")} id="wishlist" class="btn btn-dark btn-block"><FontAwesomeIcon icon={ faStar } style={{width:"18px",height:"20px"}}/>&nbsp; ADD TO WISHLIST</button>
                            </div>
                            <hr/>
                            </form>     
                        </div>
                    </div>
                    <div class="modaldetail-mobile" style={{display:"none"}}>
                        <div className="mt-4">
                            <form onSubmit={handleSubmit}>
                            <div className="card-title"
                                style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                <h3 className="text-center text-white p-3">
                                    {detailalaskaki.nama_sneaker}
                                </h3>
                            </div>
                            <center>
                            <img className="card-img-top" style={{width:"70%",border:"3px solid black",borderRadius:"20px"}} src={ process.env.PUBLIC_URL+'/images/sneakers/'+detailalaskaki.gambar } />
                            </center>
                            <br/>
                            <div class="row" style={{marginLeft:"3%"}}>
                            {cekstatus()}
                                <div class="col-sm-6">
                                <p>Harga : {detailalaskaki.harga_sneaker ? "Rp."+detailalaskaki.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : ''},-</p>
                                <p>Brand : {detailalaskaki.brand_sneaker}</p>
                                <p>Size & Fit : </p>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div>

                            <div style={{paddingLeft:"15%",paddingRight:"15%",paddingBottom:"5%"}}>
                            {
                                detailalaskaki.ukuran?
                                detailalaskaki.ukuran.map((item, index) => {
                                    if(item.ukuran_sneaker==selectukuran){
                                        return(
                                            <div style={{width:"50px",height:"50px",display:"inline-block",marginRight:"10%",cursor:"pointer",borderRadius:0,marginLeft:"-10%"}}>
                                                <button type="button" data-tip={item.stok_sneaker+" Available"} onClick={() => handlePilihUkuran(item.ukuran_sneaker,item.stok_sneaker)} className="btn btn-dark" >
                                                {item.ukuran_sneaker}
                                                </button>
                                                <ReactTooltip />
                                            </div>
                                        );
                                    }
                                    else{
                                        return(
                                            <div style={{width:"50px",height:"50px",display:"inline-block",marginRight:"10%",cursor:"pointer",borderRadius:0,marginLeft:"-10%"}}>
                                                <button type="button" data-tip={item.stok_sneaker+" Available"} onClick={() => handlePilihUkuran(item.ukuran_sneaker,item.stok_sneaker)} className="btn btn-light" >
                                                {item.ukuran_sneaker}
                                                </button>
                                                <ReactTooltip />
                                            </div>
                                        );
                                    }
                                })
                                : <h6 style={{color:"red",fontWeight:"bold",marginLeft:"-10%"}}>Size Empty </h6>
                            }
                            <div style={{marginLeft:"-10%"}}>
                                <p>Amount : </p>
                                <div class="form-group">
                                    <input type="number" id="amount" class="form-control" onChange={(e) => setDefaultstock(e.target.value)} value={defaultstock} min={0} max={maxstock} placeholder="Enter Quantity"/>
                                </div>
                            </div>
                            <br/>
                            <button type="submit" onClick={() => setSelectbutton("cart")} id="cart" class={styleCart}><FontAwesomeIcon icon={ faCartPlus } style={{width:"18px",height:"20px"}}/>&nbsp; ADD TO CART</button>
                            <button type="submit" onClick={() => setSelectbutton("wishlist")} id="wishlist" class="btn btn-dark btn-block"><FontAwesomeIcon icon={ faStar } style={{width:"18px",height:"20px"}}/>&nbsp; ADD TO WISHLIST</button>
                            </div>
                            <hr/>
                            </form>     
                        </div>
                    </div>
                    </Modal.Body>
                </> 
            : '' } 
        </Modal>
	);
}

export default ModalDetail;