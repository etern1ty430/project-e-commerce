﻿import React, { useState, useEffect } from 'react';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch,faAngleDown,faStar,faCartPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ReactTooltip from 'react-tooltip';
import './index.css';
import Collapse from 'react-bootstrap/Collapse'
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import ModalDetail from './ModalDetail';
import StarRatings from 'react-star-ratings';
import ModalReview from '../Home/ModalReview';
import ItemPreview from '../Home/ItemPreview';

const Katalog = ({ match }) => {
  const history= useHistory();
  let token=window.sessionStorage.getItem("user");
  const [alaskaki, setAlaskaki] = useState([]);

  const [ show, setShow ] = useState(false);
  const [ selectedsneaker, setSelectedsneaker ]= useState("");
  const [datareview, setDatareview] = useState([]);
  const [ showreview, setShowreview ] = useState(false);
  const [detailalaskaki, setDetailalaskaki] = useState([]);
  async function getAlasKaki(){
    if(match.params.jenis!="semua"){
      try {
        const fetchAPI= await fetch(`/apiAlasKaki/getalaskaki/${match.params.kategori}/${match.params.jenis}`, { 
            method: 'GET'
        });
        
        const result= await fetchAPI.json();
        result.alaskaki= result.alaskaki.map(item => ({
          id_sneaker: item.id_sneaker,
          id_kategori: item.id_kategori,
          nama_sneaker: item.nama_sneaker,
          jenis_sneaker: item.jenis_sneaker,
          brand_sneaker: item.brand_sneaker,
          harga_sneaker: item.harga_sneaker,
          status_sneaker: item.status_sneaker,
          gambar: item.gambar,
          ukuran:[],
          rating:[]
        }));
        for (let i = 0; i < result.alaskaki.length; i++) {
          result.alaskaki[i].ukuran=result.ukuran[i];
        }
        for (let i = 0; i < result.alaskaki.length; i++) {
          result.alaskaki[i].rating=result.rating[i];
        }
        console.log(result.alaskaki);
        setAlaskaki(result.alaskaki);
      } catch (error) {
          console.error(error);
      }
    }
    else{
      try {
        const fetchAPI= await fetch(`/apiAlasKaki/getalaskaki/${match.params.kategori}`, { 
            method: 'GET'
        });
        
        const result= await fetchAPI.json();
        result.alaskaki= result.alaskaki.map(item => ({
          id_sneaker: item.id_sneaker,
          id_kategori: item.id_kategori,
          nama_sneaker: item.nama_sneaker,
          jenis_sneaker: item.jenis_sneaker,
          brand_sneaker: item.brand_sneaker,
          harga_sneaker: item.harga_sneaker,
          status_sneaker: item.status_sneaker,
          gambar: item.gambar,
          jumlah: item.jumlah,
          ukuran:[],
          rating:[]
        }));
        for (let i = 0; i < result.alaskaki.length; i++) {
          result.alaskaki[i].ukuran=result.ukuran[i];
        }
        for (let i = 0; i < result.alaskaki.length; i++) {
          result.alaskaki[i].rating=result.rating[i];
        }
        console.log(result.alaskaki);
        setAlaskaki(result.alaskaki);
      } catch (error) {
          console.error(error);
      }
    }
  }
  async function getreviewalaskaki(){
    const fetchAPI= await fetch(`/apiAlasKaki/getreviewalaskaki/`, {
      method: 'GET'
    });
    const result= await fetchAPI.json();
    console.log(result.review);
    setDatareview(result.review);
  }
  function handleDetailAlasKaki(item){
    setShow(true);
    setDetailalaskaki(item);
  }
  function handleSeeReview(item){
    setShowreview(true);
    setSelectedsneaker(item);
  }
  useEffect(() => {
    getreviewalaskaki();
    getAlasKaki();
  }, []);
	return (
    <>
      <br/>
      <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
          <br/><br/>
          {
            alaskaki.map((item, index) => {
              return(
                <ItemPreview item={item} setShowAlasKaki={setShow} setDetailAlasKaki={setDetailalaskaki} setShowreview={setShowreview} setSelectedsneaker={setSelectedsneaker}/>
              )
          })
          }
          <br/><br/><br/>
        </div>
        <div class="col-sm-2"></div>
      </div>
      <br/>
      <ModalDetail show={show} setShow={setShow} detailalaskaki={detailalaskaki} />
          <ModalReview show={showreview} setShow={setShowreview} selectedsneaker={selectedsneaker} datareview={datareview} />
    </>
  );
}

export default Katalog;