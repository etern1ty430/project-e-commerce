import React, { useState, useEffect } from 'react';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch,faAngleDown,faStar,faCartPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ReactTooltip from 'react-tooltip';
import './index.css';
import Collapse from 'react-bootstrap/Collapse'
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';


const ModalReview = ({show,setShow,selectedsneaker,datareview}) => {
  const history= useHistory();
  let token=window.sessionStorage.getItem("user");
  function handleClose() { 
    setShow(false); 
  }
    return (
        <Modal size="lg" style={{ fontSize: '1.25vw', opacity:1 }}
        show={ show } 
        onHide={ handleClose }>
                <> 
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Body>
                    <div className="mt-4">
                        <div className="card-title"
                            style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                            <h3 className="text-center text-white p-3">
                            List Review 
                            </h3>
                        </div>
                        <center>
                        </center>
                        <br/>
                        <div class="row">
                            <div class="col-sm-1">
                            </div>
                            <div class="col-sm-10">
                                <ul class="list-group">
                                    {
                                        datareview.map((item, index) => {
                                            if(item.id_sneaker==selectedsneaker){
                                                return(
                                                    <li class="list-group-item" style={{border:"1px solid black"}}>
                                                        <div class="row">
                                                            <div class="col-sm-2" style={{borderRight:"1px solid black"}}>
                                                                <div style={{fontSize:"15pt"}}>{item.nama} </div>
                                                                <div style={{fontSize:"10pt"}}>{item.tgl.substr(0,10)}</div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <img className="card-img-top" style={{width:"100%"}} src={ process.env.PUBLIC_URL+'/images/review/'+item.gambar } /> 
                                                            </div>
                                                            <div class="col-sm-5" style={{fontSize:"12pt"}}>
                                                                <StarRatings 
                                                                    rating={parseFloat(item.rating)}
                                                                    starRatedColor="gold"
                                                                    numberOfStars={5}
                                                                    starDimension="23px"
                                                                    starSpacing="0px"
                                                                    name='rating'
                                                                />
                                                                {" "+item.rating}
                                                                <hr/>
                                                                {
                                                                    item.review?
                                                                    <div style={{fontSize:"12pt"}}>
                                                                        <div>Review :</div>
                                                                        {item.review}
                                                                    </div>
                                                                    :""
                                                                }
                                                            </div>
                                                        </div>
                                                    </li>
                                                );
                                            }
                                        })
                                    }
                                </ul>
                            </div>
                            <div class="col-sm-1">
                            </div>
                        </div>
                        <hr/>    
                    </div>
                    </Modal.Body>
                </> 
        </Modal>
	);
}

export default ModalReview;