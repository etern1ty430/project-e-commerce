﻿import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ReactTooltip from 'react-tooltip';
import './index.css';
import { Modal } from 'react-bootstrap';
import ModalDetail from "../Katalog/ModalDetail";
import StarRatings from 'react-star-ratings';
import ModalReview from "./ModalReview";
import ItemPreview from "./ItemPreview";

const Home = () => {
  let token=window.sessionStorage.getItem("user");
  const history= useHistory();
  const [tabHome, setTabHome] = useState(1);
  const [colorTab1, setColorTab1] = useState("black");
  const [colorTab2, setColorTab2] = useState("gray");
  const [colorTab3, setColorTab3] = useState("gray");

  const [datapromo, setDatapromo] = useState([]);
  const [ detailpromo, setDetailpromo ]= useState();
  const [ showpromo, setShowpromo ] = useState(false);
  
  const [dataNewArrival, setDataNewArrival] = useState([]);
  const [dataBestSeller, setDataBestSeller] = useState([]);
  const [ detailAlasKaki, setDetailAlasKaki ]= useState();
  const [ showAlasKaki, setShowAlasKaki ] = useState(false);
  
  const [ selectedsneaker, setSelectedsneaker ]= useState("");
  const [datareview, setDatareview] = useState([]);
  const [ showreview, setShowreview ] = useState(false);
  
  function HandleTab1(){
    setColorTab1("black");
    setColorTab2("gray");
    setColorTab3("gray");
    setTabHome(1);
  }
  function HandleTab2(){
    setColorTab1("gray");
    setColorTab2("black");
    setColorTab3("gray");
    setTabHome(2);
  }
  function HandleTab3(){
    setColorTab1("gray");
    setColorTab2("gray");
    setColorTab3("black");
    setTabHome(3);
  }
  function HandleTampilTab(){
    if(tabHome==1){//Best Seller
      return(
        dataBestSeller.map((item, index) => {
          return(
            <div class="container-item-preview" style={{display:"inline-block",marginLeft:"1.4%",marginTop:"2%"}}>
              <div class="card" id="container-dekstop" style={{width:"18rem"}}>
                <img className="card-img-top" style={{cursor:"pointer"}} onClick={() => handleDetailAlasKaki(item)} src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } />
                      
                <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                <h5 class="card-title" style={{color:"white"}}>{item.nama_sneaker}</h5>
                  <p class="card-text" style={{color:"white"}}>{item.brand_sneaker}</p>
                  <p class="card-text" style={{color:"white"}}>Rp.{item.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p>
                  <p class="card-text" style={{color:"white"}}>
                    Ukuran : <br/>
                    {
                      item.ukuran?
                      item.ukuran.map((item, index) => {
                        return(
                          <>
                            <span class="dot" style={{color:"black"}} data-tip={item.stok_sneaker+" Available"}>{item.ukuran_sneaker}</span>
                            <ReactTooltip />
                          </>
                        );
                      })
                      : <strong style={{color:"red"}}>Stock Empty</strong>
                    }
                  </p>
                  {
                    item.rating?
                    <div>
                      <p class="card-text" style={{color:"white"}}>
                        <StarRatings 
                          rating={parseFloat(item.rating.rating)}
                          starRatedColor="gold"
                          numberOfStars={5}
                          starDimension="23px"
                          starSpacing="0px"
                          name='rating'
                        />
                        {" "+item.rating.rating} ({item.rating.jumlah_review})
                      </p>
                      <p class="card-text" onClick={() => handleSeeReview(item.id_sneaker)} style={{color:"white",fontStyle:"italic",fontSize:"10pt",textDecoration:"underline",cursor:"pointer"}}>See Review</p>
                    </div>
                    :
                    <div>
                      <p class="card-text" style={{color:"white"}}>
                        <StarRatings 
                          rating={0}
                          starRatedColor="gold"
                          numberOfStars={5}
                          starDimension="23px"
                          starSpacing="0px"
                          name='rating'
                        />
                        {" 0"} ({0})
                      </p>
                      <p class="card-text" style={{color:"white",fontStyle:"italic",fontSize:"10pt"}}>No Review</p>
                    </div>
                  }
                  <p class="card-text" style={{color:"white",float:"right"}}>Terjual : {item.jumlah}x</p> 
                  <br/>
                </div>
            </div>
            <div class="card" id="container-mobile" style={{display:"none",width:"15rem",marginLeft:"30%",marginTop:"15%"}}>
                <img className="card-img-top" style={{cursor:"pointer"}} onClick={() => handleDetailAlasKaki(item)} src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } />
                      
                <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                <h6 class="card-title" style={{color:"white"}}>{item.nama_sneaker+" ("+item.brand_sneaker+")"}</h6>
                  <p class="card-text" style={{color:"white"}}>Rp.{item.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p>
                  <p class="card-text" style={{color:"white"}}>
                    Ukuran : <br/>
                    {
                      item.ukuran?
                      item.ukuran.map((item, index) => {
                        return(
                          <>
                            <span class="dot" style={{color:"black"}} data-tip={item.stok_sneaker+" Available"}>{item.ukuran_sneaker}</span>
                            <ReactTooltip />
                          </>
                        );
                      })
                      : <strong style={{color:"red"}}>Stock Empty</strong>
                    }
                  </p>
                  {
                    item.rating?
                    <div>
                      <p class="card-text" style={{color:"white"}}>
                        <StarRatings 
                          rating={parseFloat(item.rating.rating)}
                          starRatedColor="gold"
                          numberOfStars={5}
                          starDimension="20px"
                          starSpacing="0px"
                          name='rating'
                        />
                        {" "+item.rating.rating} ({item.rating.jumlah_review})
                      </p>
                      <p class="card-text" onClick={() => handleSeeReview(item.id_sneaker)} style={{color:"white",fontStyle:"italic",fontSize:"10pt",textDecoration:"underline",cursor:"pointer"}}>See Review</p>
                    </div>
                    :
                    <div>
                      <p class="card-text" style={{color:"white"}}>
                        <StarRatings 
                          rating={0}
                          starRatedColor="gold"
                          numberOfStars={5}
                          starDimension="20px"
                          starSpacing="0px"
                          name='rating'
                        />
                        {" 0"} ({0})
                      </p>
                      <p class="card-text" style={{color:"white",fontStyle:"italic",fontSize:"10pt"}}>No Review</p>
                    </div>
                  }
                  <p class="card-text" style={{color:"white",float:"right"}}>Terjual : {item.jumlah}x</p> 
                  <br/>
                </div>
            </div>
          </div>
          )
      })
      );
    }
    else if(tabHome==2){//New Arrival
      return(
        dataNewArrival.map((item, index) => {
          return(
            <ItemPreview item={item} setShowAlasKaki={setShowAlasKaki} setDetailAlasKaki={setDetailAlasKaki} setShowreview={setShowreview} setSelectedsneaker={setSelectedsneaker}/>
          )
      })
      );
    }
    else if(tabHome==3){//Promo
      return(
        datapromo.length?
        datapromo.map((item, index) => {
          return (
            <div class="container-all-promo" style={{width:"18rem", border:"1px solid orange",display:"inline-block",marginRight:"2%",marginTop:"2%",cursor:"pointer"}}>
              <div class="card" id="container-promo-dekstop" onClick={ () => handleDetailPromo(item.id_promo) } >
                <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar } />
                <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                  <p class="card-text" style={{color:"white"}}>
                    <h6>{item.judul.toUpperCase()}</h6>
                    Valid Until {item.tanggal_berakhir.substr(0,10)} <br/>
                  </p>
                </div>
              </div>
              <div class="card" id="container-promo-mobile" style={{display:"none"}} onClick={ () => handleDetailPromo(item.id_promo) } >
                <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar } />
                <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                  <p class="card-text" style={{color:"white"}}>
                    <h6>{item.judul.toUpperCase()}</h6>
                    Valid Until {item.tanggal_berakhir.substr(0,10)} <br/>
                  </p>
                </div>
              </div>
            </div>
          );
        })
        :
        "No promo Avalaible !"
      );
    }
  }
  
  async function getreviewalaskaki(){
    const fetchAPI= await fetch(`/apiAlasKaki/getreviewalaskaki/`, {
      method: 'GET'
    });
    const result= await fetchAPI.json();
    console.log(result.review);
    setDatareview(result.review);
  }
  async function getPromo(){
    const fetchAPI= await fetch(`/apiPromo/getPromoHome/`, {
      method: 'GET'
    });
    const result= await fetchAPI.json();
    
    setDatapromo(result.promo);
  }
  async function handleDetailPromo(id){
    setShowpromo(true);
    const fetchAPI= await fetch(`/apiPromo/getPromoHome/${id}`, {
      method: 'GET'
    });
    const result= await fetchAPI.json();
    
    setDetailpromo(result.promo);
  }
  async function getNewArrival(){
    try {
      const fetchAPI= await fetch(`/apiAlasKaki/getnewarrival`, { 
          method: 'GET'
      });
      
      const result= await fetchAPI.json();
      result.alaskaki= result.alaskaki.map(item => ({
        id_sneaker: item.id_sneaker,
        id_kategori: item.id_kategori,
        nama_sneaker: item.nama_sneaker,
        jenis_sneaker: item.jenis_sneaker,
        brand_sneaker: item.brand_sneaker,
        harga_sneaker: item.harga_sneaker,
        status_sneaker: item.status_sneaker,
        gambar: item.gambar,
        jumlah: item.jumlah,
        ukuran:[],
        rating:[]
      }));
      for (let i = 0; i < result.alaskaki.length; i++) {
        result.alaskaki[i].ukuran=result.ukuran[i];
      }
      for (let i = 0; i < result.alaskaki.length; i++) {
        result.alaskaki[i].rating=result.rating[i];
      }
      console.log(result.alaskaki);
      setDataNewArrival(result.alaskaki);
    } catch (error) {
        console.error(error);
    }
  }
  async function getBestSeller(){
    try {
      const fetchAPI= await fetch(`/apiAlasKaki/getbestseller`, { 
          method: 'GET'
      });
      
      const result= await fetchAPI.json();
      result.alaskaki= result.alaskaki.map(item => ({
        id_sneaker: item.id_sneaker,
        id_kategori: item.id_kategori,
        nama_sneaker: item.nama_sneaker,
        jenis_sneaker: item.jenis_sneaker,
        brand_sneaker: item.brand_sneaker,
        harga_sneaker: item.harga_sneaker,
        status_sneaker: item.status_sneaker,
        gambar: item.gambar,
        jumlah: item.jumlah,
        ukuran:[],
        rating:[]
      }));
      for (let i = 0; i < result.alaskaki.length; i++) {
        result.alaskaki[i].ukuran=result.ukuran[i];
      }
      for (let i = 0; i < result.alaskaki.length; i++) {
        result.alaskaki[i].rating=result.rating[i];
      }
      console.log(result.alaskaki);
      setDataBestSeller(result.alaskaki);
    } catch (error) {
        console.error(error);
    }
  }
  function handleDetailAlasKaki(item){
    setShowAlasKaki(true);
    setDetailAlasKaki(item);
  }
  function handleSeeReview(item){
    setShowreview(true);
    setSelectedsneaker(item);
  }
  function handleClosePromo() { setShowpromo(false); }
  useEffect(() => {
    getreviewalaskaki();
    getBestSeller();
    getNewArrival();
    getPromo();
  }, []);
	return (
    <>
          <div id="announce-dekstop" className="slide-container" >
            <Slide autoplay={false}>
              <div className="each-slide" style={{width:"100%"}}>
                <div className="image-container" style={{position:"relative"}}>
                  <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"man.jpg" } />
                  <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"26%",marginLeft:"4%"}}>
                    <h3 style={{color:"white",font:"normal 50px 'Cookie', cursive"}}>Men's Collections 2020</h3>
                    <p style={{color:"white",font:"normal 16px 'Cookie', cursive"}}>They say that shoes can make a man. And with the right pair, anything is possible.</p>
                    <a type="button" href="/katalog/1/semua" class="btn btn-outline-light" style={{borderRadius:"0px"}}>Shop Now !</a>
                  </div>
                </div>
              </div>
              <div className="each-slide">
                <div className="image-container" style={{position:"relative"}}>
                  <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"woman.jpg" } />
                  <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"23%",marginLeft:"4%"}}>
                    <h3 style={{color:"black",font:"normal 45px 'Cookie', cursive"}}>Woman's Collections 2020</h3>
                    <p style={{color:"black",font:"normal 16px 'Cookie', cursive"}}>A woman can never have too many shoes. <br/> From sassy to classy, cute to classic <br/> and every point in between <br/> we have just the right style <br/> for all life’s moments.</p>
                    <a type="button" href="/katalog/2/semua" class="btn btn-outline-dark" style={{borderRadius:"0px"}}>Shop Now !</a>
                  </div>
                </div>
              </div>
              <div className="each-slide">
              <div className="image-container" style={{position:"relative"}}>
                  <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"kid.jpg" } />
                  <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"26%",marginLeft:"4%"}}>
                    <h3 style={{color:"white",font:"normal 45px 'Cookie', cursive"}}>Kid's Collections 2020</h3>
                    <p style={{color:"white",font:"normal 16px 'Cookie', cursive"}}>Every step they take matters—from running around on the playground <br/> to walking into a classroom for the first time And we have <br/> the styles they need for every move.</p>
                    <a type="button" href="/katalog/3/semua" class="btn btn-outline-light" style={{borderRadius:"0px"}}>Shop Now !</a>
                  </div>
                </div>
              </div>
            </Slide>
          </div>
          <div id="announce-mobile" className="slide-container" style={{display:"none"}}>
            <Slide autoplay={false}>
              <div className="each-slide" style={{width:"100%"}}>
                <div className="image-container" style={{position:"relative"}}>
                  <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"man.jpg" } />
                  <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"26%",marginLeft:"4%"}}>
                    <h3 style={{color:"white",font:"normal 20px 'Cookie', cursive"}}>Men's Collections 2020</h3>
                    <p style={{color:"white",font:"normal 8px 'Cookie', cursive"}}>They say that shoes can make a man. And with the right pair, anything is possible.</p>
                    <a type="button" href="/katalog/1/semua" class="btn btn-outline-light" style={{borderRadius:"0px",width:"50%%",fontSize:"8pt"}}>Shop Now !</a>
                  </div>
                </div>
              </div>
              <div className="each-slide">
                <div className="image-container" style={{position:"relative"}}>
                  <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"woman.jpg" } />
                  <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"23%",marginLeft:"4%"}}>
                    <h3 style={{color:"black",font:"normal 20px 'Cookie', cursive"}}>Woman's Collections 2020</h3>
                    <p style={{color:"black",font:"normal 5px 'Cookie', cursive"}}>A woman can never have too many shoes. <br/> From sassy to classy, cute to classic <br/> and every point in between <br/> we have just the right style <br/> for all life’s moments.</p>
                    <a type="button" href="/katalog/2/semua" class="btn btn-outline-dark" style={{borderRadius:"0px",width:"50%%",fontSize:"8pt"}}>Shop Now !</a>
                  </div>
                </div>
              </div>
              <div className="each-slide">
              <div className="image-container" style={{position:"relative"}}>
                  <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"kid.jpg" } />
                  <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"26%",marginLeft:"4%"}}>
                    <h3 style={{color:"white",font:"normal 20px 'Cookie', cursive"}}>Kid's Collections 2020</h3>
                    <p style={{color:"white",font:"normal 8px 'Cookie', cursive"}}>Every step they take matters—from running around on the playground <br/> to walking into a classroom for the first time And we have <br/> the styles they need for every move.</p>
                    <a type="button" href="/katalog/3/semua" class="btn btn-outline-light" style={{borderRadius:"0px",width:"50%%",fontSize:"8pt"}}>Shop Now !</a>
                  </div>
                </div>
              </div>
            </Slide>
          </div>
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <div id="announce-category-dekstop" className="image-container" style={{position:"relative"}}>
            <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"announcement.png" } />
            <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"16%",marginLeft:"35%"}}>
              <h3 style={{color:"Black",font:"normal 35px 'calibri', cursive"}}>Sandal's <br/> Collections 2020</h3>
              <a href="/katalog/3/Sandal" style={{color:"Black",font:"normal 15px 'arial', cursive",textDecoration:"underline"}}>S h o p &nbsp; N o w !</a>
            </div>
            <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"67%",marginLeft:"3%"}}>
              <h3 style={{color:"Black",font:"normal 35px 'calibri', cursive"}}>High Heel's <br/> Collections 2020</h3>
              <a href="/katalog/2/Heels" style={{color:"Black",font:"normal 15px 'arial', cursive",textDecoration:"underline"}}>S h o p &nbsp; N o w !</a>
            </div>
            <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"62%",marginLeft:"45%"}}>
              <h3 style={{color:"Black",font:"normal 35px 'calibri', cursive"}}>Loafer's <br/> Collections 2020</h3>
              <a href="/katalog/1/Loafer" style={{color:"Black",font:"normal 15px 'arial', cursive",textDecoration:"underline"}}>S h o p &nbsp; N o w !</a>
            </div>
          </div>
          <div id="announce-category-mobile" className="image-container" style={{position:"relative",display:"none"}}>
            <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+"announcement.png" } />
            <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"16%",marginLeft:"35%"}}>
              <h3 style={{color:"Black",font:"normal 10px 'calibri', cursive"}}>Sandal's <br/> Collections 2020</h3>
              <a href="/katalog/3/Sandal" style={{color:"Black",font:"normal 5px 'arial', cursive",textDecoration:"underline"}}>S h o p &nbsp; N o w !</a>
            </div>
            <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"67%",marginLeft:"3%"}}>
              <h3 style={{color:"Black",font:"normal 10px 'calibri', cursive"}}>High Heel's <br/> Collections 2020</h3>
              <a href="/katalog/2/Heels" style={{color:"Black",font:"normal 5px 'arial', cursive",textDecoration:"underline"}}>S h o p &nbsp; N o w !</a>
            </div>
            <div style={{position:"absolute",top:"0px",left:"0px",marginTop:"62%",marginLeft:"45%"}}>
              <h3 style={{color:"Black",font:"normal 10px 'calibri', cursive"}}>Loafer's <br/> Collections 2020</h3>
              <a href="/katalog/1/Loafer" style={{color:"Black",font:"normal 5px 'arial', cursive",textDecoration:"underline"}}>S h o p &nbsp; N o w !</a>
            </div>
          </div>
          <hr/>
          <div class="container-tab-home-dekstop" style={{display:"inline-block",width:"100%"}}>
            <center>
              <a onClick={HandleTab1} style={{color:colorTab1,textDecoration:"none",font:"normal 25px 'calibri', cursive",marginLeft:"-3%",marginRight:"6%",cursor:"pointer"}}>Best Seller</a>
              <a onClick={HandleTab2} style={{color:colorTab2,textDecoration:"none",font:"normal 25px 'calibri', cursive",marginRight:"6%",cursor:"pointer"}}>New Arrivals</a>
              <a onClick={HandleTab3} style={{color:colorTab3,textDecoration:"none",font:"normal 25px 'calibri', cursive",cursor:"pointer"}}>Promo</a>
            </center>
          </div>
          <div class="container-tab-home-mobile" style={{display:"inline-block",width:"100%",display:"none"}}>
            <center>
              <a onClick={HandleTab1} style={{color:colorTab1,textDecoration:"none",font:"normal 15px 'calibri', cursive",marginLeft:"-3%",marginRight:"6%",cursor:"pointer"}}>Best Seller</a>
              <a onClick={HandleTab2} style={{color:colorTab2,textDecoration:"none",font:"normal 15px 'calibri', cursive",marginRight:"6%",cursor:"pointer"}}>New Arrivals</a>
              <a onClick={HandleTab3} style={{color:colorTab3,textDecoration:"none",font:"normal 15px 'calibri', cursive",cursor:"pointer"}}>Promo</a>
            </center>
          </div>
          <hr/>
          {HandleTampilTab()}
        </div>
        <div class="col-sm-2"></div>
      </div>
      <br/><br/>
      <ReactTooltip />
      <Modal size="md" style={{ fontSize: '1.25vw', opacity:1 }}
            show={ showpromo } 
            onHide={ handleClosePromo }>
              { detailpromo ? 
                  <> 
                      <Modal.Header closeButton>
                          <Modal.Title>
                              Detail Promo  
                          </Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="mt-4">
                        {
                          detailpromo.map((item, index) => {
                              return (
                                  <div className="card mb-4"
                                        key={ index }>
                                      <div className="card-title"
                                            style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                          <h5 className="text-center text-white p-3">
                                              {item.judul.toUpperCase()}
                                          </h5>
                                      </div>
                                      <div className="card-body"
                                            style={{ padding: '0% !important' }}>
                                          <div className="row">
                                              <img className="col-12" src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar} />
                                          </div>
                                      </div>
                                      <div>
                                        <hr/>
                                        <div style={{marginLeft:"3%"}}>
                                          <Modal.Title>
                                            Description :  
                                          </Modal.Title>
                                          <p style={{fontSize:"15px"}}>{item.deskripsi}</p>
                                        </div>
                                        <hr/>
                                        <div style={{marginLeft:"3%"}}>
                                          <Modal.Title>
                                            Terms And Conditions :  
                                          </Modal.Title>
                                          <div style={{fontSize:"15px"}}>
                                            Discount : {item.diskon}% <br/>
                                            Max Discount : {"Rp."+item.max_diskon.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')+",-"} <br/>
                                            Minimum Shopping : {"Rp."+item.min_belanja.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')+",-"} <br/> <br/>
                                            Validity : {item.tanggal_mulai.substr(0,10)} To {item.tanggal_berakhir.substr(0,10)} <br/>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              )
                          })
                        }
                          </div>
                      </Modal.Body>
                  </> 
              : '' } 
          </Modal>
          {
            detailAlasKaki?
              <ModalDetail show={showAlasKaki} setShow={setShowAlasKaki} detailalaskaki={detailAlasKaki} />
            :""
          }
          <ModalReview show={showreview} setShow={setShowreview} selectedsneaker={selectedsneaker} datareview={datareview} />
    </>
	);
}

export default Home;