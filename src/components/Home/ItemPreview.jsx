import React, { useState, useEffect } from 'react';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch,faAngleDown,faStar,faCartPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import ReactTooltip from 'react-tooltip';
import './index.css';
import Collapse from 'react-bootstrap/Collapse'
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';


const ItemPreview = ({item,setShowAlasKaki,setDetailAlasKaki,setShowreview,setSelectedsneaker}) => {
  const history= useHistory();
  
  function handleDetailAlasKaki(item){
    setShowAlasKaki(true);
    setDetailAlasKaki(item);
  }
  function handleSeeReview(item){
    setShowreview(true);
    setSelectedsneaker(item);
  }
    return (
      <div class="container-item-preview" style={{display:"inline-block",marginLeft:"1.4%",marginTop:"2%"}}>
            <div class="card" id="container-dekstop" style={{width:"18rem",marginLeft:"1.4%",marginTop:"2%"}}>
              <img className="card-img-top" style={{cursor:"pointer"}} onClick={() => handleDetailAlasKaki(item)} src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } />
                    
              <div class="card-body" style={{backgroundColor:"#292c2f"}}>
              <h5 class="card-title" style={{color:"white"}}>{item.nama_sneaker}</h5>
                <p class="card-text" style={{color:"white"}}>{item.brand_sneaker}</p>
                <p class="card-text" style={{color:"white"}}>Rp.{item.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p>
                <p class="card-text" style={{color:"white"}}>
                  Ukuran : <br/>
                  {
                    item.ukuran?
                    item.ukuran.map((item, index) => {
                      return(
                        <>
                          <span class="dot" style={{color:"black"}} data-tip={item.stok_sneaker+" Available"}>{item.ukuran_sneaker}</span>
                          <ReactTooltip />
                        </>
                      );
                    })
                    : <strong style={{color:"red"}}>Stock Empty</strong>
                  }
                </p>
                {
                  item.rating?
                  <div>
                    <p class="card-text" style={{color:"white"}}>
                      <StarRatings 
                        rating={parseFloat(item.rating.rating)}
                        starRatedColor="gold"
                        numberOfStars={5}
                        starDimension="23px"
                        starSpacing="0px"
                        name='rating'
                      />
                      {" "+item.rating.rating} ({item.rating.jumlah_review})
                    </p>
                    <p class="card-text" onClick={() => handleSeeReview(item.id_sneaker)} style={{color:"white",fontStyle:"italic",fontSize:"10pt",textDecoration:"underline",cursor:"pointer"}}>See Review</p>
                  </div>
                  :
                  <div>
                    <p class="card-text" style={{color:"white"}}>
                      <StarRatings 
                        rating={0}
                        starRatedColor="gold"
                        numberOfStars={5}
                        starDimension="23px"
                        starSpacing="0px"
                        name='rating'
                      />
                      {" 0"} ({0})
                    </p>
                    <p class="card-text" style={{color:"white",fontStyle:"italic",fontSize:"10pt"}}>No Review</p>
                  </div>
                }
                <br/>
              </div>
          </div>
          <div class="card" id="container-mobile" style={{display:"none",width:"15rem",marginLeft:"30%",marginTop:"15%"}}>
              <img className="card-img-top" style={{cursor:"pointer"}} onClick={() => handleDetailAlasKaki(item)} src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } />
                    
              <div class="card-body" style={{backgroundColor:"#292c2f"}}>
              <h6 class="card-title" style={{color:"white"}}>{item.nama_sneaker+" ("+item.brand_sneaker+")"}</h6>
                <p class="card-text" style={{color:"white"}}>Rp.{item.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</p>
                <p class="card-text" style={{color:"white"}}>
                  Ukuran : <br/>
                  {
                    item.ukuran?
                    item.ukuran.map((item, index) => {
                      return(
                        <>
                          <span class="dot" style={{color:"black"}} data-tip={item.stok_sneaker+" Available"}>{item.ukuran_sneaker}</span>
                          <ReactTooltip />
                        </>
                      );
                    })
                    : <strong style={{color:"red"}}>Stock Empty</strong>
                  }
                </p>
                {
                  item.rating?
                  <div>
                    <p class="card-text" style={{color:"white"}}>
                      <StarRatings 
                        rating={parseFloat(item.rating.rating)}
                        starRatedColor="gold"
                        numberOfStars={5}
                        starDimension="20px"
                        starSpacing="0px"
                        name='rating'
                      />
                      {" "+item.rating.rating} ({item.rating.jumlah_review})
                    </p>
                    <p class="card-text" onClick={() => handleSeeReview(item.id_sneaker)} style={{color:"white",fontStyle:"italic",fontSize:"10pt",textDecoration:"underline",cursor:"pointer"}}>See Review</p>
                  </div>
                  :
                  <div>
                    <p class="card-text" style={{color:"white"}}>
                      <StarRatings 
                        rating={0}
                        starRatedColor="gold"
                        numberOfStars={5}
                        starDimension="20px"
                        starSpacing="0px"
                        name='rating'
                      />
                      {" 0"} ({0})
                    </p>
                    <p class="card-text" style={{color:"white",fontStyle:"italic",fontSize:"10pt"}}>No Review</p>
                  </div>
                }
                <br/>
              </div>
          </div>
        </div>
	);
}

export default ItemPreview;