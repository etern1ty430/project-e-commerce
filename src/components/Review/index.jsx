import React, { useState, useEffect, Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import axios from "axios";
import { event } from 'jquery';



const Address = () => {
    const [provinsi, setProvinsi] = useState([]);
    const [kota, setKota] = useState([]);
    const [kecamatan, setKecamatan] = useState([]);
    const [kelurahan, setKelurahan] = useState([]);
    const [idprov,setIdProv] = useState([]);

    function _updateProv (event){
        setIdProv(event.target.value);
    }

    useEffect(() => {
        axios
            .get("https://dev.farizdotid.com/api/daerahindonesia/provinsi", {
            })
            .then((res) => {
                setProvinsi(res.data.provinsi);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    useEffect(() => {
        axios
            .get("https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi="+{idprov}, {
            })
            .then((res) => {
                setKota(res.data.kota_kabupaten);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    useEffect(() => {
        axios
            .get("https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=3214", {
            })
            .then((res) => {
                setKecamatan(res.data.kecamatan);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    useEffect(() => {
        axios
            .get("https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=3214010", {
            })
            .then((res) => {
                setKelurahan(res.data.kelurahan);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
        
	return (
    <>
    {console.log(idprov)}
    <div class="row">
        <div class="col-sm-2"></div>
      <div class="col-sm-8">
        <br/><br/><br/><br/>
          <form>
            <div className="form-group">
                <label>Province</label>
                <select onChange={_updateProv} class="form-control">
                <option value="" disabled selected="selected">Please Select Province</option>
                { provinsi.map(t => 
                    <option value={t.id}>{t.nama}</option>
                )}
                </select>
            </div>
            <div className="form-group">
                <label>City</label>
                <select class="form-control">
                { kota.map(t => 
                    <option value={t.id}>{t.nama}</option>
                )}
                </select>
            </div>
            <div className="form-group">
                <label>Districts</label>
                <select class="form-control">
                { kecamatan.map(t => 
                    <option value={t.id}>{t.nama}</option>
                )}
                </select>
            </div>
            <div className="form-group">
                <label>Sub - District</label>
                <select class="form-control">
                { kelurahan.map(t => 
                    <option value={t.id}>{t.nama}</option>
                )}
                </select>
            </div>
            <div className="form-group">
                <label>Address</label>
              <input type="text" class="form-control" id="alamat" placeholder="Address"/>
            </div>
            <div className="form-group">
                <label>Postal Code</label>
              <input type="text" class="form-control" id="alamat" placeholder="Postal Code"/>
            </div>
            <div className="form-group">
                <label>Recepient</label>
              <input type="text" class="form-control" id="alamat" placeholder="Recepient"/>
            </div>
            <div className="form-group">
                <label>Phone Number</label>
              <input type="text" class="form-control" id="alamat" placeholder="Phone Number"/>
            </div>
            <br/>
            <button type="submit" style={{borderRadius:"0px"}} class="btn btn-dark">Next</button>
          </form>
      </div>
      <div className="col-sm-2"></div>
    </div>
    <br/><br/><br/><br/> 
    </>
	);
}

export default Address;