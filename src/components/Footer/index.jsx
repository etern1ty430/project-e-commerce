﻿import React from "react";
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import './index.css';

const Footer = () => {
	return (
		<div class="footer-container-all">
			<div class="footer-container-dekstop" style={{marginTop:"12%"}}>
				<footer class="footer-distributed">
		
				<div class="footer-left">
		
				<h3>AlasKaki<span>Qu</span></h3>
		
				<p class="footer-links">
				<a href="#">Home</a> &nbsp;
			·	&nbsp;						
				<a href="#">Blog</a> &nbsp;
			·	&nbsp;
				<a href="#">Pricing</a> &nbsp;
			·	&nbsp;
				<a href="#">About</a> &nbsp;
			·	&nbsp;
				<a href="#">Faq</a> &nbsp;
			·	&nbsp;
				<a href="#">Contact</a> &nbsp;
				</p>
		
				<p class="footer-company-name">AlasKakiQu &copy; 2020</p>
				</div>
		
				<div class="footer-center">
		
				<div>
				<i class="circleIcon"><FontAwesomeIcon icon={ faLocationArrow } style={{width:"40%"}}/></i>
				<p><span>73-77 Ngagel Jaya Utara</span> Surabaya, Indonesia</p>
				</div>
		
				<div>
				<i class="circleIcon"><FontAwesomeIcon icon={ faPhone } style={{width:"40%"}}/></i>
				<p>+6281 2345 6789</p>
				</div>
		
				<div>
				<i class="circleIcon"><FontAwesomeIcon icon={ faEnvelope } style={{width:"40%"}}/></i>
				<p><a href="mailto:support@company.com">contact@alaskakiqu.com</a></p>
				</div>
		
				</div>
		
				<div class="footer-right">
		
				<p class="footer-company-about">
				<span>About the company</span>
					Styles come and go. Good design is a language, not a style.
				</p>
		
				<div class="footer-icons">
		
				<a href="#"><FontAwesomeIcon icon={ faFacebook }/></a>
				<a href="#"><FontAwesomeIcon icon={ faTwitter }/></a>
				<a href="#"><FontAwesomeIcon icon={ faInstagram }/></a>
				<a href="#"><FontAwesomeIcon icon={ faWhatsapp }/></a>
		
				</div>
		
				</div>
		
				</footer>
			</div>
			<div class="footer-container-mobile" style={{display:"none"}}>
				<footer class="footer-distributed">
		
				<div class="footer-left">
					<h5>AlasKaki<span>Qu</span></h5>
					<p class="footer-company-name">AlasKakiQu &copy; 2020</p>
				</div>
		
				<div class="footer-right">
					<div class="footer-icons">
						<a href="#"><FontAwesomeIcon icon={ faFacebook }/></a>
						<a href="#"><FontAwesomeIcon icon={ faTwitter }/></a>
						<a href="#"><FontAwesomeIcon icon={ faInstagram }/></a>
						<a href="#"><FontAwesomeIcon icon={ faWhatsapp }/></a>
					</div>
				</div>
				</footer>
			</div>
		</div>
	);
}

export default Footer;