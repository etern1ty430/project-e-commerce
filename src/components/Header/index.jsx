﻿import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Modal } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

import Navbar from "reactjs-navbar";
import "reactjs-navbar/dist/index.css";
import "./index.css";
import ModalDetail from "../Katalog/ModalDetail";
import StarRatings from 'react-star-ratings';
import ModalReview from '../Home/ModalReview';
import ItemPreview from '../Home/ItemPreview';
import SearchOrder from './searchOrder'
import {
	faCartPlus, faHome,
	faUsers, faInfoCircle, faSave, faSignOutAlt,
	faMale, faFemale, faChild,
	faBriefcase, faFootballBall, faRunning, faGlassCheers, faSnowflake, faFutbol, faSearch
  } from "@fortawesome/free-solid-svg-icons";

const Header = () => {
	const history= useHistory();
	const [ showSearch, setShowSearch ] = useState(false);
	const [ show, setShow ] = useState(false);
	const [ tab1, setTab1 ] = useState("active");
	const [ tab2, setTab2 ] = useState("");
	const [ tab3, setTab3 ] = useState("");
	const [ tab4, setTab4 ] = useState("");
	const [ tab5, setTab5 ] = useState("");
	const [ categories, setCategories ] = useState("");
	const [ alaskaki, setAlaskaki ] = useState([]);
	const [ detailalaskaki, setDetailalaskaki ] = useState([]);

	const [ selectedsneaker, setSelectedsneaker ]= useState("");
	const [datareview, setDatareview] = useState([]);
	const [ showreview, setShowreview ] = useState(false);

	function handleSeeReview(item){
		setShowreview(true);
		setSelectedsneaker(item);
	}
	function handleTab1(){
		setCategories("");
		setTab1("active");
		setTab2("");
		setTab3("");
		setTab4("");
		setTab5("");
	}
	function handleTab2(){
		setCategories("1");
		setTab1("");
		setTab2("active");
		setTab3("");
		setTab4("");
		setTab5("");
	}
	function handleTab3(){
		setCategories("2");
		setTab1("");
		setTab2("");
		setTab3("active");
		setTab4("");
		setTab5("");
	}
	function handleTab4(){
		setCategories("3");
		setTab1("");
		setTab2("");
		setTab3("");
		setTab4("active");
		setTab5("");
	}
	function handleTab5(){
		setCategories("");
		setTab1("");
		setTab2("");
		setTab3("");
		setTab4("");
		setTab5("active");
		setAlaskaki([]);
	}
	async function getreviewalaskaki(){
	  const fetchAPI= await fetch(`/apiAlasKaki/getreviewalaskaki/`, {
		method: 'GET'
	  });
	  const result= await fetchAPI.json();
	  console.log(result.review);
	  setDatareview(result.review);
	}
	function cekhalaman(){
		if(window.sessionStorage.getItem("user")!=null){
			return(
					<div class="container-myaccount">
						<div class="myaccount-dekstop">
							<Navbar
								menuItems={[
									{
										title: "My Account",
										icon: faUsers,
										isAuth: true,
										subItems: [
										{
											title: "Information",
											icon: faInfoCircle,
											isAuth: true,
											onClick: () => {
											// What you want to do...
												history.push(`/myaccount/1`);
												return window.location.reload();
											}
										},
										{
											title: "Wishlist",
											icon: faSave,
											isAuth: true,
											onClick: () => {
											// What you want to do...
												history.push(`/wishlist`);
												return window.location.reload();
											}
										},
										{
											title: "Logout",
											icon: faSignOutAlt,
											isAuth: true,
											onClick: () => {
											// What you want to do...
												{handleLogout()}
											}
										}
										]
									}
								]}
							/>
						</div>
						<div class="myaccount-mobile" style={{display:"none"}}>
								<Navbar
									menuItems={[
										{
											title: "",
											icon: faUsers,
											isAuth: true,
											onClick: () => {
												history.push(`/myaccount/1`);
												return window.location.reload();
											}
										}
									]}
								/>
						</div>
					</div>
			)
		}
		else{
			return(
				<div class="container-myaccount">
					<div class="myaccount-dekstop">
						<Navbar
							menuItems={[
								{
									title: "Login / Register",
									icon: faUsers,
									isAuth: true,
									onClick: () => {
									// What you want to do...
										history.push(`/login`);
										return window.location.reload();
									}
								}
							]}
						/>
					</div>
					<div class="myaccount-mobile" style={{display:"none"}}>
							<Navbar
								menuItems={[
									{
										title: "",
										icon: faUsers,
										isAuth: true,
										onClick: () => {
											history.push(`/login`);
											return window.location.reload();
										}
									}
								]}
							/>
					</div>
				</div>
			)
		}
	}
	function handleLogout(){
		window.sessionStorage.removeItem("user");
		history.push(`/`);
		return window.location.reload();
	}
	function handleDetailAlasKaki(item){
	  setShow(true);
	  setDetailalaskaki(item);
	}
	async function handleSearch(searchvalue){
		if(searchvalue=="" && categories==""){
			try {
				const fetchAPI= await fetch(`/apiAlasKaki/getalaskaki`, { 
					method: 'GET'
				});
				const result= await fetchAPI.json();
				result.alaskaki= result.alaskaki.map(item => ({
				id_sneaker: item.id_sneaker,
				id_kategori: item.id_kategori,
				nama_sneaker: item.nama_sneaker,
				jenis_sneaker: item.jenis_sneaker,
				brand_sneaker: item.brand_sneaker,
				harga_sneaker: item.harga_sneaker,
				status_sneaker: item.status_sneaker,
				gambar: item.gambar,
				ukuran:[],
				rating:[]
				}));
				for (let i = 0; i < result.alaskaki.length; i++) {
					result.alaskaki[i].ukuran=result.ukuran[i];
				}
				for (let i = 0; i < result.alaskaki.length; i++) {
				result.alaskaki[i].rating=result.rating[i];
				}
				console.log(result.alaskaki);
				setAlaskaki(result.alaskaki);
			}catch (error) {
				console.error(error);
			}
		}
		else if(searchvalue=="" && categories!=""){
			try {
				const fetchAPI= await fetch(`/apiAlasKaki/getalaskaki/${categories}`, { 
					method: 'GET'
				});
				const result= await fetchAPI.json();
				result.alaskaki= result.alaskaki.map(item => ({
				id_sneaker: item.id_sneaker,
				id_kategori: item.id_kategori,
				nama_sneaker: item.nama_sneaker,
				jenis_sneaker: item.jenis_sneaker,
				brand_sneaker: item.brand_sneaker,
				harga_sneaker: item.harga_sneaker,
				status_sneaker: item.status_sneaker,
				gambar: item.gambar,
				ukuran:[],
				rating:[]
				}));
				for (let i = 0; i < result.alaskaki.length; i++) {
					result.alaskaki[i].ukuran=result.ukuran[i];
				}
				for (let i = 0; i < result.alaskaki.length; i++) {
				result.alaskaki[i].rating=result.rating[i];
				}
				console.log(result.alaskaki);
				setAlaskaki(result.alaskaki);
			}catch (error) {
				console.error(error);
			}
		}
		else if(searchvalue!="" && categories==""){
			try {
				const fetchAPI= await fetch(`/apiAlasKaki/carialaskaki/${searchvalue}`, { 
					method: 'GET'
				});
				const result= await fetchAPI.json();
				result.alaskaki= result.alaskaki.map(item => ({
				id_sneaker: item.id_sneaker,
				id_kategori: item.id_kategori,
				nama_sneaker: item.nama_sneaker,
				jenis_sneaker: item.jenis_sneaker,
				brand_sneaker: item.brand_sneaker,
				harga_sneaker: item.harga_sneaker,
				status_sneaker: item.status_sneaker,
				gambar: item.gambar,
				ukuran:[],
				rating:[]
				}));
				for (let i = 0; i < result.alaskaki.length; i++) {
					result.alaskaki[i].ukuran=result.ukuran[i];
				}
				for (let i = 0; i < result.alaskaki.length; i++) {
				result.alaskaki[i].rating=result.rating[i];
				}
				console.log(result.alaskaki);
				setAlaskaki(result.alaskaki);
			}catch (error) {
				console.error(error);
			}
		}
		else{
			try {
				const fetchAPI= await fetch(`/apiAlasKaki/searchalaskaki/${categories}/${searchvalue}`, { 
					method: 'GET'
				}); 
				const result= await fetchAPI.json();
				result.alaskaki= result.alaskaki.map(item => ({
				id_sneaker: item.id_sneaker,
				id_kategori: item.id_kategori,
				nama_sneaker: item.nama_sneaker,
				jenis_sneaker: item.jenis_sneaker,
				brand_sneaker: item.brand_sneaker,
				harga_sneaker: item.harga_sneaker,
				status_sneaker: item.status_sneaker,
				gambar: item.gambar,
				ukuran:[],
				rating:[]
				}));
				for (let i = 0; i < result.alaskaki.length; i++) {
					result.alaskaki[i].ukuran=result.ukuran[i];
				}
				for (let i = 0; i < result.alaskaki.length; i++) {
				result.alaskaki[i].rating=result.rating[i];
				}
				console.log(result.alaskaki);
				setAlaskaki(result.alaskaki);
			}catch (error) {
				console.error(error);
			}
		}
	}
	function handleButtonSearch() {setShowSearch(true);}
	function handleCloseSearch() { setShowSearch(false); }

	useEffect(() => {
	  getreviewalaskaki();
	}, []);
	return(
		<div style={{width: '100%',backgroundColor:'rgba(51, 51, 51, 1)', position:'fixed', zIndex:'2'}}>
			<div class="row">
			<div class="col-sm-1" id="icon" style={{marginTop:'1%'}}>
				<div
					onClick={
						() => {
							history.push(`/`);
							return window.location.reload();
						}
					}
				>
					<Navbar
						logo={process.env.PUBLIC_URL+'/icons/icon1transparentsmall.png'}
						menuItems={[]}
					/>
				</div>
			</div>
			<div class="col-sm-1" id="title" style={{color:'white',marginTop: '1%'}}>
				<h3>AlasKaki<span>Qu</span></h3>
			</div>
			<div class="col-sm-6">
				<div class="list_menu_dekstop" style={{marginTop:'0.5%'}}>
				<Navbar
					menuItems={[
					{
						title: "Home",
						icon: faHome,
						isAuth: true,
						onClick: () => {
							history.push(`/`);
							return window.location.reload();
						}
					},
					{
						title: "Men",
						icon: faMale,
						isAuth: true,
						onClick: () => {
							history.push(`/katalog/1/semua`);
							return window.location.reload();
						},
						subItems: [
						{
							title: "Loafer",
							icon: faBriefcase,
							isAuth: true,
							onClick: () => {
								// What you want to do...
								history.push(`/katalog/1/Loafer`);
								return window.location.reload();
							}
						},
						{
							title: "Sport",
							icon: faFootballBall,
							isAuth: true,
							onClick: () => {
								// What you want to do...
								history.push(`/katalog/1/Sport`);
								return window.location.reload();
							}
						},
						{
							title: "Running",
							icon: faRunning,
							isAuth: true,
							onClick: () => {
								// What you want to do...
								history.push(`/katalog/1/Running`);
								return window.location.reload();
							}
						},
						{
							title: "Boot",
							icon: faSnowflake,
							isAuth: true,
							onClick: () => {
								// What you want to do...
								history.push(`/katalog/1/Boot`);
								return window.location.reload();
							}
						}
						]
					},
					{
						title: "Woman",
						icon: faFemale,
						isAuth: true,
						onClick: () => {
							history.push(`/katalog/2/semua`);
							return window.location.reload();
						},
						subItems: [
						{
							title: "Heels",
							icon: faGlassCheers,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/2/Heels`);
								return window.location.reload();
							}
						},
						{
							title: "Sport",
							icon: faFootballBall,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/2/Sport`);
								return window.location.reload();
							}
						},
						{
							title: "Running",
							icon: faRunning,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/2/Running`);
								return window.location.reload();
							}
						},
						{
							title: "Boot",
							icon: faSnowflake,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/2/Boot`);
								return window.location.reload();
							}
						}
						]
					},
					{
						title: "Kids",
						icon: faChild,
						isAuth: true,
						onClick: () => {
							history.push(`/katalog/3/semua`);
							return window.location.reload();
						},
						subItems: [
						{
							title: "Sandal",
							icon: faFutbol,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/3/Sandal`);
								return window.location.reload();
							}
						},
						{
							title: "Sport",
							icon: faFootballBall,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/3/Sport`);
								return window.location.reload();
							}
						},
						{
							title: "Running",
							icon: faRunning,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/3/Running`);
								return window.location.reload();
							}
						},
						{
							title: "Boot",
							icon: faSnowflake,
							isAuth: true,
							onClick: () => {
								history.push(`/katalog/3/Boot`);
								return window.location.reload();
							}
						}
						]
					}
					]}
				/>
				</div>
				<div class="list_menu_mobile" style={{display:"none"}}>
				<Navbar
					menuItems={[
					{
						title: "Home",
						icon: faHome,
						isAuth: true,
						onClick: () => {
							history.push(`/`);
							return window.location.reload();
						}
					},
					{
						title: "Men",
						icon: faMale,
						isAuth: true,
						onClick: () => {
							history.push(`/katalog/1/semua`);
							return window.location.reload();
						}
					},
					{
						title: "Woman",
						icon: faFemale,
						isAuth: true,
						onClick: () => {
							history.push(`/katalog/2/semua`);
							return window.location.reload();
						}
					},
					{
						title: "Kids",
						icon: faChild,
						isAuth: true,
						onClick: () => {
							history.push(`/katalog/3/semua`);
							return window.location.reload();
						}
					}
					]}
				/>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="search_bar" style={{backgroundColor:"none", paddingTop:'0.5%'}}>
					<div class="row">
						<div class="col-sm-10">
							<input class="search_input" type="text" onClick={handleButtonSearch} style={{width:"80%"}} placeholder="Search" readOnly/>
							<a id="search_input_mobile" class="btn btn-outline-secondary" onClick={handleButtonSearch}  style={{display:"none"}}>
								<FontAwesomeIcon icon={ faSearch } style={{width:"18px",height:"18px",color:"white"}}/>
							</a>
						</div>
						<div class="col-sm-2" style={{paddingTop:'3%'}}>
							<a id="cart_logo" class="btn btn-outline-secondary" href="/cart" style={{zIndex:'0'}}>
								<FontAwesomeIcon icon={ faCartPlus } style={{width:"18px",height:"18px",color:"white"}}/>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-2" id="account" style={{paddingTop:'0%'}}>
				{cekhalaman()}
			</div>
		</div>
		<Modal id="container-tab" size="xl" style={{ fontSize: '12pt', opacity:1 }}
				show={ showSearch } 
				onHide={ handleCloseSearch }>
				<> 
					<Modal.Header closeButton>
					</Modal.Header>
					<Modal.Body>	
						<div className="mt-4" style={{border:"1px solid #DCDCDC"}}>
							<ul class="nav nav-tabs">
								<li class="nav-item">
								<a class={"nav-link "+tab1} onClick={handleTab1} >All Categories</a>
								</li>
								<li class="nav-item">
								<a class={"nav-link "+tab2} onClick={handleTab2} >Men</a>
								</li>
								<li class="nav-item">
								<a class={"nav-link "+tab3} onClick={handleTab3} >Woman</a>
								</li>
								<li class="nav-item">
								<a class={"nav-link "+tab4} onClick={handleTab4} >Kids</a>
								</li>
								<li class="nav-item">
								<a class={"nav-link "+tab5} onClick={handleTab5} >Search Order</a>
								</li>
							</ul>
							<br/>
							<div class="input-group">
								{
									tab5 == "" ? 
									<input type="text" onChange={(e) => handleSearch(e.target.value)} class="form-control" id="searchbar" placeholder="S e a r c h . . ." />
									:
									<SearchOrder/>
								}
								
							</div>
							<div class="container-item-preview" style={{marginLeft:"8%"}}>
							{
								alaskaki.map((item, index) => {
									return(
										<ItemPreview item={item} setShowAlasKaki={setShow} setDetailAlasKaki={setDetailalaskaki} setShowreview={setShowreview} setSelectedsneaker={setSelectedsneaker}/>
									)
								})
							}
							</div>
							<br/><br/><br/>
						</div>
					</Modal.Body>
				</> 
		</Modal>
        <ModalDetail show={show} setShow={setShow} detailalaskaki={detailalaskaki} />
        <ModalReview show={showreview} setShow={setShowreview} selectedsneaker={selectedsneaker} datareview={datareview} />
		</div>
	)
}

export default Header;