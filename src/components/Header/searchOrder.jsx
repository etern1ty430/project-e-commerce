import React, { useState, useEffect } from 'react';
import { MDBDataTableV5 } from 'mdbreact';
import Rate from '../MyAccount/PurchaseList/rate'
import Invoice from '../MyAccount/PurchaseList/invoice'

const SearchOrder = () => {
    const [listHeader, setListHeader] = useState([]);
    const [listDetail, setListDetail] = useState([]);

    //VAR SEARCH
    const [tempListHeader, setTempListHeader] = useState([])
    const [tempListDetail, setTempListDetail] = useState([])

    //POPUP LAPORAN
    const [ dataLaporanHeader, setDataLaporanHeader ] = useState([]);
    const [ dataLaporanDetail, setDataLaporanDetail ] = useState([]);

    //TABLE order
    const [ dataTableOrder, setDataTableOrder ]= useState();
    const [ dataSetTableOrder, setDataSetTableOrder] = useState();

    //POPUP RATE
    const [showRate, setShowRate] = useState(false);

    //POPUP INVOICE
    const [popUpInvoiceHeader, setPopUpInvoiceHeader] = useState(null);
    const [popUpInvoiceDetail, setPopupInvoiceDetail] = useState(null);
    const [showInvoice, setShowInvoice] = useState(false);

	async function getPurchaseUser() {
        try {
            const data = await fetch("/purchaselist/getListPurchaseAnonymous", { 
            method: "GET"
        })
            .then(async response => {
                let data = await response.json();
                setListHeader(data.dataList);
                setListDetail(data.listBarang);
                //setTableForReview(data, data.listBarang);
            })
            .catch(error => alert('Error! ' + error.message));
        } catch (error) {
            console.error(error);
        }
    }
    async function getInvoice() {
        try {
            const data = await fetch("/apiLaporan/getLaporan", { method: "GET" })
            .then(async response => {
                let a = await response.json();
                setDataLaporanHeader(a[0].header);
                setDataLaporanDetail(a[0].detail);
            })
        } catch (error) {
            console.error(error);
        }
    }

    function handleInvoice(id_trans){
        dataLaporanHeader.map((item, index) => {
            if(item.id_trans == id_trans) setPopUpInvoiceHeader(item);
        })
    
        let tempDetail = [];
        let counter = 1;
        dataLaporanDetail.map((item, index) => {
            if(item.id_trans == id_trans) {
                item.counter = counter;
                tempDetail.push(item);
                counter = counter + 1;
            }
        })
        setPopupInvoiceDetail(tempDetail);
        setShowInvoice(true);
    }

    //STATUS = SEDANG DIKIRIM
    async function handleConfirmOrder(id_shipment){
		if(window.confirm('Are You Sure Already Got All Orders?')){
            try {
                let obj = {
                    id_shipment: id_shipment
                }
                const res = await fetch("/purchaselist/confirmOrder", { 
                    method : "POST",
                    headers : { "Content-Type" : "application/json" },
                    body : JSON.stringify(obj)
                });
                return window.location.reload();
            } catch (error) {
                console.error(error);
            }
        }
    }

    //STATUS = SUDAH DITERIMA (UNTUK RATING)
    function handleRating(){
        setShowRate(true);
    }


    function setTableForOrder(){
        // console.log(tempListHeader);
        // console.log(tempListDetail);
        if(tempListHeader.length > 0){
            if(tempListHeader[0].status_pengiriman == "Selesai" || tempListHeader[0].status_pengiriman == "Sedang Disiapkan"){
                let tempTable = tempListHeader.map((item, index) => {
                    let date = new Date(item.tgl_beli);
                    // date.setDate(date.getDate()-1);
                    return ({
                        row: index+1,
                        id_trans: `#${item.id_trans}`,
                        id_user: `#${item.id_user}`,
                        tgl_beli: date.toDateString(),
                        jumlah: item.jumlah,
                        total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
                        status: <b>{item.status_pengiriman}</b>,
            
                        invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                                style={{fontSize:"0.7vw", textAlign:"center"}}
                                className="btn btn-primary">
                                &nbsp; Invoice &nbsp;
                                </button>
                    }
                    )}
                );
                setDataSetTableOrder(tempTable);
            
                setDataTableOrder({
                    columns:  [
                        { label: '#', field: 'row' },
                        { label: 'NO. TRANS', field: 'id_trans' },
                        { label: 'ID. USER', field: 'id_user' },
                        { label: 'DATE', field: 'tgl_beli' },
                        { label: 'JUMLAH', field: 'jumlah' },
                        { label: 'TOTAL', field: 'total' },
                        { label: 'STATUS', field: 'status' },
                        { label: 'INVOICE', field: 'invoice' }
                    ],
                    rows: tempTable
                });
            }
            else if(tempListHeader[0].status_pengiriman == "Sedang Dikirim"){
                let tempTable = tempListHeader.map((item, index) => {
                    let date = new Date(item.tgl_beli);
                    date.setDate(date.getDate()-1);
                    return ({
                        row: index+1,
                        id_trans: `#${item.id_trans}`,
                        id_user: `#${item.id_user}`,
                        tgl_beli: date.toDateString(),
                        jumlah: item.jumlah,
                        total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
            
                        action: <button onClick={() => handleConfirmOrder(item.id_shipment) } 
                                style={{fontSize:"0.7vw", textAlign:"center"}}
                                className="btn btn-success">
                                &nbsp; Confirm Order &nbsp;
                                </button>,
                        invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                                style={{fontSize:"0.7vw", textAlign:"center"}}
                                className="btn btn-primary">
                                &nbsp; Invoice &nbsp;
                                </button>
                    }
                    )}
                );
                setDataSetTableOrder(tempTable);
            
                setDataTableOrder({
                    columns:  [
                        { label: '#', field: 'row' },
                        { label: 'NO. TRANS', field: 'id_trans' },
                        { label: 'ID. USER', field: 'id_user' },
                        { label: 'DATE', field: 'tgl_beli' },
                        { label: 'JUMLAH', field: 'jumlah' },
                        { label: 'TOTAL', field: 'total' },
                        { label: 'ACTION', field: 'action' },
                        { label: 'INVOICE', field: 'invoice' }
                    ],
                    rows: tempTable
                });
            }
            else if(tempListHeader[0].status_pengiriman == "Sudah Diterima"){
                let tempTable = tempListHeader.map((item, index) => {
                    let date = new Date(item.tgl_beli);
                    date.setDate(date.getDate()-1);
                    return ({
                        row: index+1,
                        id_trans: `#${item.id_trans}`,
                        id_user: `#${item.id_user}`,
                        tgl_beli: date.toDateString(),
                        jumlah: item.jumlah,
                        total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
            
                        action: <button onClick={() => handleRating() } 
                                style={{fontSize:"0.7vw", textAlign:"center"}}
                                className="btn btn-primary">
                                &nbsp; RATE &nbsp;
                                </button>,
                        invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                                style={{fontSize:"0.7vw", textAlign:"center"}}
                                className="btn btn-primary">
                                &nbsp; Invoice &nbsp;
                                </button>
                    }
                    );
                });
            
                setDataSetTableOrder(tempTable);
            
                setDataTableOrder({
                    columns:  [
                        { label: '#', field: 'row' },
                        { label: 'NO. TRANS', field: 'id_trans' },
                        { label: 'ID. USER', field: 'id_user' },
                        { label: 'DATE', field: 'tgl_beli' },
                        { label: 'JUMLAH', field: 'jumlah' },
                        { label: 'TOTAL', field: 'total' },
                        { label: 'ACTION', field: 'action' },
                        { label: 'INVOICE', field: 'invoice' }
                    ],
                    rows: tempTable
                });
            }
        }
    }

    function onSearchOrder(e){
        e.preventDefault();

        let tempHeader = [];
        listHeader.map((item, index) => {
            if(item.id_trans == e.target.searchbar.value) tempHeader.push(item);
        });
        setTempListHeader(tempHeader);

        let tempDetail = [];
        let counter = 0;
        listDetail.map((item, index) => {
            if(item.id_trans == e.target.searchbar.value) {
                tempDetail.push(item);
                tempDetail[counter].counter = counter;
                counter = counter+1;
            }
        })
        setTempListDetail(tempDetail);
    }

    function tableAndroid(){
        if(tempListHeader.length > 0){
            return(
                <>
                    <div style={{width:"100%"}}>
                        <div style={{width:"40%", float:"left"}}>
                            ID Transaksi <br/>
                            Date <br/>
                            Status <br/>
                        </div>
                        <div style={{width:"60%", float:"left"}}>
                            :{tempListHeader[0].id_trans} <br/>
                            :{tempListHeader[0].tgl_beli} <br/>
                            :<b>{tempListHeader[0].status_pengiriman}</b><br/>
                        </div>
                        {
                            tempListHeader[0].status_pengiriman == "Sedang Dikirim"?
                            <button onClick={() => handleConfirmOrder(tempListHeader[0].id_shipment) } 
                                style={{fontSize:"2vw", textAlign:"center", float:"left"}}
                                className="btn btn-success">
                                &nbsp; Confirm Order &nbsp;
                            </button>
                            : ""
                        }
                        {
                            tempListHeader[0].status_pengiriman == "Sudah Diterima"?
                            <button onClick={() => handleRating() } 
                                style={{fontSize:"2vw", textAlign:"center", float:"left"}}
                                className="btn btn-primary">
                                &nbsp; RATE &nbsp;
                            </button>
                            : ""
                        }
                        <button onClick={() => handleInvoice(tempListHeader[0].id_trans) } 
                            style={{fontSize:"2vw", textAlign:"center", float:"left"}}
                            className="btn btn-primary">
                            &nbsp; Invoice &nbsp;
                        </button>
                    </div>
                </>
            )
            
        }
    }

    useEffect(() => {
        getPurchaseUser();
        getInvoice();
    }, []);
    
    useEffect(() => {
        setTableForOrder();
    }, [tempListHeader, tempListHeader]);

	return (
        <>
            <form onSubmit={onSearchOrder} style={{width:"100%"}}>
                <input type="text" class="form-control" id="searchbar" placeholder="No Order"/><br/>
                <button type="submit" class="btn btn-dark btn-block">SUBMIT</button><br/>
            </form>
            <div class="container_tableOrder_PC" style={{width:"100%"}}> 
                { dataTableOrder && dataTableOrder.rows.length ?    
                <MDBDataTableV5 responsive
                                striped
                                searchBottom={ false }
                                data={ dataTableOrder }/>
                : 'No orders history.' }
            </div>

            <div class="container_tableOrder_HP">
                {tableAndroid()}
            </div>
            <Rate showRate={showRate} setShowRate={setShowRate} dataRate={tempListDetail}/>
            <Invoice showInvoice={showInvoice} setShowInvoice={setShowInvoice} popUpInvoiceHeader={popUpInvoiceHeader} popUpInvoiceDetail={popUpInvoiceDetail}/>
        </>
	);
}

export default SearchOrder;