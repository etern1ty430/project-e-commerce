import React, { useState, useEffect, Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import axios from "axios";
import { event } from 'jquery';
import { stat } from 'fs';
import './index.css';



const Address = () => {
	const history= useHistory();
	const [dataUser, setDataUser] = useState([]);
    const [provinsi, setProvinsi] = useState([]);
    const [cart, setCart] = useState([]);
    const [kota, setKota] = useState([]);
    const [ongkir, setOngkir] = useState([]);
    const [kelurahan, setKelurahan] = useState([]);
    const [item, setItem] = useState([]);
    const [nohp, setNohp] = useState([]);
    const [kodepos, setKode] = useState([]);
    const [nama, setNama] = useState([]);
    const [alamat, setAlamat] = useState([]);
    let token=window.sessionStorage.getItem("user");
    var total=0,jml=0;
    const [provtemp, setProvtemp] = useState([]);
    const [kotatemp, setKotatemp] = useState([]);


	async function getDataUser(){
		try{
            const fetchAPI= await fetch(`/apiUser/getuserlogin`, { 
                method: 'GET',
                headers: {
                    'x-access-token': token
                } 
            });
            const res= await fetchAPI.json();
            setDataUser(res.info);
        }catch(error){
            console.error(error);
        }
	}

    async function insertDBTrans(obj, obj2, obj3, obj4){
        //INSERT SHIPMENT
        try{
            const res = await fetch("/apiUser/addShipment", { 
                method : "POST",
                headers : { "Content-Type" : "application/json",'x-access-token': token },
                body: JSON.stringify(obj)
            });
        }catch(error){
            console.error(error);
        }

        //INSERT HTRANS
        try{
            const res = await fetch("/apiUser/addHtrans", { 
                method : "POST",
                headers : { "Content-Type" : "application/json",'x-access-token': token },
                body: JSON.stringify(obj2)
            });
        }catch(error){
            console.error(error);
        }

        //INSERT HTRANS
        try{
            const res = await fetch("/apiUser/addDtrans", { 
                method : "POST",
                headers : { "Content-Type" : "application/json"},
                body: JSON.stringify(obj3)
            });
        }catch(error){
            console.error(error);
        }

        //UPDATE STOCK
        try{
            const res = await fetch("/apiUser/updateStock", { 
                method : "POST",
                headers : { "Content-Type" : "application/json"},
                body: JSON.stringify(obj4)
            });
        }catch(error){
            console.error(error);
        }

        alert("Shipping Success!!");
        history.push('/invoice');
        return window.location.reload();
    }

    function insertTrans(e){
        e.preventDefault();
        //id_shipment nama_penerima	alamat	kode_pos	no_telp	status_pengiriman	
        const status="Sedang Disiapkan";
        let temp = ", "+kotatemp+", "+provtemp

        //VAR SHIPMENT
        let obj = {
            nohp:nohp, 
            kodepos:kodepos, 
            alamat:alamat + temp,
            nama:nama,
            status:status,
            ongkir:ongkir
        }

        //VAR HTRANS
        let obj2 = {
            idpromo:window.sessionStorage.getItem("idpromotemp"), //select dari db
            promototal:window.sessionStorage.getItem("promotemp"), 
            qty:jml,
            total : total-window.sessionStorage.getItem("promotemp")+ongkir
        }

        //VAR DTRANS
        let obj3 = {
            item : item
        }
        
        //VAR STOCK
        let query = "UPDATE ukuran u\nJOIN (\n";
        for(let i=0; i<item.length; i++){
            let tempStokSneaker = item[i].stok_sneaker - item[i].jumlah_sneaker;
            if(i == 0) query += `SELECT ${item[i].id_sneaker} as id_sneaker, ${item[i].ukuran_sneaker} as ukuran_sneaker, ${tempStokSneaker} as new_stock`;
            else query += `SELECT ${item[i].id_sneaker}, ${item[i].ukuran_sneaker}, ${tempStokSneaker}`;
            
            if(i != (item.length-1))query += "\nUNION ALL\n"
            else query += "\n"
        }
        query += ") vals ON u.id_sneaker = vals.id_sneaker and u.ukuran_sneaker = vals.ukuran_sneaker\n";
        query += "SET stok_sneaker = new_stock"
        let obj4 = {
            query : query
        }

        //CEK SALDO CUKUP
        let tempPembelanjaanTotal = total-window.sessionStorage.getItem("promotemp")+ongkir
        if(window.sessionStorage.getItem("user")!=null) {
            if(dataUser.saldo < tempPembelanjaanTotal) alert("saldo tidak cukup!!!");
            else insertDBTrans(obj, obj2, obj3, obj4);
        }
        else {
            displayMidtrans(tempPembelanjaanTotal, obj, obj2, obj3, obj4);
        }
    }

    function _updateProv (event){
        var index = event.nativeEvent.target.selectedIndex;
        setProvtemp(event.nativeEvent.target[index].text);
        axios
            .get("http://127.0.0.1:3001/apiUser/getCity/"+event.target.value, {
            })
            .then((res) => {
                setKota(res.data.rajaongkir.results);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    function _setKota (event){
        var index = event.nativeEvent.target.selectedIndex;
        setKotatemp(event.nativeEvent.target[index].text);
        axios
            .post("http://127.0.0.1:3001/apiUser/getOngkir/"+event.target.value, {
            })
            .then((res) => {
                setOngkir(res.data.rajaongkir.results[0].costs[1].cost[0].value);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    useEffect(() => {
        axios
            .get("http://127.0.0.1:3001/apiUser/getProv", {
            })
            .then((res) => {
                setProvinsi(res.data.rajaongkir.results);
            })
            .catch((err) => {
                console.log(err);
            });
            
        loadCart();
        scriptMidtrans();
    }, []);

    function loadCart(){
		if(window.sessionStorage.getItem("user")!=null){
			axios
			.get("http://127.0.0.1:3001/apiUser/getCart", {
				headers: {
					'x-access-token': token
				}
			})
			.then((res) => {
				setItem(res.data[0].listCart);
                getDataUser();
			})
			.catch((err) => {
				console.log(err);
			});
		}
		else{
			if(window.sessionStorage.getItem("cart") != null){
				let sessionCart = JSON.parse(window.sessionStorage.getItem("cart"));
				let tempQty = [];
				for(let i=0; i<sessionCart.length; i++){
					sessionCart[i].counter = i;
					tempQty[i] = sessionCart[i].jumlah_sneaker;
				}
				setItem(sessionCart);
			}
		}
	}

	for (var i = 0; i < item.length; i++) {  //loop through the array
        total += item[i].harga_sneaker*item[i].jumlah_sneaker;  //Do the math!
        jml += parseInt(item[i].jumlah_sneaker);
    }
    
    //FUNCTION TOPUP
    function scriptMidtrans(){
        const script = document.createElement('script');
    
        script.src = "https://app.sandbox.midtrans.com/snap/snap.js";
        script.async = true;
        script.dataClientKey = 'SB-Mid-client-XndPVKPVENnpp6W3'
    
        document.body.appendChild(script);
    
        return () => {
        document.body.removeChild(script);
        }
    }
    async function displayMidtrans(jumlahBelanja, obj1, obj2, obj3, obj4) {
        let obj = {
            jumlahTopup: jumlahBelanja
        }
            const fetchAPI= await fetch(`/topup/getTokenMidtrans`, {
            method: 'POST',
            headers : { "Content-Type" : "application/json" },
            body: JSON.stringify(obj)
        });
        const result= await fetchAPI.json();
        window.snap.pay(result, {
            // Optional
            onSuccess: function(result){
                alert("Payment Berhasil!!!");
                insertDBTrans(obj1, obj2, obj3, obj4);
            },
            // Optional
            onPending: function(result){
                alert("pending");
            },
            // Optional
            onError: function(result){
                alert("error");
            }
        });
    }
        
	return (
    <>
    <div class="container" style={{width:"100%"}}>
        <div style={{width:"5%", float:"left"}}>&nbsp;</div>
        <div style={{width:"90%", float:"left"}}>
            <br/><br/><br/><br/>
            <h2>Shipping Address</h2>
            <form onSubmit={insertTrans}>
                <div className="form-group">
                    <label>Province</label>
                    <select onChange={_updateProv} class="form-control" id="font_style">
                        <option id="font_style" value="" disabled selected="selected">Please Select Province</option>
                        { provinsi.map(t => 
                            <option id="font_style" value={t.province_id}>{t.province}</option>
                        )}
                    </select>
                </div>
                <div className="form-group">
                    <label>City</label>
                    <select onChange={_setKota} class="form-control" id="font_style">
                        <option id="font_style" value="" disabled selected="selected">Please Select City</option>
                        { kota.map(t => 
                            <option id="font_style" value={t.city_id}>{t.type} {t.city_name}</option>
                        )}
                    </select>
                </div>
                <div className="form-group">
                    <label>Address</label>
                <input type="text" class="form-control" placeholder="Address" id="alamat" onChange={(e) => setAlamat(e.target.value)}/>
                </div>
                <div className="form-group">
                    <label>Postal Code</label>
                <input type="text" class="form-control" placeholder="Postal Code" id="kodepos" onChange={(e) => setKode(e.target.value)}/>
                </div>
                <div className="form-group">
                    <label>Recepient</label>
                <input type="text" class="form-control" placeholder="Recepient" id="nama" onChange={(e) => setNama(e.target.value)}/>
                </div>
                <div className="form-group">
                    <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="Phone Number" id="nohp" onChange={(e) => setNohp(e.target.value)}/>
                </div>
                <br/>
                <h2>Summary</h2>
                
                    <ul className={"list-group list-group-flush"}>
                        {
                            item.map(i => <li key={i.id_cart} className={"list-group-item"}>{i.nama_sneaker} - {i.ukuran_sneaker} x {i.jumlah_sneaker} ({"Rp." + (parseInt(i.harga_sneaker)*parseInt(i.jumlah_sneaker)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')})</li>)
                        }
                    </ul><br/>
                    <p>Promo : {"Rp." + window.sessionStorage.getItem("promotemp").toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</p>
                    <p>Ongkos Kirim : {"Rp." + ongkir.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</p>
                    <p>Total : {"Rp." + (parseInt(total)-parseInt(window.sessionStorage.getItem("promotemp"))+parseInt(ongkir)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</p>
                    <br></br>
                
                <input type="submit" style={{backgroundColor:"black", color:"white", letterSpacing:"2px", padding:"20px", width:"100%",marginTop:"10px"}}/>
                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
            </form>
        </div>
        <div style={{width:"5%", float:"left"}}>&nbsp;</div>
    </div>
    <br/><br/><br/><br/> 
    </>
	);
}

export default Address;