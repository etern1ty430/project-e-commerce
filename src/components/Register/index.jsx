﻿import React from "react";
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import { useHistory } from 'react-router-dom';
import './index.css';

const Register = () => {
  const history= useHistory();
  
  async function processRegister(e){
    e.preventDefault();
    let username = e.target.username.value;
    let name = e.target.name.value;
    let email = e.target.email.value;
    let password = e.target.password.value;
    let confirmpassword = e.target.confirmpassword.value;

    let fields= {
      username: username,
      name: name,
      email: email,
      password: password,
      confirmpassword: confirmpassword
    }

    if (!Object.keys(fields).every(key => fields[key] !== '')) {
        return alert('All field required.');
    }

    if (password !== confirmpassword) {
        return alert('Password & Confirm-Password not same.');
    }

    try {
        const fetchAPI= await fetch('/apiUser/register', {
            method: 'POST',
            headers: { 'Content-Type' : 'application/json' },
            body: JSON.stringify({
                username: username,
                name: name,
                email: email,
                password: password
            })
        });
        const res= await fetchAPI.json();

        if (res.status !== 200) {
            return alert(`Failed to register.\n${res.message}`);
        }
        
        alert(res.message);
        history.push(`/login`);
        return window.location.reload();
    } catch (error) {
        return console.error(error);
    }
  }
	return (
    <div class="container-register">
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <br/><br/><br/>
          <center>
            <div style={{display:"inline-block"}}>
              <a href="/login" style={{color:"black",textDecoration:"none",font:"normal 20px 'Cookie', cursive",marginLeft:"-10%",marginRight:"20%"}}>Login</a>
              <a href="/register" style={{color:"black",textDecoration:"underline",font:"normal 20px 'Cookie', cursive"}}>Register</a>
            </div><br/><br/><br/>
          </center>
            
            <form onSubmit={processRegister}>
              <div class="form-group">
                <label>Username</label>
                <input class="form-control" id="username" placeholder="Enter Username"/>
              </div>
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" id="name" placeholder="Enter Name"/>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Email"/>
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter Password"/>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Confirm Password</label>
                <input type="password" class="form-control" id="confirmpassword" placeholder="Enter Confirm Password"/>
              </div>
              <br/>
              <button type="submit" id="btn-submit-register" style={{borderRadius:"0px"}} class="btn btn-dark">Submit</button>
            </form>
        </div>
        <div class="col-sm-2"></div>
      </div>
    </div>
	);
}

export default Register;