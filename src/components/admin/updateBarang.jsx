import React, { Component, useState, useEffect, setState } from 'react';
import SideNavigation from "./SideNavigation";
import NumberFormat from 'react-number-format';
import formInput from "./formInput";
import axios from 'axios'

const UpdateBarang = () => {
    
    const [files, setFiles] = useState([]);
    const [katagori, setKatagori] = useState([]);
    const nama_barang = formInput("");
    const brand = formInput("");
    const [gambar, setGambar] = useState();
    var [harga, setHarga] = useState(0);
    var [detail, setDetail] = useState([]);

    const [jenisSelected, setJenisSelected] = useState(1);
    const [kategoriSelected, setKategoriSelected] = useState(1);

    const gambarChange = (e) => {
        setFiles(e.target.files[0]);
        console.log(e.target.files[0]);
        if (e.target.files[0] == undefined){
            setGambar();
        } else {
            setGambar(URL.createObjectURL(e.target.files[0]));
        }
        
    }

    function getIDBarang(){
        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id_barang");
        return id;
    }
    function getDetail(){
        axios.get("/api/getSneakerBYID?id_barang=" + getIDBarang())
        .then((response) => {
            console.log(response.data[0])
            setHarga(response.data[0].harga_sneaker);
            nama_barang.setValue(response.data[0].nama_sneaker);
            brand.setValue(response.data[0].brand_sneaker);
            setDetail(response.data[0]);
            setGambar(process.env.PUBLIC_URL+'/images/sneakers/' + response.data[0].gambar)
            setJenisSelected(response.data[0].jenis_sneaker);
            setKategoriSelected(response.data[0].id_kategori);
        })
        .catch((error) => {

        })
    }
    const gantiHarga = (values) => {
        setHarga(values.value)
    }
    async function getDataKatagoriFromAPI(){
        try {
            const data = await fetch("/api/getKatagori", {method : "GET"});
            setKatagori(await data.json());
        } catch (error) {
            console.log("gagal");
            console.log(error);
        }
    }
    useEffect(() => {
        getDataKatagoriFromAPI();
        getDetail();
    }, [])

    const onSubmitTambahbarang = (e) => {
        e.preventDefault();
        if (nama_barang.value == "" || harga == 0 || brand.value == ""){
            alert("pastikan semua field sudah terisi");
        } else {
            var id_katagori = document.getElementById("katagori").value;
            var jenis = document.getElementById("jenis").value;
            try {
                let bodyFormData = new FormData()
                bodyFormData.append("id_sneaker", getIDBarang());
                bodyFormData.append("id_kategori",id_katagori);
                bodyFormData.append("nama_sneaker", nama_barang.value);
                bodyFormData.append("brand_sneaker", brand.value);
                bodyFormData.append("harga_sneaker", harga);
                bodyFormData.append("jenis_sneaker", jenis);
                bodyFormData.append("gambar_lama", detail.gambar);
                bodyFormData.append('gambar',files);
                axios({
                    method: 'post',
                    url: "/api/updateSneaker",
                    data: bodyFormData,
                    config: { headers: { 'Content-Type': 'multipart/form-data' } }
                }).then(function (response) {
                    //handle success
                    console.log(response);
                    alert(response.data.message);
                    window.location.reload();
                })
            } catch (error){
                console.log(error);
            }
        }
    }
    const onClickBack = (e) => {
        window.history.back();
    }
    return (
        <React.Fragment>
            <div className="container-fluid">
                
                <div className="row">
                    <SideNavigation></SideNavigation>
                    <div className="col-10 pl-0">
                        
                        <h3 className="mt-2" style={{color:"black"}}><button className="btn btn-info mr-2" onClick={(e) => onClickBack(e)}>back</button>Update Barang</h3>
                        <hr/>
                        <form onSubmit={(e) => onSubmitTambahbarang(e)} encType="multipart/form-data">
                            <div className="row">
                                <div className="col-6">
                                    <div class="form-group">
                                        <label for="namaBarang">Nama Barang</label>
                                        <input class="form-control" id="namaBarang" placeholder="Nama Barang" value={nama_barang.value} onChange={nama_barang.onChange}/>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaBarang">katagori Barang</label>
                                        <select className="form-control" id="katagori" onChange={(e) => setKategoriSelected(e.target.value)}>
                                            {katagori.map((item, index) => {
                                                if (item.id_kategori == detail.id_kategori){
                                                    return(
                                                        <option value={item.id_kategori} selected>{item.nama_kategori}</option>
                                                    );
                                                } else {
                                                    return(
                                                        <option value={item.id_kategori}>{item.nama_kategori}</option>
                                                    );
                                                }
                                                
                                            })}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaBarang">Harga Barang</label>
                                        <NumberFormat thousandSeparator={true} prefix={'Rp.'} className="form-control" id="harga" value={harga} onValueChange={(values) => gantiHarga(values)}/>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div class="form-group">
                                        <label for="namaBarang">Brand Barang</label>
                                        <input type="text" className="form-control" placeholder="nama brand" value={brand.value} onChange={brand.onChange}/>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenisBarang">Jenis Barang</label>
                                        <select className="form-control" id="jenis" value={jenisSelected} onChange={(e) => setJenisSelected(e.target.value)}>
                                            <option value="Sport">Sport</option>
                                            <option value="Running">Running</option>
                                            <option value="Boot">Boot</option>
                                            {kategoriSelected == 1 ? <option value="Loafer">Loafer</option> : ""}
                                            {kategoriSelected == 2 ? <option value="Heels">Heels</option> : ""}
                                            {kategoriSelected == 3 ? <option value="Sandals">Sandals</option> : ""}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaBarang">Gambar Barang</label>
                                        <input type="file" className="form-control-file" onChange={(e) => gambarChange(e)}/>
                                        <img src={gambar} className="w-100 mt-2" style={{border : "1px solid #cfcfcf"}}/>
                                    </div>
                                </div>
                                <div className="col-12 text-right mb-5">
                                    <hr className="mt-0"/>
                                    <button type="submit" className="btn btn-success">Update Barang</button>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>
        
    )
}

export default UpdateBarang;