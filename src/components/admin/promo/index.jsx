/* eslint-disable react/display-name */
import React, { useState, useEffect } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ReactTable from 'react-table-v6'
import 'react-table-v6/react-table.css'
import SideNavigation from "../SideNavigation";
import NumberFormat from 'react-number-format';
import formInput from "../formInput";
import axios from 'axios'

const Promo = () => {

    const [daftarPromo, setDaftarPromo] = useState([]);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [gambar, setGambar] = useState();
    const [files, setFiles] = useState([]);
    const useFormDate = (initialvalue) => {
        const [value, setValue] = useState(initialvalue);
        const handleChange = (e) => {
            setValue(e);
        };
        return { value, onChange: handleChange };
    };
    const useFormInput = (initialvalue) => {
        const [value, setValue] = useState(initialvalue);
        const handleChange = (e) => {
            setValue(e.target.value);
        };
        return { value, onChange: handleChange };
    };

    async function getPromoFromAPI(){
        try {
            const data = await fetch("/api/getPromo", {method : "GET"});
            //console.log(data);
            setDaftarPromo(await data.json());
        } catch (error) {
            console.log("gagal");
            console.log(error);
        }
        
    }

    async function deletePromo(e){
        try {
            const data = await fetch("/api/deletePromo?kode=" + e.target.value, {method : "DELETE"});
            //console.log(data);
            if (data.status === 200){
                alert("delete berhasil");
                getPromoFromAPI();
            } else {
                alert("delete gagal");
            }
        } catch (error) {
            console.log("gagal");
            console.log(error);
        }
    }

    

    useEffect(() => {
        getPromoFromAPI();
        
    }, []);
    

    const judul = useFormInput("");
    const deskripsi = useFormInput("");
    const diskon = useFormInput("");
    const minBelanja = useFormInput("");
    const maxDiskon = useFormInput("");
    const jumlahKupon = useFormInput("");
    
    const gambarChange = (e) => {
        setFiles(e.target.files[0]);
        console.log(e.target.files[0]);
        if (e.target.files[0] == undefined){
            setGambar();
        } else {
            setGambar(URL.createObjectURL(e.target.files[0]));
        }
        
    }

    const columns = [
        {
            Header : "Kode Promo",
            accessor : "kode",
            filterable : true,
            sortable : true
        },{
            Header : "Nama Promo",
            accessor : "nama",
            filterable : true,
            sortable : true
        },{
            Header : "Diskon",
            accessor : "diskon",
            filterable : true,
            sortable : true
        },
        {
            Header : "Maksimal Diskon",
            accessor : "maksimal",
            filterable : true,
            sortable : true,
            
        },
        {
            Header : "Berlaku Sampai",
            accessor : "berlaku_sampai",
            filterable : true,
            sortable : true,
            Cell : (props) =>{
                console.log(props.original.berlaku_sampai);
                var d = new Date(props.original.berlaku_sampai);
                return(
                    <p>{d.getDate()}/{d.getMonth()}/{d.getFullYear()}</p>
                );
            }
        },
        {
            Header : "Action",
            accessor : "berlaku_sampai",
            filterable : true,
            sortable : true,
            Cell : (props) =>{
                console.log(props.original.kode);
                // var d = new Date(props.original.berlaku_sampai);
                return(
                    
                    <button value={props.original.kode} className="btn btn-primary w-100" onClick={deletePromo}>DELETE</button>
                );
            }
        }
    ]

    const onSubmitPromo = async (e) => {
        e.preventDefault();
        console.log(judul.value)
        console.log(deskripsi.value)
        console.log(diskon.value)
        console.log(minBelanja.value)
        console.log(maxDiskon.value)
        console.log(jumlahKupon.value)
        if (judul.value == "" || deskripsi.value == "" || diskon.value == "" || minBelanja.value == "" || maxDiskon.value == "" || jumlahKupon.value == ""){
            alert("Pastikan semua field sudah terisi");
        } else {
            try {
                var strDate = new Date(startDate);
                strDate.setHours(0, 0, 0);
                var eDate = new Date(endDate);
                eDate.setHours(0, 0, 0);
                let bodyFormData = new FormData()
                bodyFormData.append("judul",judul.value);
                bodyFormData.append("deskripsi", deskripsi.value);
                bodyFormData.append("diskon", diskon.value);
                bodyFormData.append("min_belanja", minBelanja.value);
                bodyFormData.append('max_diskon',maxDiskon.value);
                bodyFormData.append("tanggal_mulai", strDate.toLocaleDateString());
                bodyFormData.append("tanggal_berahkir", eDate.toLocaleDateString());
                bodyFormData.append("jumlah_kupon", jumlahKupon.value);
                bodyFormData.append("gambar2", files);
                axios({
                    method: 'post',
                    url: "/api/addPromo",
                    data: bodyFormData,
                    config: { headers: { 'Content-Type': 'multipart/form-data' } }
                }).then(function (response) {
                    //handle success
                    console.log(response);
                    alert("promo di insert");
                    window.location.reload();
                })
            } catch (error){
                console.log(error);
            }
        }
    }

    return(
        <React.Fragment>
            <div className="container-fluid">
                
                <div className="row">
                    <SideNavigation activeKey="4"></SideNavigation>
                    <div className="col-10 pl-0">
                        <h1 className="text-center">Promo Management Menu</h1>
                        <hr/>
                        <form onSubmit={(e) => onSubmitPromo(e)}>
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="kode-promo">Judul Promo</label>
                                        <input type="text" className="form-control" id="kode-promo" placeholder="Judul Promo" value={judul.value} onChange={judul.onChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="nama-promo">Deskripsi Promo</label>
                                        <textarea name="deskripsi" id="deskripsi" cols="30" rows="4" className="form-control" placeholder="deskripsi" value={deskripsi.value} onChange={deskripsi.onChange}></textarea>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="diskon">Diskon</label>
                                        <input type="number" className="form-control" id="diskon" placeholder="Diskon" name="diskon" min="1" max="100" value={diskon.value} onChange={diskon.onChange}/>
                                        <small id="diskonHelp" className="form-text text-muted">Diskon dalam persentase.</small>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="maks-diskon">Minimal Belanja</label>
                                        <input type="number" className="form-control" id="min-belanja" placeholder="Minimal belanja" name="max-diskon" min="1" value={minBelanja.value} onChange={minBelanja.onChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="maks-diskon">Maksimal Diskon</label>
                                        <input type="number" className="form-control" id="maks-diskon" placeholder="Maksimal Diskon" name="max-diskon" min="1" value={maxDiskon.value} onChange={maxDiskon.onChange}/>
                                        <small id="diskonHelp" className="form-text text-muted">Field merupakan maksimal diskon yang di berikan.</small>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label htmlFor="maks-diskon">Jumlah kupon</label>
                                        <input type="number" className="form-control" id="jumlah-kupon" placeholder="Jumlah Kupon" name="max-diskon" min="1" value={jumlahKupon.value} onChange={jumlahKupon.onChange}/>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <label>Masa Berlaku Promo</label>
                                        </div>
                                        
                                        <div className="col-auto">
                                            <p style={{display : "inline-block", marginRight : "10px"}}>From : </p>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={date => setStartDate(date)}
                                                selectsStart
                                                startDate={startDate}
                                                endDate={endDate} 
                                                className="form-control"
                                                dateFormat="dd/MM/yyyy"
                                            />
                                        </div>
                                        <div className="col-auto">
                                            <p style={{display : "inline-block", marginRight : "10px"}}>To : </p>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={date => setEndDate(date)}
                                                selectsEnd
                                                startDate={startDate}
                                                endDate={endDate}
                                                minDate={startDate} 
                                                className="form-control"
                                                dateFormat="dd/MM/yyyy"
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <label for="namaBarang">Gambar Barang</label>
                                        <input type="file" className="form-control-file" onChange={(e) => gambarChange(e)}/>
                                        <img src={gambar} className="w-100 mt-2" style={{border : "1px solid #cfcfcf"}}/>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary">Tambah Promo</button>
                        </form>
                        
                    </div>
                </div>
            </div>
        </React.Fragment>
        
    );
}

export default Promo;