import { Switch, Route } from "react-router-dom";
import React, { Component, useEffect } from 'react';
import insertBarang from "./insertBarang";
import Header from "./Header";
import Dashboard from './dashboard';
//import Katagori from "./katagori";
import DaftarProduk from "./daftarProduk";
import Promo from "./promo";
import DetailProduk from "./detail";
import KatalogPromo from "./katalogPromo";
import DetailPromo from "./detailPromo";
import UpdateBarang from './updateBarang';
import Order from './order'
import Laporan from '../Laporan/'
import Chart from '../Laporan/chart'

const Admin = () => {
    useEffect(() => {
        //window.location.reload();
    }, []);
    return(
        <React.Fragment>
            <Header/>
            <Switch>
                <Route exact path = "/admin" component = { Dashboard } />
                {/* <Route exact path = "/admin/katagori" component = { Katagori } /> */}
                <Route exact path = "/admin/insertBarang" component = { insertBarang } />
                <Route exact path = "/admin/daftarProduk" component = { DaftarProduk } />
                <Route exact path = "/admin/promo" component = { Promo } />
                <Route exact path = "/admin/katalogPromo" component = { KatalogPromo } />
                <Route path = "/admin/detailProduk" component = { DetailProduk } />
                <Route path = "/admin/detailPromo" component = { DetailPromo } />
                <Route path = "/admin/updateBarang" component = { UpdateBarang } />
                <Route path = "/admin/order" component = { Order } />
                <Route path = "/admin/laporan" component = { Laporan } />
                <Route path = "/admin/chart" component = { Chart } />
            </Switch>
        </React.Fragment>
    );
}

export default Admin;