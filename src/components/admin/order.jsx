import React, { useState, useEffect } from 'react';
import { MDBDataTableV5 } from 'mdbreact';
import { useHistory } from 'react-router-dom';
import SideNav from "./SideNavigation";
import Invoice from '../MyAccount/PurchaseList/invoice'

const Order = () =>{
	const history= useHistory();
    const [purchaseListOrder, setPurchaseListOrder] = useState([]);

    //TABLE PurchaseListForOrder
    const [ dataTableOrder, setDataTableOrder] = useState(null);
    const [ dataSetTableOrder, setDataSetTableOrder] = useState();

    //TABLE PurchaseListForHistory
    const [ dataTableHistory, setDataTableHistory] = useState(null);
    const [ dataSetTableHistory, setDataSetTableHistory] = useState();

    //POPUP LAPORAN
    const [ dataLaporanHeader, setDataLaporanHeader ] = useState([]);
    const [ dataLaporanDetail, setDataLaporanDetail ] = useState([]);

    //POPUP INVOICE
    const [popUpInvoiceHeader, setPopUpInvoiceHeader] = useState(null);
    const [popUpInvoiceDetail, setPopupInvoiceDetail] = useState(null);
    const [showInvoice, setShowInvoice] = useState(false);

    async function getPurchaseUserForOrder() {
        try {
            const data = await fetch("/purchaselist/getAllOrder", { 
                method: "GET"
            })
            .then(async response => {
                let data = await response.json();
                setPurchaseListOrder(data.dataList);
            })
            .catch(error => alert('Error! ' + error.message));
        } catch (error) {
            console.error(error);
        }
    }

    async function getInvoice() {
        try {
          const data = await fetch("/apiLaporan/getLaporan", { method: "GET" })
            .then(async response => {
                let a = await response.json();
                setDataLaporanHeader(a[0].header);
                setDataLaporanDetail(a[0].detail);
            })
        } catch (error) {
          console.error(error);
        }
    }

    function setTableForOrder(){
        let tempPurchaseListOrder = [];
        purchaseListOrder.map((item, index) => {
          if(item.status_pengiriman == "Sedang Disiapkan") tempPurchaseListOrder.push(item);
        })
    
        let tempTable = tempPurchaseListOrder.map((item, index) => {
          let date = new Date(item.tgl_beli);
        //   date.setDate(date.getDate()-1);
          return ({
              row: index+1,
              id_trans: `#${item.id_trans}`,
              pembeli: `${item.nama_penerima}`,
              tgl_beli: date.toDateString(),
              jumlah: item.jumlah,
              total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
    
              action: <button onClick={() => handleSendOrder(item.id_shipment) } 
                      style={{fontSize:"0.7vw", textAlign:"center"}}
                      className="btn btn-success">
                      &nbsp; SEND &nbsp;
                      </button>,
              invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                      style={{fontSize:"0.7vw", textAlign:"center"}}
                      className="btn btn-primary">
                      &nbsp; Invoice &nbsp;
                      </button>
            }
          );
        });
    
        setDataSetTableOrder(tempTable);
    
        setDataTableOrder({
          columns:  [
              { label: '#', field: 'row' },
              { label: 'NO. TRANS', field: 'id_trans' },
              { label: 'PEMBELI', field: 'pembeli' },
              { label: 'DATE', field: 'tgl_beli' },
              { label: 'JUMLAH', field: 'jumlah' },
              { label: 'TOTAL', field: 'total' },
              { label: 'ACTION', field: 'action' },
              { label: 'INVOICE', field: 'invoice' }
          ],
          rows: tempTable
        });
    }

    function setTableForHistory(){
        let tempPurchaseListHistory = [];
        purchaseListOrder.map((item, index) => {
          if(item.status_pengiriman != "Sedang Disiapkan") tempPurchaseListHistory.push(item);
        })
    
        let tempTable = tempPurchaseListHistory.map((item, index) => {
          let date = new Date(item.tgl_beli);
        //   date.setDate(date.getDate()-1);
          return ({
              row: index+1,
              id_trans: `#${item.id_trans}`,
              pembeli: `${item.nama_penerima}`,
              tgl_beli: date.toDateString(),
              jumlah: item.jumlah,
              total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
              status: item.status_pengiriman,
              invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                      style={{fontSize:"0.7vw", textAlign:"center"}}
                      className="btn btn-primary">
                      &nbsp; Invoice &nbsp;
                      </button>
            }
          );
        });
    
        setDataSetTableHistory(tempTable);
    
        setDataTableHistory({
          columns:  [
              { label: '#', field: 'row' },
              { label: 'NO. TRANS', field: 'id_trans' },
              { label: 'PEMBELI', field: 'pembeli' },
              { label: 'DATE', field: 'tgl_beli' },
              { label: 'JUMLAH', field: 'jumlah' },
              { label: 'TOTAL', field: 'total' },
              { label: 'STATUS', field: 'status' },
              { label: 'INVOICE', field: 'invoice' }
          ],
          rows: tempTable
        });
    }

    function handleInvoice(id_trans){
        dataLaporanHeader.map((item, index) => {
            if(item.id_trans == id_trans) {
                setPopUpInvoiceHeader(item);
            }
        })
    
        let tempDetail = [];
        let counter = 1;
        dataLaporanDetail.map((item, index) => {
            if(item.id_trans == id_trans) {
                item.counter = counter;
                tempDetail.push(item);
                counter = counter + 1;
            }
        })
        console.log(tempDetail);
        setPopupInvoiceDetail(tempDetail);
        setShowInvoice(true);
    }

    async function handleSendOrder(id_shipment){
            if(window.confirm('Are You Sure?')){
            try {
                let obj = {
                    id_shipment: id_shipment
                }
                const res = await fetch("/purchaselist/sendOrder", { 
                    method : "POST",
                    headers : { "Content-Type" : "application/json" },
                    body : JSON.stringify(obj)
                });
                history.push(`/admin/order`);
                return window.location.reload();
            } catch (error) {
                console.error(error);
            }
        }
    }

    useEffect(() => {
        getPurchaseUserForOrder();
        getInvoice();
    }, []);
    
    useEffect(() => {
        setTableForOrder();
        setTableForHistory();
    }, [purchaseListOrder, dataLaporanHeader, dataLaporanDetail]);

    return (
        <React.Fragment>
            <div className="container-fluid">
                <div className="row">
                    <SideNav activeKey="6"></SideNav>
                        <div class="col-sm-9">
                        <h4>Ongoing Order</h4>
                            { dataTableOrder && dataTableOrder.rows.length ?     
                                <MDBDataTableV5 responsive
                                                striped
                                                searchBottom={ false }
                                                data={ dataTableOrder } />
                            : 'No orders history.' }

                        <h4>History Order</h4>
                            { dataTableHistory && dataTableHistory.rows.length ?     
                                <MDBDataTableV5 responsive
                                                striped
                                                searchBottom={ false }
                                                data={ dataTableHistory } />
                            : 'No orders history.' }
                        </div>
                </div>
            </div>
            <Invoice showInvoice={showInvoice} setShowInvoice={setShowInvoice} popUpInvoiceHeader={popUpInvoiceHeader} popUpInvoiceDetail={popUpInvoiceDetail}/>
        </React.Fragment>
    );
}

export default Order;