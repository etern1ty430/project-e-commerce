﻿import React from "react";
import { useHistory } from 'react-router-dom';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser,faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const Header = () => {
	const history= useHistory();
	
	function ubah_halaman_home(){
		localStorage.setItem("halaman","home");
		history.push(`/`);//ini redirect ke index.jsx nya admin
		return window.location.reload();
	}
	function ubah_halaman_katalog(){
		localStorage.setItem("halaman","katalog");
		history.push(`/katalog`);//ini redirect ke index.jsx nya admin
		return window.location.reload();
	}
	return (
		<div style={{backgroundColor:"#292c2f"}}>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-5">
					<center>
						<br/>	
						<h5 style={{color:"white",font:"normal 30px 'Cookie', cursive"}}>admin A l a s K a k i<span style={{color:"#5383d3"}}> Q u</span></h5>
						<br/>
						
					</center>
					<br/>
				</div>
				
			</div>
		</div>
	);
}

export default Header;