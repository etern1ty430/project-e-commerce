import React, { Component, useState, useEffect, setState } from 'react';
import SideNavigation from "./SideNavigation";
import NumberFormat from 'react-number-format';
import formInput from "./formInput";
import axios from 'axios'

const InsertBarang = () => {
    const [files, setFiles] = useState([]);
    const [katagori, setKatagori] = useState([]);
    const nama_barang = formInput("");
    
    const brand = formInput("");
    const [gambar, setGambar] = useState();
    var [harga, setHarga] = useState(0);

    const [kategoriSelected, setKategoriSelected] = useState(1);
    async function getDataKatagoriFromAPI(){
        try {
            const data = await fetch("/api/getKatagori", {method : "GET"});
            setKatagori(await data.json());
        } catch (error) {
            console.log("gagal");
            console.log(error);
        }
    }

    const gambarChange = (e) => {
        setFiles(e.target.files[0]);
        console.log(e.target.files[0]);
        if (e.target.files[0] == undefined){
            setGambar();
        } else {
            setGambar(URL.createObjectURL(e.target.files[0]));
        }
        
    }


    
    useEffect(() => {
        getDataKatagoriFromAPI();
        
    }, []);
    const gantiHarga = (values) => {
        setHarga(values.value)
    }
    const onSubmitTambahbarang = (e) => {
        e.preventDefault();
        
        if (nama_barang.value == "" || harga == 0 || brand.value == ""){
            alert("pastikan semua field sudah terisi");
        } else {
            var id_katagori = document.getElementById("katagori").value;
            var jenis = document.getElementById("jenis").value;
            try {
                let bodyFormData = new FormData()
                bodyFormData.append("id_kategori",id_katagori);
                bodyFormData.append("nama_sneaker", nama_barang.value);
                bodyFormData.append("brand_sneaker", brand.value);
                bodyFormData.append("harga_sneaker", harga);
                bodyFormData.append('jenis_sneaker',jenis);
                bodyFormData.append('gambar',files);
                axios({
                    method: 'post',
                    url: "/api/addProduct",
                    data: bodyFormData,
                    config: { headers: { 'Content-Type': 'multipart/form-data' } }
                }).then(function (response) {
                    //handle success
                    console.log(response);
                    alert("barang berhasil di insert");
                    window.location.reload();
                })
            } catch (error){
                console.log(error);
            }
        }
    }

    function handleKategori(e){
        e.preventDefault();
        setKategoriSelected(e.target.value);
    }

    return (
        <React.Fragment>
            <div className="container-fluid">
                
                <div className="row">
                    <SideNavigation activeKey="2-1"></SideNavigation>
                    <div className="col-10 pl-0">
                        <h3 className="mt-2" style={{color : "black"}}>Tambah Barang</h3>
                        <hr/>
                        <form onSubmit={(e) => onSubmitTambahbarang(e)} encType="multipart/form-data">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="namaBarang">Nama Barang</label>
                                        <input class="form-control" id="namaBarang" placeholder="Nama Barang" value={nama_barang.value} onChange={nama_barang.onChange}/>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaBarang">Katagori Barang</label>
                                        <select className="form-control" id="katagori" onChange={handleKategori}>
                                            {katagori.map((item, index) => {
                                                return(
                                                    <option value={item.id_kategori}>{item.nama_kategori}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaBarang">Harga Barang</label>
                                        <NumberFormat thousandSeparator={true} prefix={'Rp.'} className="form-control" id="harga" onValueChange={(values) => gantiHarga(values)}/>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="namaBarang">Brand Barang</label>
                                        <input type="text" className="form-control" placeholder="nama brand" value={brand.value} onChange={brand.onChange}/>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenisBarang">Jenis Barang</label>
                                        <select className="form-control" id="jenis">
                                            <option value="Sport">Sport</option>
                                            <option value="Running">Running</option>
                                            <option value="Boot">Boot</option>
                                            {kategoriSelected == 1 ? <option value="Loafer">Loafer</option> : ""}
                                            {kategoriSelected == 2 ? <option value="Heels">Heels</option> : ""}
                                            {kategoriSelected == 3 ? <option value="Sandals">Sandals</option> : ""}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaBarang">Gambar Barang</label>
                                        <input type="file" className="form-control-file" onChange={(e) => gambarChange(e)}/>
                                        <img src={gambar} className="w-100 mt-2" style={{border : "1px solid #cfcfcf"}}/>
                                    </div>
                                </div>
                                <div className="col-12 text-right mb-5">
                                    <hr className="mt-0"/>
                                    <button type="submit" className="btn btn-success">Insert Barang</button>
                                </div>
                                
                            </div>
                        </form>
                        
                        
                    </div>
                </div>
            </div>
        </React.Fragment>
        
    )
}

export default InsertBarang;