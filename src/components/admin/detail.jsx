import React, { Component, useState, useEffect } from 'react';
import axios from "axios";
import { SideBySideMagnifier } from "react-image-magnifiers";
import NumberFormat from 'react-number-format';
import formInput from "./formInput";
import SideNavigation from "./SideNavigation";
import { Button, Modal } from 'react-bootstrap';
import { Icon } from 'rsuite';
import {useHistory} from 'react-router-dom';
const Detail = () => {
    const nama_sneaker = formInput("");
    const brand_sneaker = formInput("");
    const harga_sneaker = formInput("");
    const gambar = formInput("");
    const [detailSneaker, setDetailSneaker] = useState([]);
    const [ukuran, setUkuran] = useState([]);
    const history = useHistory();
    function getIDBarang(){
        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id_barang");
        return id;
    }

    async function getDetailBarang(){
        axios.get("/api/getSneakerBYID?id_barang="+getIDBarang())
        .then(function (response){
            setDetailSneaker(response.data[0]);
            nama_sneaker.setValue(response.data[0].nama_sneaker);
            brand_sneaker.setValue(response.data[0].brand_sneaker);
            harga_sneaker.setValue(response.data[0].harga_sneaker);
            gambar.setValue(response.data[0].gambar);
        }).catch(function (error){
            console.log(error);
        });
    }

    async function getUkuran(){
        axios.get("/api/getUkuran?id_barang="+getIDBarang())
        .then(function (response){
            setUkuran(response.data);
            
        }).catch(function (error){
            console.log(error);
        });
    }

    useEffect(() => {
        
        getDetailBarang();
        getUkuran();
        
    }, []);

    function displayImage(){
        if (detailSneaker.gambar != undefined){
            return(
                <SideBySideMagnifier imageSrc={process.env.PUBLIC_URL+'/images/sneakers/' + detailSneaker.gambar} className="w-75 mb-5" alwaysInPlace="true" style={{ marginLeft:"auto", marginRight:"auto", boxShadow: "10px 10px 20px #f2f2f2"}}/>
            );
        }
    }
    const [show, setShow] = useState(false);
    const [show2, setShow2] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow2(true);
    const handleClose2 = () => {
        setShow2(false)
        stokInput.setValue("");
        ukuranInput.setValue("");
    };
    var [tempStok, setTempStok] = useState("");
    var [tempUkuran, setTempUkuran] = useState("");
    var [tempID, setTempID] = useState("");
    var stokBaru = formInput("");
    var ukuranInput = formInput("");
    var stokInput = formInput("");
    const onClickEdit = (e, id, stok, ukuran) => {
        setTempID(id);
        setTempStok(stok);
        setTempUkuran(ukuran);
        stokBaru.setValue(stok);
        setShow(true)
    }

    

    const onClickEditSubmit = (e) => {
        try {
            axios.post('/api/editStok', {
                id_ukuran : tempID,
                stok_baru : stokBaru.value
            })
            .then(function (response) {
                alert(response.data.message);
                getUkuran();
                setShow(false)
                
            })
            .catch(function (error) {
                console.log(error);
            });
        } catch (error) {
            console.log(error);
        }
    }


    function displayModal() {
        return(
            <Modal show={show} onHide={handleClose} style={{opacity:1}}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Stok</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-2 pr-0 text-right">
                            <p className="mt-2">Ukuran : </p>
                        </div>
                        <div className="col-auto ml-0">
                            <p className="mt-2">{tempUkuran}</p>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="col-2 pr-0 text-right">
                            <p className="mt-2">Stok : </p>
                        </div>
                        <div className="col-auto ml-0">
                            <input type="number" name="stok" id="stok" value={stokBaru.value} onChange={stokBaru.onChange} className="form-control"/>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={(e) => onClickEditSubmit(e)}>
                    Save Changes
                </Button>
                </Modal.Footer>
            </Modal>
        )
    }

    const onClickSave = (e) => {
        e.preventDefault();
        try {
            axios.post('/api/insertUkuran', {
                id_sneaker : getIDBarang(),
                ukuran_sneaker : ukuranInput.value,
                stok_sneaker : stokInput.value
            })
            .then(function (response) {
                alert(response.data.message);
                getUkuran();
                
                handleClose2();
            })
            .catch(function (error) {
                console.log(error);
            });
        } catch (error) {
            console.log(error);
        }
    }

    function displayModal2() {
        return(
            <Modal show={show2} onHide={handleClose2} style={{opacity:1}}>
                <Modal.Header closeButton>
                    <Modal.Title>Tambah Ukuran Sneaker</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-2 pr-0 text-right">
                            <p className="mt-2">Ukuran : </p>
                        </div>
                        <div className="col-auto ml-0">
                            <input type="number" name="stok" id="stok" value={ukuranInput.value} onChange={ukuranInput.onChange} className="form-control" min="0" step="0.5"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2 pr-0 text-right">
                            <p className="mt-2">Stok : </p>
                        </div>
                        <div className="col-auto ml-0">
                            <input type="number" name="stok" id="stok" value={stokInput.value} onChange={stokInput.onChange} className="form-control"/>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleClose2}>
                    Close
                </Button>
                <Button variant="primary" onClick={(e) => onClickSave(e)}>
                    Save
                </Button>
                </Modal.Footer>
            </Modal>
        )
    }

    const updateBarang = (e) => {
        history.push("/admin/updateBarang?id_barang="+getIDBarang());
    }

    const onClickDelete = (e, id) => {
        if (window.confirm('Apakah anda akan menghapus')) {
            // Save it!
            try {
                axios.delete('/api/deleteStok/'+id)
                .then(function (response) {
                    alert(response.data.message);
                    getUkuran();
                })
                .catch(function (error) {
                    console.log(error);
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            // Do nothing!
            console.log('Thing was not saved to the database.');
        }
    }
    
    const onClickDeteleSneaker = (e) => {
        if (window.confirm('Apakah anda akan menghapus')) {
            // Save it!
            try {
                axios.post('/api/deleteSneaker', {
                    id_sneaker : getIDBarang()
                })
                .then(function (response) {
                    alert(response.data.message);
                    history.push("/admin/daftarProduk")
                })
                .catch(function (error) {
                    console.log(error);
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            // Do nothing!
            console.log('Thing was not saved to the database.');
        }
    }

    function displayUkuran(){
        if(ukuran.length > 0){
            return(
                <table className="table table-bordered mt-4">
                    <thead>
                        <tr>
                            <th>Ukuran</th>
                            <th>Stok</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ukuran.map((item, index) => {
                                return(
                                    <React.Fragment>
                                    <tr>
                                        <td>{item.ukuran_sneaker}</td>
                                        <td>{item.stok_sneaker}</td>
                                        <td>
                                            <button className="btn btn-primary mr-3" onClick={(e) => onClickEdit(e, item.id_ukuran, item.stok_sneaker, item.ukuran_sneaker)}>Edit</button>
                                            <button className="btn btn-danger" onClick={(e) => onClickDelete(e, item.id_ukuran)}>Delete</button>
                                        </td>
                                    </tr>
                                    
                                    </React.Fragment>
                                );
                            })
                        }
                    </tbody>
                </table>
            );
        }
    }
    const onClickBack = (e) => {
        window.history.back();
    }
    return (
        <React.Fragment>
            <div className="container-fluid">
            
                <div className="row">
                    <SideNavigation activeKey="2-2"></SideNavigation>
                    <div className="col-10 pl-0">
                        <h3 className="mt-2" style={{color : "black"}}><button className="btn btn-info mr-2" onClick={(e) => onClickBack(e)}>back</button>Detail Produk</h3>
                        <hr/>
                        <div className="row">
                            <div className="col-6">{displayImage()}</div>
                            <div className="col-6">
                                <p style={{fontSize:"5vh"}}>{nama_sneaker.value}</p>
                                <p style={{fontSize:"2vh"}}>{brand_sneaker.value}</p>
                                <NumberFormat className="w-100" style={{fontSize:"3vh", color:"black"}} value={harga_sneaker.value} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} />
                                <br/>
                                <button className="btn btn-success mt-3" onClick={handleShow}>Tambah Ukuran</button>
                                <button className="btn btn-danger mt-3 ml-3" onClick={(e) => onClickDeteleSneaker(e)}>Delete Sneaker <Icon className="ml-1" icon="trash"/></button>
                                <button className="btn btn-primary mt-3 ml-3" onClick={(e) => updateBarang(e)}>Update Sneaker <Icon className="ml-1" icon="pencil"/></button>
                                {displayUkuran()}
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
            {displayModal()}
            {displayModal2()}
        </React.Fragment>
    );
}

export default Detail;