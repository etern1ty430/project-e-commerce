import React, { Component, useEffect, useState } from 'react';
import SideNavigation from "./SideNavigation";
import axios from 'axios';
import { SideBySideMagnifier } from "react-image-magnifiers";
import NumberFormat from 'react-number-format';

const DetailPromo = () => {
    function getJudul(){
        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("nama");
        return id;
    }
    var [promo, setPromo] = useState([]);
    function getPromo(){
        axios.get("/api/getDetailPromo?judul="+getJudul())
        .then((response) => {
            setPromo(response.data);
            console.log(response.data);
        })
        .catch((error) => {
            console.log(error);
        })
    }
    useEffect(() => {
        getPromo();
    }, [])
    function displayImage(){
        
        if (promo.length > 0){
            return(
                <SideBySideMagnifier imageSrc={process.env.PUBLIC_URL+'/images/promo/' + promo[0].gambar} className="w-75 mb-5" alwaysInPlace="true" style={{ marginLeft:"auto", marginRight:"auto", boxShadow: "10px 10px 20px #f2f2f2"}}/>
            );
        }
        
    }

    function detailPromo(){
        if (promo.length > 0) {
            var mulai = new Date(promo[0].tanggal_mulai);
            var selesai = new Date(promo[0].tanggal_berakhir);
            return(
                <React.Fragment>
                    <p style={{fontSize:"5vh"}}>{promo[0].judul}</p>
                    <p style={{fontSize:"2vh"}}>{promo[0].deskripsi}</p>
                    <h5 style={{color : "#5383c8"}}>{promo[0].diskon}% off</h5>
                    <div className="row mt-3">
                        <div className="col-auto text-right" style={{fontSize:"3vh", color:"black"}}>Min Belanja : </div>
                        <div className="col-auto pl-0">
                            <NumberFormat className="w-100" style={{fontSize:"3vh", color:"black"}} value={promo[0].min_belanja} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-auto text-right" style={{fontSize:"3vh", color:"black"}}>Max Diskon : </div>
                        <div className="col-auto pl-0">
                            <NumberFormat className="w-100" style={{fontSize:"3vh", color:"black"}} value={promo[0].max_diskon} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} />
                        </div>
                    </div>
                    
                    <p className="mt-3 mb-0">Masa berlaku</p>
                    <p className="mt-0 mb-1 text-muted">{mulai.toLocaleDateString("en-GB")} - {selesai.toLocaleDateString("en-GB")}</p>
                    <br/>
                    <p className="font-weight-bold">Kupon yang beredar</p>
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <td>ID User</td>
                                <td>Nama User</td>
                                <td>Jumlah Kupon</td>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                promo.map((item, index) => {
                                    return(
                                        <tr>
                                            <td>{item.id_user}</td>
                                            <td>{item.nama}</td>
                                            <td>{item.jumlah_kupon}</td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </React.Fragment>
            );
        }
    }
    return (
        <React.Fragment>
            <div className="container-fluid">
                
                <div className="row">
                    <SideNavigation activeKey="5"></SideNavigation>
                    <div className="col-10 pl-0 mb-4">
                        <h3 className="mt-2" style={{color : "black"}}>Detail Promo</h3>
                        <hr/>
                        
                        <div className="row">
                            <div className="col-12 col-md-6">{displayImage()}</div>
                            <div className="col-12 col-md-6">
                                {detailPromo()}
                                
                                
                            </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default DetailPromo;