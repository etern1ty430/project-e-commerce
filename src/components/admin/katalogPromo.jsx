import React, { Component, useState, useEffect } from 'react';
import SideNavigation from "./SideNavigation";
import axios from "axios";
import {useHistory} from "react-router-dom";

const KatalogPromo = () => {
    var history = useHistory();
    var [daftarPromo, setDaftarPromo] = useState([]);
    function getDaftarPromo(){
        axios.get("/api/getDaftarPromo")
        .then((response) => {
            setDaftarPromo(response.data);
        })
        .catch((error) => {
            console.log(error);
        })
    }
    const onClickPromo = (e, nama) => {
        history.push("/admin/detailPromo?nama=" + nama);
    }
    useEffect(()=>{
        getDaftarPromo();
    },[])
    return(
        <React.Fragment>
            <div className="container-fluid">
                
                <div className="row">
                    <SideNavigation activeKey="5"></SideNavigation>
                    <div className="col-10 pl-0 mb-4">
                        <h3 className="mt-2" style={{color : "black"}}>Katalog Promosi</h3>
                        <hr/>
                        
                        <div className="row">
                            {
                                daftarPromo.map((item, index) => {
                                    var mulai = new Date(item.tanggal_mulai);
                                    var selesai = new Date(item.tanggal_berakhir);
                                    
                                    return(
                                        <div className="col-md-6 col-lg-4">
                                            <div className="card mt-3" style={{cursor : "pointer"}} onClick={(e) => onClickPromo(e, item.judul)}>
                                                <img src={process.env.PUBLIC_URL+'/images/promo/' + item.gambar} height="250vh" class="card-img-top" alt=""/>
                                                <div className="card-body">
                                                    <h6>{item.judul}</h6>
                                                    <h5 className="mb-1" style={{color : "#5383c8"}}>{item.diskon}% off</h5>
                                                    <p className="mt-0 mb-1">{mulai.toLocaleDateString("en-GB")} - {selesai.toLocaleDateString("en-GB")}</p>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default KatalogPromo;