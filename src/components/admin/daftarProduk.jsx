import React, { Component, useState, useEffect } from 'react';
import SideNavigation from "./SideNavigation";
import NumberFormat from 'react-number-format';
import formInput from "./formInput";
import ListProduct from "./listProduct";
import axios from 'axios'
import {useHistory} from "react-router-dom";

const DaftarProduk = () => {
    const [barang, setBarang] = useState([]);
    const [barangView, setBarangView] = useState([]);
    const history = useHistory();
    async function getBarangFromAPI(){
        axios.get("/api/getProduk")
        .then(function (response){
            setBarang(response.data);
            setBarangView(response.data);
            console.log(response.data)
        }).catch(function (error){
            console.log(error);
        });
    }
    useEffect(() => {
        
        getBarangFromAPI();
        
    }, []);

    

    async function cariBarang(e){
        var value = e.target.value;
        let items_temp = [...barang]
        let hasil = items_temp.filter((e)=> e.nama_sneaker.toLowerCase().includes(value.toLowerCase()))
        setBarangView(hasil)
    }
    return (
        <React.Fragment>
            <div className="container-fluid">
                
                <div className="row">
                    <SideNavigation activeKey="2-2"></SideNavigation>
                    <div className="col-10 pl-0">
                        <h3 className="mt-2" style={{color : "black"}}>Daftar Produk</h3>
                        <hr/>
                        <div className="row">
                            <div className="col-12">
                                <input type="text" id="nama" placeholder="Search" className="form-control" onChange={cariBarang}/>
                            </div>
                        </div>
                        <div className="row mt-3">
                            {barangView.map((item, index) => {
                                return (
                                    <ListProduct nama={item.nama_sneaker} harga={item.harga_sneaker} gambar={item.gambar} id={item.id_sneaker}/>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default DaftarProduk;