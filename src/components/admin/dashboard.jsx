import React, { Component } from 'react';
import SideNavigation from "./SideNavigation";
const Dashboard = () => {
    return (
        <React.Fragment>
            <div className="container-fluid">
                <div className="row">
                    <SideNavigation></SideNavigation>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Dashboard;