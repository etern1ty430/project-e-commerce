import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import {useHistory} from 'react-router-dom';

const ListProduct = (props) => {
    var nama = props.nama;
    var harga = props.harga;
    var gambar = props.gambar;
    var id = props.id;
    var history = useHistory();
    const onClickDetail = (e) => {
        history.push("/admin/detailProduk?id_barang=" + id);
    }
    return (
        <div className="col-12 col-md-3 mt-4" onClick={(e) => onClickDetail(e)}>
        <div class="card" style={{cursor:"pointer", boxShadow: "2px 2px 20px #f2f2f2"}}>
            <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/sneakers/'+ gambar } style={{height:"22rem"}} />
            <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                <h5 class="card-title" style={{color:"white"}}>{nama}</h5>
                <NumberFormat value={harga} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} style={{color:"white"}} />
            <br/>
            </div>
        </div>
        </div>
    );
}

export default ListProduct;