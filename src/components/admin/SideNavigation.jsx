import React, { Component } from 'react';
import 'rsuite/dist/styles/rsuite-default.css';
import { Sidenav, Dropdown, Icon, Nav } from 'rsuite';
import { useHistory } from 'react-router-dom';

const SideNavigation = (props) => {
    
    const history = useHistory();

    const onClickTambahProduk = (e) =>{
        
        history.push("/admin/insertBarang");
    }
    const onClickDashboard = (e) =>{
        
        history.push("/admin");
    }
    
    const onClickDaftarProduk = (e) =>{
        history.push("/admin/daftarProduk");
    }

    // const onClickKatagori = (e) =>{
    //     history.push("/admin/katagori");
    // }

    const onClickPromo = (e) => {
        history.push("/admin/promo");
    }
    const onClickKatalogPromo = (e) => {
        history.push("/admin/katalogPromo");
    }
    const onClickOrder = (e) => {
        history.push("/admin/order");
    }
    const onClickLaporan = (e) => {
        history.push("/admin/laporan");
    }
    const onClickChart = (e) => {
        history.push("/admin/chart");
    }
    const onClickLogout = (e) => {
        history.push("/");
        return window.location.reload();
    }

    return(
        <div className="col-2 pl-0" style={{zIndex:"0"}}>
            <Sidenav defaultOpenKeys={['2']} activeKey={props.activeKey} style={{backgroundColor:"#5383c8", height:"100%"}} appearance="inverse">
                <Sidenav.Body>
                    <Nav>
                        <Nav.Item eventKey="2-1" onClick={(e) => onClickTambahProduk(e)} icon={<Icon icon="plus" />}>Tambah Produk</Nav.Item>
                        <Nav.Item eventKey="2-2" onClick={(e) => onClickDaftarProduk(e)} icon={<Icon icon="shopping-bag" />}>Daftar Produk</Nav.Item>
                        {/* <Nav.Item eventKey="3" title="Katagori" icon={<Icon icon="list" />} onClick={(e) => onClickKatagori(e)}>
                            Katagori
                        </Nav.Item> */}
                        <Nav.Item eventKey="4" title="Katagori" icon={<Icon icon="money" />} onClick={(e) => onClickPromo(e)}>
                            Promo
                        </Nav.Item>
                        <Nav.Item eventKey="5" title="Katagori" icon={<Icon icon="th-large" />} onClick={(e) => onClickKatalogPromo(e)}>
                            Katalog Promo
                        </Nav.Item>
                        <Nav.Item eventKey="6" title="Katagori" icon={<Icon icon="envelope-square" />} onClick={(e) => onClickOrder(e)}>
                            Orders
                        </Nav.Item>
                        <Nav.Item eventKey="7" title="Katagori" icon={<Icon icon="order-form" />} onClick={(e) => onClickLaporan(e)}>
                            Laporan
                        </Nav.Item>
                        <Nav.Item eventKey="8" title="Katagori" icon={<Icon icon="charts" />} onClick={(e) => onClickChart(e)}>
                            Chart
                        </Nav.Item>
                        <Nav.Item eventKey="9" title="Katagori" icon={<Icon icon="sign-out" />} onClick={(e) => onClickLogout(e)}>
                            Logout
                        </Nav.Item>
                    </Nav>
                </Sidenav.Body>
            </Sidenav>
        </div>
    );
}

export default SideNavigation;