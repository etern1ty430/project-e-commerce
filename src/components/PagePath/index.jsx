﻿import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPhone, faEnvelope, faLocationArrow, faUser, faSignOutAlt,faShoppingBag,faAd,faTag, faWallet,faAngleDoubleRight } from "@fortawesome/free-solid-svg-icons"
import { Modal } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

import Navbar from "reactjs-navbar";
import "reactjs-navbar/dist/index.css";
import "./index.css";

const PagePath = () => {
	let path="HOME/"+window.location.href.substr(22).toUpperCase();
	let finalpath=[];
	let explodepath=path.split("/");
	function check(){
		if(explodepath[1]=="" || explodepath[1]=="LOGIN" || explodepath[1]=="REGISTER" || explodepath[1]=="MYACCOUNT" || explodepath[1]=="ADMIN" || explodepath[1]=="INVOICE" ){
			return(
				<>
				</>
			);
		}
		else{
			if(explodepath[2]!=null){
				if(explodepath[2].length==1){
				path="";
				for (let i = 0; i < explodepath.length; i++) {
					if(i==2){
						if(explodepath[2]=="1")path=path+"MEN/"
						else if(explodepath[2]=="2")path=path+"WOMAN/"
						else if(explodepath[2]=="3")path=path+"KIDS/"
					}
					else{
						path=path+explodepath[i]+"/";
					}
				}
				path=path.substr(0,path.length-1);
				let explodefinal=path.split("/");
				finalpath=[];
				for (let i = 0; i < explodefinal.length; i++) {
					if(i==explodefinal.length-1){
						finalpath.push(explodefinal[i]);
					}
					else{
						finalpath.push(explodefinal[i]);
						finalpath.push(<FontAwesomeIcon icon={ faAngleDoubleRight } style={{width:"18px",height:"20px",marginLeft:"0.5%",marginRight:"0.5%"}}/>);
					}
				}
			}
			}
			else{
				let explodefinal=path.split("/");
				finalpath=[];
				for (let i = 0; i < explodefinal.length; i++) {
					if(i==explodefinal.length-1){
						finalpath.push(explodefinal[i]);
					}
					else{
						finalpath.push(explodefinal[i]);
						finalpath.push(<FontAwesomeIcon icon={ faAngleDoubleRight } style={{width:"18px",height:"20px",marginLeft:"0.5%",marginRight:"0.5%"}}/>);
					}
				}
			}
			return(
				<>
					<br/><br/><br/>
					<h5 style={{marginLeft:"4%"}}>
						{
							finalpath.map((item, index) => {
								return(item);
							})
						}
					</h5>
				</>
			);
		}
	}
	return(
		check()
	)
}

export default PagePath;