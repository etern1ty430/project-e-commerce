import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faWallet } from "@fortawesome/free-solid-svg-icons"
import { useHistory } from 'react-router-dom';
import './topup.css';

const Topup = () => {
  let token=window.sessionStorage.getItem("user");
  const history= useHistory();
  
  //VARIABEL TOPUP
  const [jumlahTopup, setJumlahTopup] = useState(0);
  const [saldo, setSaldo] = useState(0);

  //FUNCTION TOPUP
  function scriptMidtrans(){
    getDataSaldo();
    const script = document.createElement('script');
  
    script.src = "https://app.sandbox.midtrans.com/snap/snap.js";
    script.async = true;
    script.dataClientKey = 'SB-Mid-client-XndPVKPVENnpp6W3'
  
    document.body.appendChild(script);
  
    return () => {
      document.body.removeChild(script);
    }
  }
  async function getDataSaldo(){
    const fetchAPI= await fetch(`/apiUser/getuserlogin`, {
      method: 'GET',
      headers : { 
        'x-access-token': token
      }
    });
    const res= await fetchAPI.json();
    setSaldo(res.info.saldo);
  }
  async function displayMidtrans() {
    let obj = {
      jumlahTopup: jumlahTopup
    }
		const fetchAPI= await fetch(`/topup/getTokenMidtrans`, {
      method: 'POST',
      headers : { "Content-Type" : "application/json" },
      body: JSON.stringify(obj)
    });
    const result= await fetchAPI.json();
    window.snap.pay(result, {
      // Optional
      onSuccess: function(result){
        alert("Topup Berhasil!!!");
        tambahSaldo();
      },
      // Optional
      onPending: function(result){
        alert("pending");
      },
      // Optional
      onError: function(result){
        alert("error");
      }
    });
  }
  async function tambahSaldo(){
    let obj = {
      jumlahTopup: jumlahTopup
    }
		const fetchAPI= await fetch(`/topup/tambahSaldo`, {
      method: 'POST',
      headers : { 
        "Content-Type" : "application/json",
        'x-access-token': token
      },
      body: JSON.stringify(obj)
    });
    const res= await fetchAPI.json();
    history.push(`/myaccount/4`);
    return window.location.reload();
  }

  useEffect(() => {
    scriptMidtrans();
  }, []);
  return(
    <>
        <div class="topup-dekstop">
          <br/><br/>
          <div class="row">
            <div class="col-sm-1" style={{textAlign:"right", fontSize:"23px"}}>
                <br/><br/>
                <label>Rp. </label>
            </div>
            <div class="col-sm-9">
                <label style={{fontSize:"23px"}}>Saldo : {"Rp. " + saldo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</label><br/><br/>
                <input type="number" class="form-control" min="0" step="100" value={jumlahTopup} onChange={(e) => setJumlahTopup(e.target.value)}/>
            </div>
            <div class="col-sm-2" style={{fontSize:"23px"}}>
                <br/><br/>
                <button class="btn btn-dark btn-block" id="pay-button" onClick={() => displayMidtrans()} style={{width:"80%"}}><FontAwesomeIcon icon={ faWallet } style={{width:"18px",height:"18px"}}/>&nbsp;&nbsp;TOPUP</button>
            </div>
          </div>
        </div>
        <div class="topup-mobile" style={{display:"none"}}>
          <h4 style={{marginLeft:"5%"}}>Topup : </h4>
          <br/>
          <div class="row">
            <div class="col-sm-10" style={{paddingLeft:"9%",paddingRight:"9%"}}>
                <label style={{fontSize:"18px"}}>Saldo : {"Rp. " + saldo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</label>
                <input type="number" class="form-control" min="0" step="100" value={jumlahTopup} onChange={(e) => setJumlahTopup(e.target.value)}/>
            </div>
            <div class="col-sm-2" style={{fontSize:"23px",paddingLeft:"9%",paddingRight:"9%"}}>
              <br/>
                <button class="btn btn-dark btn-block" id="pay-button" onClick={() => displayMidtrans()} style={{width:"100%"}}><FontAwesomeIcon icon={ faWallet } style={{width:"18px",height:"18px"}}/>&nbsp;&nbsp;TOPUP</button>
            </div>
          </div>
          <br/>
        </div>
    </>
  );
}

export default Topup;