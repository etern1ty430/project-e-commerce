import React, { useState, useEffect } from 'react';
import { MDBDataTableV5 } from 'mdbreact';
import Rate from './rate'
import Invoice from './invoice'
import { useHistory } from 'react-router-dom';

const PurchaseList = () => {
	const history= useHistory();
  let token=window.sessionStorage.getItem("user");
  const [purchaseListRating, setPurchaseListRating] = useState([]);
  const [listDetail, setListDetail] = useState([]);

  //TABLE PurchaseListForRating
  const [ dataTableRating, setDataTableRating] = useState(null);
  const [ dataSetTableRating, setDataSetTableRating] = useState();

  //TABLE PurchaseListForUpcoming
  const [ dataTableUpcoming, setDataTableUpcoming ]= useState();
  const [ dataSetTableUpcoming, setDataSetTableUpcoming] = useState();

  //TABLE PurchaseListForHistory
  const [ dataTableHistory, setDataTableHistory ]= useState();
  const [ dataSetTableHistory, setDataSetTableHistory] = useState();

  //TABLE PurchaseListForPackage
  const [ dataTablePackage, setDataTablePackage ]= useState();
  const [ dataSetTablePackage, setDataSetTablePackage] = useState();

  //POPUP LAPORAN
  const [ dataLaporanHeader, setDataLaporanHeader ] = useState([]);
  const [ dataLaporanDetail, setDataLaporanDetail ] = useState([]);

  //POPUP RATE
  const [showRate, setShowRate] = useState(false);
  const [dataRate, setDataRate] = useState([]);

  //POPUP INVOICE
  const [popUpInvoiceHeader, setPopUpInvoiceHeader] = useState(null);
  const [popUpInvoiceDetail, setPopupInvoiceDetail] = useState(null);
  const [showInvoice, setShowInvoice] = useState(false);


  //TABLE FOR ANDROID
  const [ dataTableRatingHP, setDataTableRatingHP] = useState(null);
  const [ dataSetTableRatingHP, setDataSetTableRatingHP] = useState();
  const [ dataTableUpcomingHP, setDataTableUpcomingHP ]= useState();
  const [ dataSetTableUpcomingHP, setDataSetTableUpcomingHP] = useState();
  const [ dataTableHistoryHP, setDataTableHistoryHP ]= useState();
  const [ dataSetTableHistoryHP, setDataSetTableHistoryHP] = useState();
  const [ dataTablePackageHP, setDataTablePackageHP ]= useState();
  const [ dataSetTablePackageHP, setDataSetTablePackageHP] = useState();

  async function getPurchaseUserForRating() {
    try {
      const data = await fetch("/purchaselist/getListPurchaseRating", { 
        method: "GET",
        headers: {'x-access-token': token}
      })
      .then(async response => {
          let data = await response.json();
          setPurchaseListRating(data.dataList);
          setListDetail(data.listBarang);
          //setTableForReview(data, data.listBarang);
      })
      .catch(error => alert('Error! ' + error.message));
    } catch (error) {
        console.error(error);
    }
  }

  async function getInvoice() {
    try {
      const data = await fetch("/apiLaporan/getLaporan", { method: "GET" })
      .then(async response => {
          let a = await response.json();
          setDataLaporanHeader(a[0].header);
          setDataLaporanDetail(a[0].detail);
      })
    } catch (error) {
      console.error(error);
    }
  }

  function setTableForReview(){
    let tempPurchaseListRating = [];
    purchaseListRating.map((item, index) => {
      if(item.status_pengiriman == "Sudah Diterima") tempPurchaseListRating.push(item);
    })

    let tempTable = tempPurchaseListRating.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          row: index+1,
          id_trans: `#${item.id_trans}`,
          id_user: `#${item.id_user}`,
          tgl_beli: date.toDateString(),
          jumlah: item.jumlah,
          total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),

          action: <button onClick={() => handleRating(item.id_trans, listDetail) } 
                  style={{fontSize:"0.7vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; RATE &nbsp;
                  </button>,
          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"0.7vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      );
    });

    setDataSetTableRating(tempTable);

    setDataTableRating({
      columns:  [
          { label: '#', field: 'row' },
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'ID. USER', field: 'id_user' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'JUMLAH', field: 'jumlah' },
          { label: 'TOTAL', field: 'total' },
          { label: 'ACTION', field: 'action' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable
    });



    //FOR ANDROID
    let tempTable2 = tempPurchaseListRating.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          id_trans: `#${item.id_trans}`,
          tgl_beli: date.toDateString(),

          action: <button onClick={() => handleRating(item.id_trans, listDetail) } 
                  style={{fontSize:"2vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; RATE &nbsp;
                  </button>,
          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"2vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      );
    });

    setDataSetTableRatingHP(tempTable2);

    setDataTableRatingHP({
      columns:  [
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'ACTION', field: 'action' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable2
    });
  }

  function setTableForHistory(){
    let tempPurchaseListHistory = [];
    purchaseListRating.map((item, index) => {
      if(item.status_pengiriman == "Selesai") tempPurchaseListHistory.push(item);
    })

    let tempTable = tempPurchaseListHistory.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          row: index+1,
          id_trans: `#${item.id_trans}`,
          id_user: `#${item.id_user}`,
          tgl_beli: date.toDateString(),
          jumlah: item.jumlah,
          total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),

          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"0.7vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      )}
    );
    setDataSetTableHistory(tempTable);

    setDataTableHistory({
      columns:  [
          { label: '#', field: 'row' },
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'ID. USER', field: 'id_user' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'JUMLAH', field: 'jumlah' },
          { label: 'TOTAL', field: 'total' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable
    });



    //FOR ANDROID
    let tempTable2 = tempPurchaseListHistory.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          id_trans: `#${item.id_trans}`,
          tgl_beli: date.toDateString(),

          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"2vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      )}
    );
    setDataSetTableHistoryHP(tempTable2);

    setDataTableHistoryHP({
      columns:  [
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable2
    });
  }

  function setTableForPackage(){
    let tempPurchaseListHistory = [];
    purchaseListRating.map((item, index) => {
      if(item.status_pengiriman == "Sedang Disiapkan") tempPurchaseListHistory.push(item);
    })

    let tempTable = tempPurchaseListHistory.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          row: index+1,
          id_trans: `#${item.id_trans}`,
          id_user: `#${item.id_user}`,
          tgl_beli: date.toDateString(),
          jumlah: item.jumlah,
          total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),

          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"0.7vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      )}
    );
    setDataSetTablePackage(tempTable);

    setDataTablePackage({
      columns:  [
          { label: '#', field: 'row' },
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'ID. USER', field: 'id_user' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'JUMLAH', field: 'jumlah' },
          { label: 'TOTAL', field: 'total' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable
    });




    //FOR ANDROID
    let tempTable2 = tempPurchaseListHistory.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          id_trans: `#${item.id_trans}`,
          tgl_beli: date.toDateString(),

          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"2vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      )}
    );
    setDataSetTablePackageHP(tempTable2);

    setDataTablePackageHP({
      columns:  [
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable2
    });
  }

  function setTableForUpcoming(){
    let tempPurchaseListUpcoming = [];
    purchaseListRating.map((item, index) => {
      if(item.status_pengiriman == "Sedang Dikirim") tempPurchaseListUpcoming.push(item);
    })

    let tempTable = tempPurchaseListUpcoming.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          row: index+1,
          id_trans: `#${item.id_trans}`,
          id_user: `#${item.id_user}`,
          tgl_beli: date.toDateString(),
          jumlah: item.jumlah,
          total: "Rp." + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),

          action: <button onClick={() => handleConfirmOrder(item.id_shipment) } 
                  style={{fontSize:"0.7vw", textAlign:"center"}}
                  className="btn btn-success">
                  &nbsp; Confirm Order &nbsp;
                  </button>,
          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"0.7vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      )}
    );
    setDataSetTableUpcoming(tempTable);

    setDataTableUpcoming({
      columns:  [
          { label: '#', field: 'row' },
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'ID. USER', field: 'id_user' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'JUMLAH', field: 'jumlah' },
          { label: 'TOTAL', field: 'total' },
          { label: 'ACTION', field: 'action' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable
    });




    //FOR ANDROID
    let tempTable2 = tempPurchaseListUpcoming.map((item, index) => {
      let date = new Date(item.tgl_beli);
      // date.setDate(date.getDate()-1);
      return ({
          id_trans: `#${item.id_trans}`,
          tgl_beli: date.toDateString(),

          action: <button onClick={() => handleConfirmOrder(item.id_shipment) } 
                  style={{fontSize:"2vw", textAlign:"center"}}
                  className="btn btn-success">
                   Confirm 
                  </button>,
          invoice: <button onClick={() => handleInvoice(item.id_trans) } 
                  style={{fontSize:"2vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
        }
      )}
    );
    setDataSetTableUpcomingHP(tempTable2);

    setDataTableUpcomingHP({
      columns:  [
          { label: 'NO. TRANS', field: 'id_trans' },
          { label: 'DATE', field: 'tgl_beli' },
          { label: 'ACTION', field: 'action' },
          { label: 'INVOICE', field: 'invoice' }
      ],
      rows: tempTable2
    });
  }

  function handleRating(id_trans, dataDetail){
    let temp = [];
    let counter = 0;
    for(let i=0; i<dataDetail.length; i++){
      if(dataDetail[i].id_trans == id_trans) {
        temp.push(dataDetail[i]);
        temp[counter].counter = counter;
        counter = counter+1;
      }
    }
    setDataRate(temp);
    setShowRate(true);
  }
  
  function handleInvoice(id_trans){
    dataLaporanHeader.map((item, index) => {
      if(item.id_trans == id_trans) setPopUpInvoiceHeader(item);
    })

    let tempDetail = [];
    let counter = 1;
    dataLaporanDetail.map((item, index) => {
      if(item.id_trans == id_trans) {
        item.counter = counter;
        tempDetail.push(item);
        counter = counter + 1;
      }
    })
    setPopupInvoiceDetail(tempDetail);
    setShowInvoice(true);
  }

  async function handleConfirmOrder(id_shipment){
		if(window.confirm('Are You Sure Already Got All Orders?')){
      try {
        let obj = {
          id_shipment: id_shipment
        }
        const res = await fetch("/purchaselist/confirmOrder", { 
            method : "POST",
            headers : { "Content-Type" : "application/json" },
            body : JSON.stringify(obj)
        });
        history.push(`/myaccount/2`);
        return window.location.reload();
      } catch (error) {
          console.error(error);
      }
    }
  }

  useEffect(() => {
    getPurchaseUserForRating();
    getInvoice();
  }, []);

  useEffect(() => {
    setTableForReview();
    setTableForHistory();
    setTableForUpcoming();
    setTableForPackage();
  }, [purchaseListRating, listDetail, dataLaporanHeader, dataLaporanDetail]);

  return(
    <div style={{marginLeft:"5%"}}>
      <br/><br/>
      <div class="container_purchasetable_PC">
        <h4>Your Orders (Still Package): </h4>
        { dataTablePackage && dataTablePackage.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTablePackage }/>
          : 'No orders history.' }<hr/>

        <h4>Upcoming Orders : </h4>
        { dataTableUpcoming && dataTableUpcoming.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTableUpcoming } />
          : 'No orders history.' }<hr/>

        <h4>Purchase List : </h4>
        { dataTableRating && dataTableRating.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTableRating } />
          : 'No orders history.' }<hr/>

        <h4>History Purchase : </h4>
        { dataTableHistory && dataTableHistory.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTableHistory } />
          : 'No orders history.' }
      </div>


      <div class="container_purchasetable_HP">
        <h4>Your Orders (Still Package): </h4>
        { dataTablePackageHP && dataTablePackageHP.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTablePackageHP }
                              style={{fontSize:"10px"}}/>
          : 'No orders history.' }<hr/>

        <h4>Upcoming Orders : </h4>
        { dataTableUpcomingHP && dataTableUpcomingHP.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTableUpcomingHP }
                              style={{fontSize:"10px"}}/>
          : 'No orders history.' }<hr/>

        <h4>Purchase List : </h4>
        { dataTableRatingHP && dataTableRatingHP.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTableRatingHP }
                              style={{fontSize:"10px"}}/>
          : 'No orders history.' }<hr/>

        <h4>History Purchase : </h4>
        { dataTableHistoryHP && dataTableHistoryHP.rows.length ?     
              <MDBDataTableV5 responsive
                              striped
                              searchBottom={ false }
                              data={ dataTableHistoryHP }
                              style={{fontSize:"10px"}}/>
          : 'No orders history.' }
      </div>
      <Rate showRate={showRate} setShowRate={setShowRate} dataRate={dataRate}/>
      <Invoice showInvoice={showInvoice} setShowInvoice={setShowInvoice} popUpInvoiceHeader={popUpInvoiceHeader} popUpInvoiceDetail={popUpInvoiceDetail}/>
    </div>
  );
}

export default PurchaseList;