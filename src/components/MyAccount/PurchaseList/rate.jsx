import React, { useState, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import StarRatingComponent from 'react-star-rating-component';
import ReactWhatsapp from 'react-whatsapp';
import { useHistory } from 'react-router-dom';
import './index.css'

const Rate = ({showRate, setShowRate, dataRate}) => {
	const history= useHistory();
    let token=window.sessionStorage.getItem("user");

    const [descReview, setDescReview] = useState([]);
    const [rating, setRating] = useState([]);

    const [filesUpload, setFilesUpload] = useState([]); //SIMPAN PATH GAMBAR DI DB
    const [gambar, setGambar] = useState([]); //UNTUK MUNCULKAN GAMBAR

    const [isComplaint, setIsComplaint] = useState(false);

    const gambarChange = (e, counter) => {
        let tempFiles = [...filesUpload];//cloning array to diff memory
        let tempGambar = [...gambar];

        if (e.target.files[0]){
            tempGambar[counter] = (URL.createObjectURL(e.target.files[0]));
            tempFiles[counter] = e.target.files[0];
        }
        else{
            tempGambar[counter] = "";
            tempFiles[counter] = null;
        }
        setGambar(tempGambar);
        setFilesUpload(tempFiles);
    }

    function handleClosePopup() { 
        setShowRate(false);
        setDescReview([]);
        setRating([]);
        setFilesUpload([]);
        setGambar([]);
        setIsComplaint(false);
    }

    useEffect(() => {
        let temp = [];
        let tempfile = [];
        for(let i=0; i<dataRate.length; i++){
            temp[i] = "-";
            tempfile[i] = null;
        }
        setDescReview(temp);
        setFilesUpload(tempfile);
    }, [dataRate]);

    useEffect(() => {
        let tempIsComplaint = false;
        for(let i=0; i<rating.length; i++){
            if(parseInt(rating[i]) <= 2)tempIsComplaint = true;
        }
        setIsComplaint(tempIsComplaint);
    }, [rating])

    function handleChangeDescReview(e, counter){
        let temp = [...descReview];
        temp[counter] = e.target.value;
        setDescReview(temp);
    }

    function onStarClick(nextValue, prevValue, name, counter) {
        let temp = [...rating];
        let index = counter.split(":");
        temp[parseInt(index[1])] = parseInt(prevValue);
        setRating(temp);
    }

    async function onSubmitReview(e){
        e.preventDefault();
        let q = "INSERT into review VALUES "
        let jumlah_data = 0;
        let bodyFormData = new FormData();
        //alert(dataRate.length);
        console.log(dataRate);
        for(let i=0; i<dataRate.length; i++){
            //alert(rating[i] + "-" + filesUpload[i]);
            if(rating[i] && filesUpload[i]){
                //alert(i+"");
                jumlah_data = jumlah_data+1;
                bodyFormData.append("gambar"+i, filesUpload[i]);
                q += "(";
                q += `null, 'id_user', ${dataRate[i].id_sneaker}, '${descReview[i]}', ${rating[i]}, now(), 'gambar_filename'`;
                q += "),\n";
            }
        }
        q = q.substr(0,q.length-2);
        bodyFormData.append("q", q);
        bodyFormData.append("jumlah_data", jumlah_data);
        let message = "";
        if(jumlah_data > 0){
            const res = await fetch("/purchaselist/addReview", { 
                method : "POST",
                body : bodyFormData,
                headers: {
                    'x-access-token': token
                }
            });
            let data = await res.json();
            message = data.message;
        }

        //UPDATE STATUS SHIPMENT
        let obj = {
            id_shipment: dataRate[0].id_shipment
        }
        const res = await fetch("/purchaselist/updateStatusShipment", { 
            method : "POST",
            headers : { "Content-Type" : "application/json" },
            body : JSON.stringify(obj)
        });

        alert(message);
        setShowRate(false); 
        if(token) {
            history.push(`/myaccount/2`);
        }
        return window.location.reload();
    }

  return(
    <>
        <Modal size="lg" style={{ fontSize: '1.25vw', opacity:1 }}
                show={ showRate } 
                onHide={ handleClosePopup }>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            RATING
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="mt-4" id="container_rate_PC">
                            <div className="card mb-4">
                                <form onSubmit={onSubmitReview}>
                                {
                                    dataRate.map((item, index) => {
                                        return (
                                            <div className="card-body">
                                                <div className="row">
                                                    <div class="col-sm-3">
                                                        <img className="col-12" src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar_sneaker} />
                                                    </div>
                                                    <div class="col-sm-3" style={{fontSize:"15px"}}>
                                                        <br/>
                                                        <label style={{fontSize:"20px"}}>{item.nama_sneaker}</label><br/>
                                                        <label >{item.brand_sneaker}</label><br/>
                                                        <b><label>{"Rp." + item.harga_satuan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</label></b><br/>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <StarRatingComponent 
                                                            name={"rate:" + item.counter}
                                                            value={rating[item.counter]}
                                                            onStarClick={onStarClick.bind(this, item.counter)}
                                                        />
                                                        <input type="file" style={{fontSize:"10px", marginBottom:"3%"}} name={"file" + item.counter} className="form-control-file" onChange={(e) => gambarChange(e, item.counter)} multiple/>
                                                        <textarea style={{width:'100%', height:'50%', fontSize:'15px'}} onChange={(e) => handleChangeDescReview(e, item.counter)}/>
                                                        
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <img src={gambar[item.counter]} className="col-12"/>
                                                    </div>
                                                </div>
                                                <hr/>
                                            </div>
                                        )
                                    })
                                }
                                <center>
                                    <button class="btn btn-dark btn-block" style={{width:"90%"}}>Rate</button>
                                    {isComplaint ? <ReactWhatsapp class="btn btn-dark btn-block" number="+6281233126565" message="Permisi saya mau complain ..." style={{width:"90%"}}>Rate & Complaint</ReactWhatsapp> : ""}
                                    <br/>
                                </center>
                                </form>
                            </div>
                        </div>



                        <div className="mt-4" id="container_rate_HP">
                            <div className="card mb-4">
                                <form onSubmit={onSubmitReview}>
                                {
                                    dataRate.map((item, index) => {
                                        return (
                                            <div className="card-body">
                                                <div style={{width:"100%"}}>
                                                    <div style={{width:"50%", float:"left"}}>
                                                        <img className="col-12" src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar_sneaker} />
                                                    </div>
                                                    <div style={{fontSize:"15px", width:"50%", float:"left"}}>
                                                        <br/>
                                                        <label style={{fontSize:"20px"}}>{item.nama_sneaker}</label><br/>
                                                        <label >{item.brand_sneaker}</label><br/>
                                                        <b><label>{"Rp." + item.harga_satuan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</label></b><br/>
                                                    </div>
                                                </div>

                                                <div style={{clear:"both"}}/><br/><br/><br/><br/>

                                                <div style={{width:"100%"}}>
                                                    <div style={{float:"left", width:"50%"}}>
                                                        <div style={{fontSize:"20px"}}>
                                                            <StarRatingComponent 
                                                                name={"rate:" + item.counter}
                                                                value={rating[item.counter]}
                                                                onStarClick={onStarClick.bind(this, item.counter)}
                                                            />
                                                        </div>
                                                        <input type="file" style={{fontSize:"10px", marginBottom:"3%"}} name={"file" + item.counter} className="form-control-file" onChange={(e) => gambarChange(e, item.counter)} multiple/>
                                                        <textarea style={{width:'100%', height:'50%', fontSize:'15px'}} onChange={(e) => handleChangeDescReview(e, item.counter)}/>
                                                    </div>
                                                    <div style={{float:"left", width:"50%"}}>
                                                        <img src={gambar[item.counter]} className="col-12"/>
                                                    </div>
                                                </div>
                                                
                                                <br style={{clear:"both"}}/>
                                                <hr/>
                                            </div>
                                        )
                                    })
                                }
                                <center>
                                    <button class="btn btn-dark btn-block" style={{width:"90%"}}>Rate</button>
                                    {isComplaint ? <ReactWhatsapp class="btn btn-dark btn-block" number="+6281233126565" message="Permisi saya mau complain ..." style={{width:"90%"}}>Rate & Complaint</ReactWhatsapp> : ""}
                                    <br/>
                                </center>
                                </form>
                            </div>
                        </div>
                    </Modal.Body>
        </Modal>
    </>
  );
}

export default Rate;