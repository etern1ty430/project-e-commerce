import React, { useState, useEffect } from 'react';
import './account_info.css';

const AccountInformation = () => {
  let token=window.sessionStorage.getItem("user");
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [currentpassword, setCurrentpassword] = useState("");


  async function getUserLogin(){
    try {
      const fetchAPI= await fetch(`/apiUser/getuserlogin`, { 
          method: 'GET',
          headers: {
              'x-access-token': token
          } 
      });
      const res= await fetchAPI.json();
      
      setUsername(res.info.username);
      setName(res.info.nama);
      setEmail(res.info.email);
      setCurrentpassword(res.info.password);
    } catch (error) {
        console.error(error);
    }
  }
  async function handleSubmitChange(e){
    e.preventDefault();
    let password=currentpassword;
    if(e.target.currentpassword.value || e.target.newpassword.value){
      if(e.target.newpassword.value==e.target.confirmpassword.value && e.target.newpassword.value!=""){
        try {
          const fetchAPI= await fetch('/apiUser/editprofile', {
              method: 'POST',
              headers: { 
                'Content-Type' : 'application/json',
                'x-access-token': token 
              },
              body: JSON.stringify({
                  name: name,
                  email: email,
                  currentpasswordhash:password,
                  currentpassword:e.target.currentpassword.value,
                  newpassword:e.target.newpassword.value,
              })
          });
          const res= await fetchAPI.json();
    
          if (res.status !== 200) {return alert(`Failed to Edit Profile.\n${res.message}`);}
          alert(res.message);
          return window.location.reload();
        } catch (error) {
            return console.error(error);
        }
      }
      else{return alert("New Password and Confirm Password Didn't Match or Empty!");}
    }
    else{
      try {
        const fetchAPI= await fetch('/apiUser/editprofilewithoutpassword', {
            method: 'POST',
            headers: { 
              'Content-Type' : 'application/json',
              'x-access-token': token 
            },
            body: JSON.stringify({
                name: name,
                email: email
            })
        });
        const res= await fetchAPI.json();
  
        if (res.status !== 200) {return alert(`Failed to Edit Profile.\n${res.message}`);}
        alert(res.message);
        return window.location.reload();
      } catch (error) {
          return console.error(error);
      }
    }
  }
  function handleUsername(e){setUsername(e.target.value);}
  function handleName(e){setName(e.target.value);}
  function handleEmail(e){setEmail(e.target.value);}

  useEffect(() => {
    getUserLogin();
  }, []);
  return(
    <>
    <div class="account-information-dekstop">
      <form onSubmit={handleSubmitChange}>
        <br/><br/>
        <h4 style={{marginLeft:"5%"}}>Account Detail : </h4>
        <br/>
        <div style={{marginLeft:"13%",border:"2px solid #DCDCDC",padding:"3%"}}>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Username : </label>
                <input class="form-control" id="username" onChange={handleUsername} value={username} placeholder="Enter Username" disabled/>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Name : </label>
                <input class="form-control" id="name" onChange={handleName} value={name} placeholder="Enter Name"/>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Email : </label>
            <input class="form-control" id="email" onChange={handleEmail} value={email} placeholder="Enter Email"/>
          </div> 
        </div> 
        <br/>  
        <h4 style={{marginLeft:"5%"}}>Password Change : </h4>
        <br/>
        <div style={{marginLeft:"13%",border:"2px solid #DCDCDC",padding:"3%"}}>
          <div class="form-group">
            <label for="exampleInputPassword1">Current Password</label>
            <input type="password" class="form-control" id="currentpassword" placeholder="Leave Blank To Leave Unchange"/>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="exampleInputPassword1">New Password</label>
                <input type="password" class="form-control" id="newpassword" placeholder="Leave Blank To Leave Unchange"/>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="exampleInputPassword1">Confirm New Password</label>
                <input type="password" class="form-control" id="confirmpassword"/>
              </div>
            </div>
          </div>
        </div> 
          <br/><br/>
          <button type="submit" style={{marginLeft:"6%",borderRadius:"0px"}} class="btn btn-dark btn-block">Save Change</button> 
   
      </form>
      <br/><br/><br/><br/><br/>
    </div>

    <div class="account-information-mobile" style={{display:"none"}}>
      <form onSubmit={handleSubmitChange}>
        <h4 style={{marginLeft:"5%"}}>Account Detail : </h4>
        <br/>
        <div style={{padding:"5%"}}>
        <div style={{border:"2px solid #DCDCDC",padding:"5%"}}>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Username : </label>
                <input class="form-control" id="username" onChange={handleUsername} value={username} placeholder="Enter Username" disabled/>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Name : </label>
                <input class="form-control" id="name" onChange={handleName} value={name} placeholder="Enter Name"/>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Email : </label>
            <input class="form-control" id="email" onChange={handleEmail} value={email} placeholder="Enter Email"/>
          </div> 
        </div> 
        </div> 
        <br/>  
        <h4 style={{marginLeft:"5%"}}>Password Change : </h4>
        <br/>
        <div style={{padding:"5%"}}>
        <div style={{border:"2px solid #DCDCDC",padding:"5%"}}>
          <div class="form-group">
            <label for="exampleInputPassword1">Current Password</label>
            <input type="password" class="form-control" id="currentpassword" placeholder="Leave Blank To Leave Unchange"/>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="exampleInputPassword1">New Password</label>
                <input type="password" class="form-control" id="newpassword" placeholder="Leave Blank To Leave Unchange"/>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="exampleInputPassword1">Confirm New Password</label>
                <input type="password" class="form-control" id="confirmpassword"/>
              </div>
            </div>
          </div>
        </div> 
        </div>
          <div style={{padding:"5%"}}>
          <button type="submit" style={{borderRadius:"0px"}} class="btn btn-dark btn-block">Save Change</button> 
          </div>
      </form>
      <br/>
    </div>
    </>
  );
}

export default AccountInformation;