﻿import React, { useState, useEffect, Fragment } from 'react';
import { faFacebook, faTwitter, faInstagram, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faPhone, faEnvelope, faLocationArrow, faUser, faSignOutAlt,faShoppingBag,faAd,faTag, faWallet,faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Fade } from 'react-slideshow-image';
import { Slide } from 'react-slideshow-image';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import AccountInformation from './AccountInformation';
import PromoUser from './PromoUser';
import PurchaseList from './PurchaseList/';
import Topup from './Topup';
import WishList from '../Wishlist/';
import './index.css';

const Myaccount = ({ match }) => {
	const history= useHistory();
  const [tab, setTab] = useState(1);
  
  function checkTab(){
    if(tab==1){//account information
      return(
        <AccountInformation/>
      );
    }
    else if(tab==2){//purchase list
      return(
        <PurchaseList/>
      );
    }
    else if(tab==3){//promo
      return(
        <PromoUser/>
      );
    }
    else if(tab == 4){
      return(
        <Topup/>
      )
    }
    else if(tab == 5){
      return(
        <>
          <h4 style={{marginLeft:"5%"}}>Wishlist : </h4>
          <WishList/>
        </>
      )
    }
    else if(tab == 6){
      window.sessionStorage.removeItem("user");
      history.push(`/`);
      return window.location.reload();
    }
  }
  function changeTab1(){setTab(1);}
  function changeTab2(){setTab(2);}
  function changeTab3(){setTab(3);}
  function changeTab4(){setTab(4);}
  function changeTab5(){setTab(5);}
  function changeTab6(){setTab(6);}

  useEffect(() => {
    setTab(match.params.index)
  }, []);
	return (
    <>
    <br/>
      <div class="myaccount-tab-dekstop">
        <div class="row">
          <div class="col-sm-2" style={{borderRight:"1px solid #DCDCDC"}}>
            <br/>
              <h2 style={{marginLeft:"20%",marginTop:"5%"}}>My Account</h2>
            <br/>
            <div className="MyAccountTab" style={{marginLeft:"20%",cursor:"pointer",fontSize:"18px",color:"gray"}}>
              <br/>
              <a onClick={changeTab1}><FontAwesomeIcon icon={ faUser } style={{width:"18px",height:"20px"}}/> Account Information </a><br/><br/>
              <a onClick={changeTab2}><FontAwesomeIcon icon={ faShoppingBag } style={{width:"18px",height:"20px"}}/> Purchase List </a><br/><br/>
              <a onClick={changeTab3}><FontAwesomeIcon icon={ faTag } style={{width:"18px",height:"20px"}}/> Promo </a><br/><br/>
              <a onClick={changeTab4}><FontAwesomeIcon icon={ faWallet } style={{width:"18px",height:"20px"}}/> Topup </a><br/><br/>
            </div>
          </div>
          <div class="col-sm-8">
            {checkTab()}
          </div>
          <div class="col-sm-2"></div>
        </div>
      </div>
      <div class="myaccount-tab-mobile"  style={{display:"none"}}>
        <div class="row">
          <div class="col-sm-2" style={{borderRight:"1px solid #DCDCDC"}}>
            <br/>
              <h2 style={{marginLeft:"5%",marginTop:"5%"}}>My Account</h2>
            <br/>
            <div className="MyAccountTab" style={{marginLeft:"15%",cursor:"pointer",fontSize:"18px",color:"gray"}}>
              <br/>
              <a onClick={changeTab1}><FontAwesomeIcon icon={ faUser } style={{width:"18px",height:"20px"}}/> Account Information </a><br/><br/>
              <a onClick={changeTab2}><FontAwesomeIcon icon={ faShoppingBag } style={{width:"18px",height:"20px"}}/> Purchase List </a><br/><br/>
              <a onClick={changeTab3}><FontAwesomeIcon icon={ faTag } style={{width:"18px",height:"20px"}}/> Promo </a><br/><br/>
              <a onClick={changeTab4}><FontAwesomeIcon icon={ faWallet } style={{width:"18px",height:"20px"}}/> Topup </a><br/><br/>
              <a onClick={changeTab5}><FontAwesomeIcon icon={ faSave } style={{width:"18px",height:"20px"}}/> Wishlist </a><br/><br/><br/>
              <a onClick={changeTab6}><FontAwesomeIcon icon={ faSignOutAlt } style={{width:"18px",height:"20px"}}/> Logout </a><br/><br/><br/>
            </div>
          </div>
          <div class="col-sm-8">
            {checkTab()}
          </div>
          <div class="col-sm-2"></div>
        </div>
      </div>
    </>
	);
}

export default Myaccount;