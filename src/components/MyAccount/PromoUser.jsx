import React, { useState, useEffect } from 'react';
import { Modal } from 'react-bootstrap';

const PromoUser = () => {
  let token=window.sessionStorage.getItem("user");

  const [datapromo, setDatapromo] = useState([]);
  const [ detail, setDetail ]= useState();
  const [ show, setShow ] = useState(false);
  
  function handleClose() { setShow(false); }
  async function getPromo(){
    const fetchAPI= await fetch(`/apiPromo/getPromoMyAccount/`, {
      method: 'GET',
      headers: { 
        'x-access-token': token 
      },
    });
    const result= await fetchAPI.json();
    
    result.promo= result.promo.map(item => ({
        id_promo: item.id_promo,
        judul: item.judul,
        tanggal_berakhir: item.tanggal_berakhir,
        jumlah_kupon: item.jumlah_kupon,
        gambar: item.gambar
    }));
    setDatapromo(result.promo);
  }
  async function handleDetailPromo(id){
    setShow(true);
    const fetchAPI= await fetch(`/apiPromo/getPromo/${id}`, {
      method: 'GET',
      headers: { 
        'x-access-token': token 
      },
    });
    const result= await fetchAPI.json();
    
    result.promo= result.promo.map(item => ({
      id_promo: item.id_promo,
      id_user: item.id_user,
      judul: item.judul,
      deskripsi: item.deskripsi,
      diskon: item.diskon,
      min_belanja: item.min_belanja,
      max_diskon: item.max_diskon,
      tanggal_mulai: item.tanggal_mulai,
      tanggal_berakhir: item.tanggal_berakhir,
      jumlah_kupon: item.jumlah_kupon,
      gambar: item.gambar
    }));
    setDetail(result.promo);
  }

  useEffect(() => {
    getPromo();
  }, []);
  return(
    <div>
        <div class="container-promo-dekstop">
            <br/><br/>
            <h4 style={{marginLeft:"5%"}}>Promo : </h4>
            <br/>
            <div style={{marginLeft:"10%",border:"2px solid #DCDCDC",padding:"3%"}}>
                { 
                datapromo.length>0?
                datapromo.map((item, index) => {
                    return (
                    <div class="card" onClick={ () => handleDetailPromo(item.id_promo) } style={{width:"15rem", border:"1px solid orange",display:"inline-block",marginRight:"2%",marginTop:"2%",cursor:"pointer"}}>
                        <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar } />
                        <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                        <p class="card-text" style={{color:"white"}}>
                            <h6>{item.judul.toUpperCase()}</h6>
                            Valid Until {item.tanggal_berakhir.substr(0,10)} <br/>
                            {item.jumlah_kupon}x Remaining
                        </p>
                        </div>
                    </div>
                    );
                })
                : <h5>No promo Avalaible For This Account !</h5>
                }
                <br/><br/>
            </div> 
            <br/>
            <br/>
        </div>
        <div class="container-promo-mobile"  style={{display:"none",marginLeft:"5%"}}>
            <h4 style={{marginLeft:"5%"}}>Promo : </h4>
            <div style={{marginLeft:"10%",padding:"3%"}}>
                { 
                datapromo.length>0?
                datapromo.map((item, index) => {
                    return (
                    <div class="card" onClick={ () => handleDetailPromo(item.id_promo) } style={{width:"15rem", border:"1px solid orange",display:"inline-block",marginRight:"2%",marginTop:"10%",cursor:"pointer"}}>
                        <img className="card-img-top" src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar } />
                        <div class="card-body" style={{backgroundColor:"#292c2f"}}>
                        <p class="card-text" style={{color:"white"}}>
                            <h6>{item.judul.toUpperCase()}</h6>
                            Valid Until {item.tanggal_berakhir.substr(0,10)} <br/>
                            {item.jumlah_kupon}x Remaining
                        </p>
                        </div>
                    </div>
                    );
                })
                : <h5>No promo Avalaible For This Account !</h5>
                }
                <br/><br/>
            </div> 
            <br/>
            <br/>
        </div>
        <Modal size="md" style={{ fontSize: '1.25vw', opacity:1 }}
                show={ show } 
                onHide={ handleClose }>
            { detail ? 
                <> 
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Detail Promo  
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="mt-4">
                        {
                                detail.map((item, index) => {
                                    return (
                                        <div className="card mb-4"
                                                key={ index }>
                                            <div className="card-title"
                                                    style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                                <h5 className="text-center text-white p-3">
                                                    {item.judul.toUpperCase()}
                                                </h5>
                                            </div>
                                            <div className="card-body"
                                                    style={{ padding: '0% !important' }}>
                                                <div className="row">
                                                    <img className="col-12" src={ process.env.PUBLIC_URL+'/images/promo/'+item.gambar} />
                                                </div>
                                            </div>
                                            <div>
                                                <hr/>
                                                <div style={{marginLeft:"3%"}}>
                                                <Modal.Title>
                                                    Description :  
                                                </Modal.Title>
                                                <p style={{fontSize:"15px"}}>{item.deskripsi}</p>
                                                </div>
                                                <hr/>
                                                <div style={{marginLeft:"3%"}}>
                                                <Modal.Title>
                                                    Terms And Conditions :  
                                                </Modal.Title>
                                                <div style={{fontSize:"15px"}}>
                                                    Discount : {item.diskon}% <br/>
                                                    Max Discount : {"Rp."+item.max_diskon.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')+",-"} <br/>
                                                    Minimum Shipping : {"Rp."+item.min_belanja.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')+",-"} <br/> <br/>
                                                    Validity : {item.tanggal_mulai.substr(0,10)} To {item.tanggal_berakhir.substr(0,10)} <br/>
                                                </div>
                                                </div>
                                                <hr/>
                                                <p style={{textAlign:"right",marginRight:"2%",fontSize:"15px",color:"red"}}>{item.jumlah_kupon}x Promo Remaining</p>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Modal.Body>
                </> 
            : '' } 
        </Modal>
    </div>
  );
}

export default PromoUser;