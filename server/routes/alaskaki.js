const express = require("express");
const jwt= require('jwt-simple');
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);

const router = express.Router();
const { promisify } = require('util');

router.get("/getalaskaki", async (req, res) => {
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker
        where status_sneaker = 1
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});
router.get("/getnewarrival", async (req, res) => {
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker 
        where status_sneaker = 1
        order by id_sneaker desc
        LIMIT 8
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});
router.get("/getbestseller", async (req, res) => {
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT a.id_sneaker,b.id_kategori,b.nama_sneaker,b.jenis_sneaker,b.brand_sneaker,b.harga_sneaker,b.status_sneaker,b.gambar, sum(a.jumlah_sneaker)"jumlah" 
        FROM dtrans a,sneaker b 
        WHERE a.id_sneaker=b.id_sneaker and b.status_sneaker = 1
        GROUP BY id_sneaker 
        ORDER BY jumlah desc
        LIMIT 8
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});

router.get("/getalaskaki/:kategori", async (req, res) => {
    const kategori= req.params.kategori;
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker
        WHERE id_kategori = ${kategori} and status_sneaker = 1
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});

router.get("/getalaskaki/:kategori/:jenis", async (req, res) => {
    const kategori= req.params.kategori;
    const jenis= req.params.jenis;
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker
        WHERE id_kategori = ${kategori} and jenis_sneaker='${jenis}' and status_sneaker = 1
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});

router.get("/getdetailalaskaki/:id_sneaker", async (req, res) => {
    const id_sneaker= req.params.id_sneaker;
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker
        WHERE id_sneaker = ${id_sneaker}
    `);
    let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${id_sneaker}
    `);
    return res.status(200).json({
        status: 200,
        detail: query1[0],
        ukuran: query2
    });
});

router.get("/searchalaskaki/:kategori/:nama", async (req, res) => {
    const kategori= req.params.kategori;
    const nama= req.params.nama;
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker
        WHERE id_kategori = ${kategori} and nama_sneaker LIKE '%${nama}%' and status_sneaker = 1
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});

router.get("/carialaskaki/:nama", async (req, res) => {
    const nama= req.params.nama;
    const ukuran=[];
    const rating=[];
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM sneaker
        WHERE nama_sneaker LIKE '%${nama}%' and status_sneaker = 1
    `);
    for (let i = 0; i < query1.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query1[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query1.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query1[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }
    
    return res.status(200).json({
        status: 200,
        alaskaki: query1,
        ukuran: ukuran,
        rating: rating
    });
});

router.get("/getreviewalaskaki/:id_sneaker", async (req, res) => {
    const id_sneaker= req.params.id_sneaker;
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        SELECT *
        FROM review
        WHERE id_sneaker=${id_sneaker}
    `);
    return res.status(200).json({
        status: 200,
        review: query1
    });
});

router.get("/getreviewalaskaki/", async (req, res) => {
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query1= await promisfydbconnection(`
        select a.id_review,a.id_user,b.nama,a.id_sneaker,c.nama_sneaker,a.review,a.rating,a.tgl,a.gambar
        FROM review a,user b,sneaker c
        WHERE a.id_user=b.id_user AND a.id_sneaker = c.id_sneaker
    `);
    return res.status(200).json({
        status: 200,
        review: query1
    });
});
module.exports = router;