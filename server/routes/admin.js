const express = require("express");
const path = require("path");
const multer = require("multer");
const connection = require("../connection");

const app = express.Router();
const fs = require('fs');
app.use("/public/images/sneakers", express.static("./public/images/sneakers"));
app.use("/public/images/promo", express.static("./public/images/promo"));
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/sneakers");
    },
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() +
        path.extname(file.originalname));
    }
});

const storage2 = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/promo");
    },
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() +
        path.extname(file.originalname));
    }
});

const upload2 = multer({
    storage : storage2  
}).single('gambar2');

const upload = multer({
    storage : storage
}).single('gambar');

app.get("/getKatagori", async (req, res) => {
    var q = "select * from kategori";
    const data = await connection.query(q);
    
    res.json(data);
});

app.post("/addCategory", async (req, res) => {
    var nama_kategori = req.body.nama;
    

    var q = `INSERT INTO kategori (id_kategori, nama_kategori) VALUES (NULL, '${nama_kategori}')`
    console.log(q);
    try {
        const hasil = await connection.query(q)
        //console.log(hasil)
        if(hasil.affectedRows > 0 ){
            res.status(200).send({status:"success",message:"Tambah katagori berhasil !"})
        } else {
            res.status(400).send({status:"fail",message:"Tambah katagori gagal"})
        }
        
    } catch (error) {
        res.status(500).send(error)
    }
});

app.delete("/deleteCategory", async (req, res) => {
    var id_category = req.query.id;

    var qq = `select * from barang where id_katagori = ${id_category}`;
    console.log(qq);
    const cek = await connection.query(qq);
    if (cek.length > 0){
        res.status(200).send({status:"fail",message:"Gagal hapus katagori masih ada barang dengan katagori tersebut"});
    } else {
        var q = `delete from katagori where id_katagori = ${id_category}`;
        console.log(q);
        try {
            const hasil = await connection.query(q)
            //console.log(hasil)
            if(hasil.affectedRows > 0 ){
                res.status(200).send({status:"success",message:"Hapus katagori berhasil !"})
            } else {
                res.status(200).send({status:"fail",message:"Hapus katagori gagal"})
            }
            
        } catch (error) {
            res.status(500).send(error)
        }
    }
});


app.post("/addProduct", async (req, res) => {
    
    upload(req, res, async (err) => {
        var id_kategori = req.body.id_kategori;
        var nama_sneaker = req.body.nama_sneaker;
        var brand_sneaker = req.body.brand_sneaker;
        var harga_sneaker = req.body.harga_sneaker;
        var jenis_sneaker = req.body.jenis_sneaker;
        
        if (err) {
            console.log(err);
        } else {
            if (req.file == undefined){
                console.log("gambar belum dipilih");
                res.send("gambar belum dipilih");
                
            } else {
                
                var gambar = req.file.filename;
                var q = `INSERT INTO sneaker (id_sneaker, id_kategori, nama_sneaker, jenis_sneaker, brand_sneaker, harga_sneaker, status_sneaker, gambar) VALUES (NULL, ${id_kategori}, '${nama_sneaker}', '${jenis_sneaker}', '${brand_sneaker}', ${harga_sneaker}, 1, '${gambar}')`;
                console.log(q);

                try {
                    const hasil = await connection.query(q)
                    
                    if(hasil.affectedRows > 0 ){
                        res.status(200).send({status:"success",message:"Insert barang berhasil !", data:hasil})
                    } else {
                        res.status(400).send({status:"fail",message:"Insert barang gagal"})
                    }
                    
                } catch (error) {
                    res.status(500).send(error)
                }
            }
        }
    });
});

app.get("/getProduk", async (req, res) => {
    var q = "select * from sneaker where status_sneaker = 1";
    const data = await connection.query(q);
    
    res.json(data);
});

app.get("/getPromo", async (req, res) => {
    var q = "select * from promo";
    const data = await connection.query(q);
    
    res.json(data);
});

app.post("/addPromo", async (req, res) => {
    
    upload2(req, res, async (err) => {
        var judul = req.body.judul;
        var deskripsi = req.body.deskripsi;
        var diskon = req.body.diskon;
        var min_belanja = req.body.min_belanja;
        var max_diskon = req.body.max_diskon;
        var tanggal_mulai = req.body.tanggal_mulai;
        var tanggal_berahkir = req.body.tanggal_berahkir;
        var jumlah_kupon = req.body.jumlah_kupon;

        var gambar = req.file.filename;
        if (err) {
            console.log(err);
        } else {
            if (req.file == undefined){
                console.log("gambar belum dipilih");
                res.send("gambar belum dipilih");
                
            } else {
                var rows = 0;
                var q = "select * from user";
                const data = await connection.query(q);
                data.forEach(async element => {
                    var q2 = `INSERT INTO promo (id_promo, id_user, judul, deskripsi, diskon, min_belanja, max_diskon, tanggal_mulai, tanggal_berakhir, jumlah_kupon, gambar) VALUES (NULL, ${element.id_user}, '${judul}', '${deskripsi}', ${diskon}, ${min_belanja}, ${max_diskon}, STR_TO_DATE('${tanggal_mulai}', '%m/%d/%Y'), STR_TO_DATE('${tanggal_berahkir}', '%m/%d/%Y'), ${jumlah_kupon}, '${gambar}')`;
                    try {
                        console.log(q2);
                        const hasil = await connection.query(q2)
                        
                        
                    } catch (error) {
                        res.status(500).send(error)
                    }
                });
                
                
                res.status(200).send({status:"success",message:"berhasil"})

            }
        }
    });
});

app.get("/getSneakerBYID", async (req, res) => {
    var id = req.query.id_barang;

    var q = `select * from sneaker where id_sneaker = ${id}`;
    const data = await connection.query(q);
    
    res.json(data);
});

app.get("/getUkuran", async (req, res) => {
    var id = req.query.id_barang;

    var q = `select * from ukuran where id_sneaker = ${id}`;
    const data = await connection.query(q);
    
    res.json(data);
});

app.post("/editStok", async (req, res) => {
    var id_ukuran = req.body.id_ukuran;
    var stokBaru = req.body.stok_baru;

    var q = `update ukuran set stok_sneaker = ${stokBaru} where id_ukuran = ${id_ukuran}`;  
    const hasil = await connection.query(q);
    if(hasil.affectedRows > 0 ){
        res.status(200).send({status:"success",message:"Upadate Stok Berhasil !", data:hasil})
    } else {
        res.status(400).send({status:"fail",message:"Update Stok Gagal"})
    }
});

app.delete("/deleteStok/:id_ukuran", async (req, res) => {
    var id_ukuran = req.params.id_ukuran;

    var q = "delete from ukuran where id_ukuran = " + id_ukuran;
    console.log(q);

    const hasil = await connection.query(q);
    if(hasil.affectedRows > 0 ){
        res.status(200).send({status:"success",message:"Delete Ukuran Berhasil !", data:hasil})
    } else {
        res.status(400).send({status:"fail",message:"Delete Ukuran Gagal"})
    }
});


app.post("/insertukuran", async (req, res) => {
    var id_sneaker = req.body.id_sneaker;
    var ukuran_sneaker = req.body.ukuran_sneaker;
    var stok_sneaker = req.body.stok_sneaker;

    var q = `select * from ukuran where id_sneaker = ${id_sneaker} and ukuran_sneaker = ${ukuran_sneaker}`;

    var hasil = await connection.query(q);

    if (hasil.length > 0){
        res.status(400).send({status:"fail",message:"Ukuran Sudah Terdaftar"});
    } else {
        var q2 = `INSERT INTO ukuran (id_ukuran, id_sneaker, ukuran_sneaker, stok_sneaker) VALUES (NULL, ${id_sneaker}, ${ukuran_sneaker}, ${stok_sneaker})`;
        var hasil2 = await connection.query(q2);
        if(hasil2.affectedRows > 0 ){
            res.status(200).send({status:"success",message:"Insert Ukuran Berhasil !", data:hasil2})
        } else {
            res.status(400).send({status:"fail",message:"Insert Ukuran Gagal"})
        }
    }
});

app.get("/getDaftarPromo", async (req, res) => {
    var id = req.query.id_barang;

    var q = `select distinct judul, deskripsi, diskon, min_belanja, max_diskon, tanggal_mulai, tanggal_berakhir, gambar from promo`;
    const data = await connection.query(q);
    
    res.json(data);
});

app.get("/getDetailPromo", async (req, res) => {
    var judul = req.query.judul;

    var q = `select * from promo p, user u where judul='${judul}'and p.id_user = u.id_user`;
    //console.log(q);
    const data = await connection.query(q);
    
    res.json(data);
});

app.post("/deleteSneaker", async (req, res) => {
    var id_sneaker = req.body.id_sneaker;

    var q = `update sneaker set status_sneaker = 0 where id_sneaker = ${id_sneaker}`;
    console.log(q);
    var hasil = await connection.query(q);
    if(hasil.affectedRows > 0 ){
        var q2 = `delete from ukuran where id_sneaker = ${id_sneaker}`;
        console.log(q2);
        var hasil2 = await connection.query(q2);
        res.status(200).send({status:"success",message:"Delete Sneaker Berhasil"})
    } else {
        res.status(400).send({status:"fail",message:"Delete Sneaker Gagal"})
    }
});

app.post("/updateSneaker", (req, res) => {

    upload(req, res, async (err) => {
        var gambarLama = req.body.gambar_lama;

        var id_sneaker = req.body.id_sneaker;
        var id_kategori = req.body.id_kategori;
        var nama_sneaker = req.body.nama_sneaker;
        var brand_sneaker = req.body.brand_sneaker;
        var harga_sneaker = req.body.harga_sneaker;
        var jenis_sneaker = req.body.jenis_sneaker;

        if (req.file == undefined){
            //gambar tidak diganti
            console.log("gambar belum dipilih");
            //res.send("gambar belum dipilih");
            var q = `update sneaker set id_kategori=${id_kategori}, nama_sneaker='${nama_sneaker}', jenis_sneaker='${jenis_sneaker}', brand_sneaker='${brand_sneaker}', harga_sneaker=${harga_sneaker} where id_sneaker=${id_sneaker}`;
            console.log(q);
            try {
                const hasil = await connection.query(q)
                
                if(hasil.affectedRows > 0 ){
                    res.status(200).send({status:"success",message:"Update barang berhasil dilakukan"})
                } else {
                    res.status(400).send({status:"fail",message:"Insert spesifikasi gagal"})
                }
                
            } catch (error) {
                res.status(500).send(error)
            }
        } else {
            //gambar diganti
            console.log('./public/images/sneakers/'+gambarLama);
            try {
                fs.unlinkSync('./public/images/sneakers/'+gambarLama);
                console.log("sukses hapus gambar lama");
            } catch (err) {
                console.log(err);
            }
            var gambar = req.file.filename;
            var q = `update sneaker set id_kategori=${id_kategori}, nama_sneaker='${nama_sneaker}', brand_sneaker='${brand_sneaker}', harga_sneaker=${harga_sneaker}, gambar='${gambar}' where id_sneaker=${id_sneaker}`;
            console.log(q);
            try {
                const hasil = await connection.query(q)
                
                if(hasil.affectedRows > 0 ){
                    res.status(200).send({status:"success",message:"Update barang berhasil dilakukan"})
                } else {
                    res.status(400).send({status:"fail",message:"Insert spesifikasi gagal"})
                }
            } catch (error) {
                res.status(500).send(error)
            }
        }
    });
});
module.exports = app;
