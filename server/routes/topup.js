const midtransClient = require('midtrans-client');
const express = require("express");
const jwt= require('jwt-simple');
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);
const router = express.Router();
const { promisify } = require('util');

router.post("/getTokenMidtrans", async (req, res) => {
    // Create Snap API instance
    let snap = new midtransClient.Snap({
        isProduction : false,
        serverKey : 'SB-Mid-server-NUylG_oG5s-6_vYLf-JXNPUE',
        clientKey : 'SB-Mid-client-XndPVKPVENnpp6W3'
    });  

    console.log("order-id-node-"+Math.round((new Date()).getTime() / 1000) + "\ntopup : " + req.body.jumlahTopup);
    
    let parameter = {
        "transaction_details": {
            "order_id": "order-id-node-"+Math.round((new Date()).getTime() / 1000),
            "gross_amount": req.body.jumlahTopup
        }, "credit_card":{
            "secure" : true
        }
    };
    
    snap.createTransaction(parameter)
    .then((transaction)=>{
        // transaction token
        let transactionToken = transaction.token;
        console.log('transactionToken:',transactionToken);
        return res.json(transactionToken);
    })
});

router.post("/tambahSaldo", async (req, res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `SELECT * FROM user WHERE id_user='${payload.id_user}'`
    );
    const saldoTemp = parseInt(result[0].saldo) + parseInt(req.body.jumlahTopup);
    result1 = await promisfydbconnection( 
        `UPDATE user 
        SET saldo=${saldoTemp}
        WHERE id_user='${payload.id_user}'`
    );
    return res.status(200).json({
        status: 200,
        message: 'Topup Success!!!'
    });
});

module.exports = router;