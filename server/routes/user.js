const express = require("express");
const jwt= require('jwt-simple');
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);
const axios = require('axios');
const request = require("request");


const router = express.Router();
const { promisify } = require('util');
const { encrypt, decrypt } = require('./hashing');

router.post("/register", async (req, res) => {
    let username=req.body.username;
    let name=req.body.name;
    let email=req.body.email;
    let password=req.body.password;
    let saldo=0;
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `select * from user where username='${username}'`
    );
    
    if(result.length){
        return res.status(400).json({
            status: 400,
            message: 'Username Already Used.'
        });
    }
    else{
        password=encrypt(password);
        result2 = await promisfydbconnection( 
            `insert into user values(null,'${username}','${name}','${email}','${password.iv+"|"+password.content}','${saldo}')`
        );
        return res.status(200).json({
            status: 200,
            message: 'Register success.'
        });
    }
});

router.post("/login", async (req, res) => {
    let username=req.body.username;
    let password=req.body.password;
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `select * from user where username='${username}'`
    );
    if(!result.length){
        return res.status(400).json({
            status: 400,
            message: "Can't Found Username!"
        });
    }
    else{
        let dbpasswordsplit=result[0].password.split("|");
        let dbpassword={
            iv:dbpasswordsplit[0],
            content:dbpasswordsplit[1]
        }
        if(password==decrypt(dbpassword)){
            const token= jwt.encode({ id_user: result[0].id_user }, 'user');
            return res.status(200).json({
                status: 200,
                message: 'Login success.',
                userLogged: {
                    token:token
                }
            });
        }
        else{
            return res.status(400).json({
                status: 400,
                message: "Wrong Password!"
            });
        }
    }
});

router.get("/getuserlogin", async (req, res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT *
        FROM user
        WHERE id_user = ${payload.id_user}
    `);

    return res.status(200).json({
        status: 200,
        info: query[0]
    });
});


router.post("/editprofile", async (req, res) => {
    let name=req.body.name;
    let email=req.body.email;
    let currentpasswordhash=req.body.currentpasswordhash;
    let currentpassword=req.body.currentpassword;
    let newpassword=req.body.newpassword;

    let hashpasswordsplit = currentpasswordhash.split("|");
    let hashpassword = {
        iv:hashpasswordsplit[0],
        content:hashpasswordsplit[1]
    }
    if(currentpassword==decrypt(hashpassword)){
        const token= req.header('x-access-token');
        const payload= jwt.decode(token, 'user');
        newpassword=encrypt(newpassword);
        newpassword=newpassword.iv+"|"+newpassword.content;
        let promisfydbconnection = promisify(pool.query).bind(pool);
        result = await promisfydbconnection( 
            `UPDATE user SET nama='${name}',email='${email}',password='${newpassword}' WHERE id_user = ${payload.id_user} `
        );
        return res.status(200).json({
            status: 200,
            message: 'Edit Profile success.'
        });
    }
    else{
        return res.status(400).json({
            status: 400,
            message: "Wrong Current Password!"
        });
    }
});

router.post("/editprofilewithoutpassword", async (req, res) => {
    let name=req.body.name;
    let email=req.body.email;
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');

    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `UPDATE user SET nama='${name}',email='${email}' WHERE id_user = ${payload.id_user} `
    );
    return res.status(200).json({
        status: 200,
        message: 'Edit Profile success.'
    });
});

//cart
router.post("/addtocart", async (req, res) => {
    let id_sneaker=req.body.id_sneaker;
    let jumlah_sneaker=req.body.jumlah_sneaker;
    let ukuran_sneaker=req.body.ukuran_sneaker;
    let harga_satuan=req.body.harga_sneaker;
    let subtotal=req.body.subtotal;
    let max=req.body.max;
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `select * from cart where id_user=${payload.id_user} and id_sneaker=${id_sneaker} and ukuran_sneaker=${ukuran_sneaker}`
    );
    if (result.length>0) {
        let combine=parseInt(result[0].jumlah_sneaker)+parseInt(jumlah_sneaker);
        console.log(combine);
        if (combine>max) {
            return res.status(400).json({
                status: 400,
                message: 'Exceed The Stock.'
            });
        }
        else{
            result2 = await promisfydbconnection( 
                `UPDATE cart SET jumlah_sneaker=${combine},subtotal=${parseInt(combine)*result[0].harga_satuan} WHERE id_cart = ${result[0].id_cart} `
            );
            return res.status(200).json({
                status: 200,
                message: 'Update Cart Success.'
            });
        }
    }
    else{
        result2 = await promisfydbconnection( 
            `insert into cart values(null,${payload.id_user},${id_sneaker},${jumlah_sneaker},${ukuran_sneaker},${harga_satuan},${subtotal})`
        );
        return res.status(200).json({
            status: 200,
            message: 'Add To Cart Success.'
        });
    }
});

//wishlist
router.post("/addtowishlist", async (req, res) => {
    let id_sneaker=req.body.id_sneaker;
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `select * from wishlist where id_user=${payload.id_user} and id_sneaker=${id_sneaker}`
    );
    if(result.length>0){
        return res.status(400).json({
            status: 400,
            message: 'Item Already In Wishlist.'
        });
    }
    else{
        result2 = await promisfydbconnection( 
            `insert into wishlist values(null,${payload.id_user},${id_sneaker})`
        );
        return res.status(200).json({
            status: 200,
            message: 'Add To Wishlist Success.'
        });
    }
});

router.get("/getCart", async(req,res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    
    //GET SNEAKER
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT s.nama_sneaker as nama_sneaker,s.harga_sneaker as harga_sneaker,s.gambar as gambar_sneaker
        , w.id_sneaker "id_sneaker", w.id_cart "id_cart", w.ukuran_sneaker as ukuran_sneaker,w.jumlah_sneaker as jumlah_sneaker
        , u.stok_sneaker "stok_sneaker"
        FROM cart w,sneaker s, ukuran u
        WHERE w.id_user = ${payload.id_user} and w.id_sneaker=s.id_sneaker and w.id_sneaker=u.id_sneaker and w.ukuran_sneaker=u.ukuran_sneaker
    `);
    for(let i=0; i<query.length; i++){
        query[i].counter = i;
    }

    //GET PROMO
    let query2= await promisfydbconnection(`
        SELECT *
        FROM promo
        Where id_user = ${payload.id_user} and tanggal_berakhir >= SYSDATE() and jumlah_kupon>0
        order by tanggal_mulai desc
    `);
    return res.send([
        {
            listCart: query,
            promo: query2
        }
    ]);
})

router.post("/updateQtyCart", async(req,res) => {
    let id_cart = req.body.id_cart;
    let qty= req.body.qty;
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        Update cart set jumlah_sneaker=${qty} WHERE id_cart = ${id_cart}
    `);
    res.send(query);
})

router.delete("/deleteCart", async(req,res) => {
    let id_cart = req.body.id_cart
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        Delete from cart WHERE id_cart = ${id_cart}
    `);
    res.send(query);
})

router.delete("/emptyCart", async(req,res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        Delete from cart WHERE id_user = ${payload.id_user}
    `);
    res.send(query);
})

router.get("/getAllWishlist", async(req,res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    let ukuran=[];
    const rating=[];
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT s.id_sneaker as id_sneaker, s.id_kategori as id_kategori, s.nama_sneaker as nama_sneaker, s.jenis_sneaker as jenis_sneaker
        ,s.brand_sneaker as brand_sneaker, s.harga_sneaker as harga_sneaker, s.status_sneaker as status_sneaker, s.gambar as gambar
        ,w.id_wishlist
        FROM wishlist w,sneaker s
        WHERE w.id_user = ${payload.id_user} and w.id_sneaker=s.id_sneaker and s.status_sneaker = 1
    `);
    for (let i = 0; i < query.length; i++) {
        let query2= await promisfydbconnection(`
        SELECT *
        FROM ukuran 
        WHERE id_sneaker = ${query[i].id_sneaker}
        `);
        if(query2.length>0){ukuran.push(query2);}
        else{ukuran.push(null)}
    }
    for (let i = 0; i < query.length; i++) {
        let query3= await promisfydbconnection(`
        SELECT id_sneaker,COUNT(id_sneaker)"jumlah_review",SUBSTR((SUM(rating)/COUNT(id_sneaker)),1,3)"rating"
        FROM review
        WHERE id_sneaker = ${query[i].id_sneaker}
        GROUP BY id_sneaker
        `);
        if(query3.length>0){rating.push(query3[0]);}
        else{rating.push(null)}
    }

    return res.status(200).json({
        status: 200,
        alaskaki: query,
        ukuran: ukuran,
        rating: rating
    });
})

router.post("/deletePerItemWishlist", async(req,res) => {
    const id_wishlist = req.body.id_wishlist;
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        Delete from wishlist WHERE id_wishlist = ${id_wishlist}
    `);
    res.send(query);
})

router.delete("/emptyWishlist", async(req,res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        Delete from wishlist WHERE id_user = ${payload.id_user}
    `);
    res.send(query);
})

router.post("/addShipment", async (req, res) => {
    const nama = req.body.nama;
    const alamat = req.body.alamat;
    const kodepost = req.body.kodepos;
    const nohp = req.body.nohp;
    const status = req.body.status;
    const ongkir = req.body.ongkir;
    //console.log(ongkir);
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        insert into shipment values(null,'${nama}','${alamat}',${kodepost},'${nohp}','${status}',${ongkir})
    `);
    res.send(query);
});

router.get("/getProv", async (req,res) => {
    var options = {
        method: 'GET',
        url: 'https://api.rajaongkir.com/starter/province',
        headers: {key: 'edf4e0781a37ca71137fefac8d0b2678'}
      };
      
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
      });
});

router.get("/getCity/:idprov", async (req,res) => {
    var request = require("request");
    var idprov = req.params.idprov;
    var options = {
        method: 'GET',
        url: 'https://api.rajaongkir.com/starter/city',
        qs: {province: idprov},
        headers: {key: 'edf4e0781a37ca71137fefac8d0b2678'}
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
    });
});

router.post("/getOngkir/:idtujuan", async (req,res) => {
    var request = require("request");
    var idtujuan = req.params.idtujuan;
    var options = {
        method: 'POST',
        url: 'https://api.rajaongkir.com/starter/cost',
        headers: {key: 'edf4e0781a37ca71137fefac8d0b2678', 'content-type': 'application/x-www-form-urlencoded'},
        form: {origin: '444', destination: idtujuan, weight: 1000, courier: 'jne'}
      };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
    });
});


router.post("/addHtrans", async (req, res) => {
    let token= req.header('x-access-token');
    let payload= [];
    let iduser=0;
    let idpromo = req.body.idpromo;
    let jumlah = req.body.qty;
    let totalpromo = req.body.promototal;
    let total = req.body.total;
    // console.log(token);
    if(token.toString() != 'null'){
        // console.log("ada usernya");
        payload = jwt.decode(token, 'user');
        iduser = payload.id_user;
    }

    let desk = [];
    let diskon = [];
    let id_shipment = [];
    //console.log("woi " + idpromo + " - " + jumlah + " - " + totalpromo + " - " + total);
    let promisfydbconnection = promisify(pool.query).bind(pool);
    if(iduser != 0){
        console.log(idpromo);
        if(idpromo.toString() != 'null'){
            result1 = await promisfydbconnection( 
                //	id_trans	id_user	id_shipment	tgl_beli	jumlah	total	deskripsi_promo	total_promo	diskon_promo	
                `SELECT * FROM promo WHERE id_promo=${idpromo}`
            );
        
            desk = result1[0].deskripsi;
            diskon = result1[0].diskon;
    
            //PENGURANGAN PROMO
            if(result1[0].jumlah_kupon > 0){
                let tempJumlahKupon = result1[0].jumlah_kupon - 1;
                await promisfydbconnection( 
                    `UPDATE promo set jumlah_kupon=${tempJumlahKupon} where id_promo=${idpromo}`
                );
            }
        }
        else {
            desk = '-';
            totalpromo = 0;
            diskon = 0;
        }
        
        //PENGURANGAN SALDO
        resultUser = await promisfydbconnection( 
            `SELECT * FROM user WHERE id_user=${iduser}`
        );
        let tempSaldoSekarang = parseInt(resultUser[0].saldo) - parseInt(total);
        await promisfydbconnection( 
            `UPDATE user set saldo=${tempSaldoSekarang} where id_user=${iduser}`
        );
    }
    else {
        desk = '-';
        totalpromo = 0;
        diskon = 0;
    }
    
    idship = await promisfydbconnection( 
        //	id_trans	id_user	id_shipment	tgl_beli	jumlah	total	deskripsi_promo	total_promo	diskon_promo	
        `SELECT max(id_shipment) as id FROM shipment`
    );
    id_shipment = idship[0].id;
    //console.log("woi " + iduser + " - " + id_shipment + " - " + jumlah + " - " + total + " - " + desk + " - " + totalpromo + " - " + diskon);
    await promisfydbconnection( 
        //	id_trans	id_user	id_shipment	tgl_beli	jumlah	total	deskripsi_promo	total_promo	diskon_promo	
        `insert into htrans values(null,${iduser},${id_shipment},now(),${jumlah},${total},'${desk}',${totalpromo},${diskon})`
    );
    return res.status(200).json({
        status: 200,
        message: 'Transaction Success.'
    });
});

router.post("/addDtrans", async (req, res) => {
    let item = req.body.item;
    //console.log("woi");
    //console.log(item);

    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        //	id_trans	id_user	id_shipment	tgl_beli	jumlah	total	deskripsi_promo	total_promo	diskon_promo	
        `SELECT max(id_trans) "id_trans" from htrans`
    );
    //console.log("woi1");

    let query = "INSERT INTO dtrans VALUES ";
    for(let i=0; i<item.length; i++){
        let tempSubtotal = item[i].jumlah_sneaker * item[i].harga_sneaker;
        query += `(null, ${result[0].id_trans}, ${item[i].id_sneaker}, ${item[i].jumlah_sneaker}, ${item[i].ukuran_sneaker}, ${item[i].harga_sneaker}, ${tempSubtotal}),\n`
    }
    query = query.substr(0,query.length-2);
    //console.log(query);
    await promisfydbconnection(query);
    //console.log("selesai");

    return res.status(200).json({
        status: 200,
        message: 'Insert Dtrans Success.'
    });
});

router.post("/updateStock", async (req, res) => {
    let query = req.body.query;

    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection(query);

    return res.status(200).json({
        status: 200,
        message: 'Update Stock Success.'
    });
});

router.get("/getInvoice", async (req, res) => {
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `SELECT h.id_trans "id_trans", DATE_FORMAT(CAST(h.tgl_beli AS CHAR), '%d %M %Y %H:%i') "tanggal_beli", h.total "total_harga", h.jumlah "jumlah_beli",
        h.deskripsi_promo "deskripsi_promo", h.total_promo "total_promo", h.diskon_promo "diskon_promo",
        s.nama_penerima "nama_penerima", s.alamat "alamat_pengiriman", s.kode_pos "kode_pos", s.no_telp "no_telp", s.ongkir "harga_shipping"
        FROM htrans h, shipment s
        where h.id_shipment = s.id_shipment and h.id_trans = (select max(id_trans) from htrans)`
    );
    result2 = await promisfydbconnection( 
        `SELECT d.id_trans "id_trans", d.ukuran_sneaker "ukuran_item", d.jumlah_sneaker "jumlah_item", d.harga_satuan "harga_item", d.subtotal "subtotal",
        s.nama_sneaker "nama_item", s.brand_sneaker "brand_item"
        FROM dtrans d, sneaker s 
        WHERE d.id_sneaker = s.id_sneaker and d.id_trans = ${result[0].id_trans}
        `
    );
    for(let i=0; i<result2.length; i++){
        result2[i].counter = i+1;
    }
    return res.status(200).json({
        header : result,
        detail : result2
    });
});

module.exports = router;