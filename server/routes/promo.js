const express = require("express");
const jwt= require('jwt-simple');
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);

const router = express.Router();
const { promisify } = require('util');

router.get("/getPromoMyAccount", async (req, res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT *
        FROM promo
        WHERE id_user = ${payload.id_user} and tanggal_berakhir >= SYSDATE() and jumlah_kupon>0
        order by tanggal_mulai desc
    `);

    return res.status(200).json({
        status: 200,
        promo: query
    });
});
router.get("/getPromo/:id_promo", async (req, res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');
    const id_promo= req.params.id_promo;
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT *
        FROM promo
        WHERE id_user = ${payload.id_user} and id_promo = ${id_promo}
    `);

    return res.status(200).json({
        status: 200,
        promo: query
    });
});
//home
router.get("/getPromoHome", async (req, res) => {
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT *
        FROM promo
        WHERE id_user = 1 and tanggal_berakhir >= SYSDATE()
        order by tanggal_mulai desc
        LIMIT 8
    `);

    return res.status(200).json({
        status: 200,
        promo: query
    });
});
router.get("/getPromoHome/:id_promo", async (req, res) => {
    const id_promo= req.params.id_promo;
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let query= await promisfydbconnection(`
        SELECT *
        FROM promo
        WHERE id_promo = ${id_promo}
    `);

    return res.status(200).json({
        status: 200,
        promo: query
    });
});
module.exports = router;