const multer= require('multer');
const path= require('path');

const storage=multer.diskStorage({
    destination:function(req,file,callback){
        callback(null,"./public/images/review");
    },
    filename:function(req,file,callback) {
        const filename=file.originalname.split(".");
        const extension=filename[1];
        callback(null,Date.now()+"."+extension);
    }
});
const upload=multer({
    storage:storage
});

module.exports= upload;