const express = require("express");
const jwt= require('jwt-simple');
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);
const path = require("path");
const multer = require("multer");
const upload = require('./upload');
const fs = require('fs');

const router = express.Router();
const { promisify } = require('util');

// const storage = multer.diskStorage({
//     destination: function (req, file, callback) {
//         callback(null, "./public/images/review");
//     },
//     filename: function(req, file, cb){
//         cb(null, file.fieldname + '-' + Date.now() +
//         path.extname(file.originalname));
//     }
// });

// const upload = multer({
//     storage : storage
// }).single('gambar');

router.get("/getListPurchaseRating", async (req, res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');

    let promisfydbconnection = promisify(pool.query).bind(pool);
    result1 = await promisfydbconnection(//FOR TABLE RATING PURCHASE LIST
        `SELECT h.id_trans "id_trans", h.id_user "id_user", h.tgl_beli "tgl_beli", h.jumlah "jumlah", h.total "total",
        s.id_shipment "id_shipment", s.status_pengiriman "status_pengiriman"
        FROM htrans h, shipment s
        WHERE h.id_user=${payload.id_user} and h.id_shipment = s.id_shipment`
    );

    result2 = await promisfydbconnection(//FOR POPUP RATING
        `SELECT DISTINCT h.id_trans "id_trans",
        d.id_sneaker "id_sneaker", d.harga_satuan "harga_satuan",
        s.nama_sneaker "nama_sneaker", s.brand_sneaker "brand_sneaker", s.gambar "gambar_sneaker",
        sh.id_shipment "id_shipment"
        FROM htrans h, dtrans d, sneaker s, shipment sh
        WHERE h.id_user=${payload.id_user} and h.id_trans = d.id_trans and d.id_sneaker = s.id_sneaker and h.id_shipment = sh.id_shipment`
    );

    return res.status(200).json({
        status: 200,
        dataList: result1,
        listBarang: result2
    });
})

router.post("/addReview", upload.any(), async (req, res) => {
    console.log(req.files);
    let token= req.header('x-access-token');
    let payload= [];
    if(token.toString() != 'null') {
        payload = jwt.decode(token, 'user');
    }
    else {
        payload.id_user = 0;
    }
    
    let q = req.body.q;
    let jumlah_data = req.body.jumlah_data;
    
    for(let i=0; i<jumlah_data; i++){
        q = q.replace(`'id_user'`, payload.id_user);
        q = q.replace(`gambar_filename`, req.files[i].filename);
    }
    //console.log(q);

    try {
        let promisfydbconnection = promisify(pool.query).bind(pool);
        let result = await promisfydbconnection(q)

        res.status(200).send({status:"success",message:"Insert review berhasil!"})
    } catch (error) {
        res.status(500).send({message:error})
    }
});

router.post("/updateStatusShipment", async (req, res) => {
    let id_shipment = req.body.id_shipment;
    try {
        let promisfydbconnection = promisify(pool.query).bind(pool);
        let result = await promisfydbconnection(
            `UPDATE shipment
            SET status_pengiriman = 'Selesai'
            WHERE id_shipment = ${id_shipment}
            `
        )
        res.status(200).send({status:"success",message:"Update status shipment berhasil!"})
    } catch (error) {
        res.status(500).send({message:error})
    }
});

router.post("/confirmOrder", async (req, res) => {
    let id_shipment = req.body.id_shipment;
    try {
        let promisfydbconnection = promisify(pool.query).bind(pool);
        let result = await promisfydbconnection(
            `UPDATE shipment
            SET status_pengiriman = 'Sudah Diterima'
            WHERE id_shipment = ${id_shipment}
            `
        )
        res.status(200).send({status:"success",message:"Update status shipment berhasil!"})
    } catch (error) {
        res.status(500).send({message:error})
    }
});


//ADMIN
router.get("/getAllOrder", async (req, res) => {

    let promisfydbconnection = promisify(pool.query).bind(pool);
    result1 = await promisfydbconnection(//FOR TABLE RATING PURCHASE LIST
        `SELECT h.id_trans "id_trans", h.id_user "id_user", h.tgl_beli "tgl_beli", h.jumlah "jumlah", h.total "total",
        s.id_shipment "id_shipment", s.status_pengiriman "status_pengiriman", s.nama_penerima "nama_penerima"
        FROM htrans h, shipment s
        WHERE h.id_shipment = s.id_shipment`
    );

    return res.status(200).json({
        status: 200,
        dataList: result1
    });
})

router.post("/sendOrder", async (req, res) => {
    let id_shipment = req.body.id_shipment;
    try {
        let promisfydbconnection = promisify(pool.query).bind(pool);
        let result = await promisfydbconnection(
            `UPDATE shipment
            SET status_pengiriman = 'Sedang Dikirim'
            WHERE id_shipment = ${id_shipment}
            `
        )
        res.status(200).send({status:"success",message:"Update status shipment berhasil!"})
    } catch (error) {
        res.status(500).send({message:error})
    }
});


//SEARCH ORDER ANONYMOUS
router.get("/getListPurchaseAnonymous", async (req, res) => {
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result1 = await promisfydbconnection(//FOR TABLE RATING PURCHASE LIST
        `SELECT h.id_trans "id_trans", h.id_user "id_user", h.tgl_beli "tgl_beli", h.jumlah "jumlah", h.total "total",
        s.id_shipment "id_shipment", s.status_pengiriman "status_pengiriman"
        FROM htrans h, shipment s
        WHERE h.id_shipment = s.id_shipment`
    );

    result2 = await promisfydbconnection(//FOR POPUP RATING
        `SELECT DISTINCT h.id_trans "id_trans",
        d.id_sneaker "id_sneaker", d.harga_satuan "harga_satuan",
        s.nama_sneaker "nama_sneaker", s.brand_sneaker "brand_sneaker", s.gambar "gambar_sneaker",
        sh.id_shipment "id_shipment"
        FROM htrans h, dtrans d, sneaker s, shipment sh
        WHERE h.id_trans = d.id_trans and d.id_sneaker = s.id_sneaker and h.id_shipment = sh.id_shipment`
    );

    return res.status(200).json({
        status: 200,
        dataList: result1,
        listBarang: result2
    });
})

module.exports = router;