﻿const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");	
const logger = require("morgan");
const cors = require("cors");

const laporan = require("./routes/laporan");
const user = require("./routes/user");
const promo = require("./routes/promo");
const alaskaki = require("./routes/alaskaki");
const adminRouter = require("./routes/admin");
const topup = require("./routes/topup");
const purchaselist = require("./routes/purchaselist");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.resolve(__dirname, "build")));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  next();
});

app.use("/apiLaporan", laporan);
app.use("/apiUser", user);
app.use("/apiPromo", promo);
app.use("/apiAlasKaki", alaskaki);
app.use("/api", adminRouter);
app.use("/topup", topup);
app.use("/purchaselist", purchaselist);

app.get("*", (req, res) => {
  res.sendFile("build/index.html", { root: __dirname });
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// TODO Web Template Studio: Add your own error handler here.
if (process.env.NODE_ENV === "production") {
  // Do not send stack trace of error message when in production
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send("Error occurred while handling the request.");
  });
} else {
  // Log stack trace of error message while in development
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    console.log(err);
    res.send(err.message);
  });
}

module.exports = app;
