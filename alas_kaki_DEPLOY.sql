-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2020 at 07:13 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alas_kaki`
--
CREATE DATABASE IF NOT EXISTS `alas_kaki` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `alas_kaki`;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id_cart` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sneaker` int(11) NOT NULL,
  `jumlah_sneaker` int(11) NOT NULL,
  `ukuran_sneaker` double(8,2) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dtrans`
--

DROP TABLE IF EXISTS `dtrans`;
CREATE TABLE `dtrans` (
  `id_dtrans` int(11) NOT NULL,
  `id_trans` int(11) NOT NULL,
  `id_sneaker` int(11) NOT NULL,
  `jumlah_sneaker` int(11) NOT NULL,
  `ukuran_sneaker` double(8,2) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dtrans`
--

INSERT INTO `dtrans` (`id_dtrans`, `id_trans`, `id_sneaker`, `jumlah_sneaker`, `ukuran_sneaker`, `harga_satuan`, `subtotal`) VALUES
(1, 1, 1, 1, 40.00, 500000, 500000),
(2, 1, 2, 1, 35.00, 600000, 600000),
(3, 2, 2, 1, 35.00, 600000, 600000),
(4, 2, 2, 1, 37.00, 600000, 600000),
(5, 2, 1, 2, 38.00, 500000, 1000000),
(6, 3, 1, 1, 40.00, 500000, 500000),
(7, 4, 1, 2, 40.00, 500000, 1000000),
(8, 5, 1, 1, 38.00, 500000, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `htrans`
--

DROP TABLE IF EXISTS `htrans`;
CREATE TABLE `htrans` (
  `id_trans` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_shipment` int(11) NOT NULL,
  `tgl_beli` date NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `deskripsi_promo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_promo` int(10) NOT NULL,
  `diskon_promo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `htrans`
--

INSERT INTO `htrans` (`id_trans`, `id_user`, `id_shipment`, `tgl_beli`, `jumlah`, `total`, `deskripsi_promo`, `total_promo`, `diskon_promo`) VALUES
(1, 0, 1, '2020-12-09', 2, 1126000, '-', 0, 0),
(2, 18, 2, '2020-12-09', 4, 2247000, '-', 0, 0),
(3, 18, 3, '2020-12-09', 1, 315000, 'SepatuQu celebrate winter season :D', 200000, 40),
(4, 18, 4, '2020-12-10', 2, 526000, 'Kami dari toko SepatuQu memberikan diskon besar besaran kepada kalian semua :)', 500000, 50),
(5, 18, 5, '2020-12-10', 1, 348000, 'SepatuQu celebrate winter season :D', 200000, 40);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Man'),
(2, 'Woman'),
(3, 'Kid');

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

DROP TABLE IF EXISTS `promo`;
CREATE TABLE `promo` (
  `id_promo` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `diskon` int(11) NOT NULL,
  `min_belanja` int(20) NOT NULL,
  `max_diskon` int(20) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_berakhir` date NOT NULL,
  `jumlah_kupon` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id_promo`, `id_user`, `judul`, `deskripsi`, `diskon`, `min_belanja`, `max_diskon`, `tanggal_mulai`, `tanggal_berakhir`, `jumlah_kupon`, `gambar`) VALUES
(1, 1, 'Promo Christmas', 'Kami dari toko SepatuQu memberikan diskon besar besaran kepada kalian semua :)', 50, 500000, 500000, '2020-12-09', '2020-12-31', 2, 'gambar2-1607522431125.jpg'),
(2, 0, 'Promo Christmas', 'Kami dari toko SepatuQu memberikan diskon besar besaran kepada kalian semua :)', 50, 500000, 500000, '2020-12-09', '2020-12-31', 2, 'gambar2-1607522431125.jpg'),
(3, 18, 'Promo Christmas', 'Kami dari toko SepatuQu memberikan diskon besar besaran kepada kalian semua :)', 50, 550000, 500000, '2020-12-09', '2020-12-31', 1, 'gambar2-1607522431125.jpg'),
(4, 0, 'Promo Winter', 'SepatuQu celebrate winter season :D', 40, 250000, 300000, '2020-12-09', '2020-12-31', 1, 'gambar2-1607522667929.jpg'),
(5, 18, 'Promo Winter', 'SepatuQu celebrate winter season :D', 40, 250000, 300000, '2020-12-09', '2020-12-31', 0, 'gambar2-1607522667929.jpg'),
(6, 1, 'Promo Winter', 'SepatuQu celebrate winter season :D', 40, 250000, 300000, '2020-12-09', '2020-12-31', 1, 'gambar2-1607522667929.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `id_review` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_sneaker` bigint(20) NOT NULL,
  `review` varchar(255) NOT NULL,
  `rating` float NOT NULL,
  `tgl` date NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id_review`, `id_user`, `id_sneaker`, `review`, `rating`, `tgl`, `gambar`) VALUES
(9, 0, 1, 'sesuai gambar', 5, '2020-12-09', '1607521656639.png'),
(10, 0, 2, 'agak kotor', 2, '2020-12-09', '1607521656665.png'),
(11, 18, 2, 'kok warnanya biru?', 2, '2020-12-09', '1607522096047.png'),
(12, 18, 1, 'b aja', 3, '2020-12-10', '1607533925356.png');

-- --------------------------------------------------------

--
-- Table structure for table `shipment`
--

DROP TABLE IF EXISTS `shipment`;
CREATE TABLE `shipment` (
  `id_shipment` bigint(20) UNSIGNED NOT NULL,
  `nama_penerima` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pengiriman` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ongkir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment`
--

INSERT INTO `shipment` (`id_shipment`, `nama_penerima`, `alamat`, `kode_pos`, `no_telp`, `status_pengiriman`, `ongkir`) VALUES
(1, 'Ferdinan Gutana', 'Babatan Pantai Utara 9/66, Kabupaten Badung, Bali', 60133, '0812345678', 'Selesai', 26000),
(2, 'Edwin Sidharta', 'Babatan Pantai Utara 9/66, Kota Bengkulu, Bengkulu', 60133, '0812345678', 'Selesai', 47000),
(3, 'Edwin Sidharta', 'Babatan Pantai Utara 9/66, Kota Jakarta Barat, DKI Jakarta', 60133, '0812345678', 'Selesai', 15000),
(4, 'Edwin Sidharta', 'Babatan Pantai Utara 9/66, Kabupaten Badung, Bali', 60133, '0812345678', 'Selesai', 26000),
(5, 'Ferdinan Gutana', 'Babatan Pantai Utara 9/66, Kabupaten Bangka, Bangka Belitung', 60133, '0812345678', 'Sedang Disiapkan', 48000);

-- --------------------------------------------------------

--
-- Table structure for table `sneaker`
--

DROP TABLE IF EXISTS `sneaker`;
CREATE TABLE `sneaker` (
  `id_sneaker` bigint(20) UNSIGNED NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_sneaker` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_sneaker` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_sneaker` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_sneaker` int(11) NOT NULL,
  `status_sneaker` tinyint(4) NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sneaker`
--

INSERT INTO `sneaker` (`id_sneaker`, `id_kategori`, `nama_sneaker`, `jenis_sneaker`, `brand_sneaker`, `harga_sneaker`, `status_sneaker`, `gambar`) VALUES
(1, 1, 'Air Force 2', 'Loafer', 'Nike', 500000, 1, 'gambar-1607518746184.jpg'),
(2, 2, 'Air Force Peach', 'Sport', 'Nike', 600000, 1, 'gambar-1607518789042.jpg'),
(3, 3, 'Air Force 1', 'Running', 'Nike', 550000, 1, 'gambar-1607527140118.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ukuran`
--

DROP TABLE IF EXISTS `ukuran`;
CREATE TABLE `ukuran` (
  `id_ukuran` int(11) NOT NULL,
  `id_sneaker` int(11) NOT NULL,
  `ukuran_sneaker` double(8,2) NOT NULL,
  `stok_sneaker` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ukuran`
--

INSERT INTO `ukuran` (`id_ukuran`, `id_sneaker`, `ukuran_sneaker`, `stok_sneaker`) VALUES
(3, 2, 35.00, 8),
(4, 2, 37.00, 14),
(6, 1, 40.00, 6),
(7, 1, 38.00, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `nama`, `email`, `password`, `saldo`) VALUES
(0, 'Guest', 'Guest', 'guest@gmail.com', '', 0),
(1, 'admin', 'admin', 'admin@gmail.com', '', 0),
(18, 'edwin13', 'Edwin Sidharta', 'edwin0sidharta@gmail.com', 'c436c7e0a36cb33c869ebdf9fbba6178|fc21b9', 1564000);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `id_wishlist` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sneaker` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id_wishlist`, `id_user`, `id_sneaker`) VALUES
(10, 18, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indexes for table `dtrans`
--
ALTER TABLE `dtrans`
  ADD PRIMARY KEY (`id_dtrans`);

--
-- Indexes for table `htrans`
--
ALTER TABLE `htrans`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id_promo`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`);

--
-- Indexes for table `shipment`
--
ALTER TABLE `shipment`
  ADD PRIMARY KEY (`id_shipment`);

--
-- Indexes for table `sneaker`
--
ALTER TABLE `sneaker`
  ADD PRIMARY KEY (`id_sneaker`);

--
-- Indexes for table `ukuran`
--
ALTER TABLE `ukuran`
  ADD PRIMARY KEY (`id_ukuran`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id_wishlist`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `dtrans`
--
ALTER TABLE `dtrans`
  MODIFY `id_dtrans` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `htrans`
--
ALTER TABLE `htrans`
  MODIFY `id_trans` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
  MODIFY `id_promo` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id_review` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `shipment`
--
ALTER TABLE `shipment`
  MODIFY `id_shipment` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sneaker`
--
ALTER TABLE `sneaker`
  MODIFY `id_sneaker` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ukuran`
--
ALTER TABLE `ukuran`
  MODIFY `id_ukuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id_wishlist` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
